<?php 
 return array (
  'XAU' => 
  array (
    'graph' => 'XAUUSD',
    'name' => 'Gold',
    'name_short' => 'Gold',
    'units' => 'OZ',
    'active' => 1,
  ),
  'XAG' => 
  array (
    'graph' => 'XAGUSD',
    'name' => 'Silver',
    'name_short' => 'Silver',
    'units' => 'OZ',
    'active' => 1,
  ),
  'XPT' => 
  array (
    'graph' => 'XPTUSD',
    'name' => 'Platinum',
    'name_short' => 'Platinum',
    'units' => 'OZ',
    'active' => 1,
  ),
  'XPD' => 
  array (
    'graph' => 'XPDUSD',
    'name' => 'Palladium',
    'name_short' => 'Palladium',
    'units' => 'OZ',
    'active' => 1,
  ),
);