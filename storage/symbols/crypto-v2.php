<?php 
 return array (
  'BTC:USDT' => 
  array (
    'minmov' => '1',
    'pricescale' => '100',
    'active' => 1,
    'name' => 'Bitcoin',
  ),
  'BCH:USDT' => 
  array (
    'minmov' => '1',
    'pricescale' => '100',
    'active' => 1,
    'name' => 'Bitcoin Cash',
  ),
  'BCH:BTC' => 
  array (
    'minmov' => '0.0001',
    'pricescale' => '1000000',
    'active' => 1,
    'name' => 'Bitcoin Cash',
  ),
  'BNB:USDT' => 
  array (
    'minmov' => '1',
    'pricescale' => '100',
    'active' => 1,
    'name' => 'Binance Coin',
  ),
  'ETH:USDT' => 
  array (
    'minmov' => '1',
    'pricescale' => '100',
    'active' => 1,
    'name' => 'Ethereum',
  ),
  'ETH:BTC' => 
  array (
    'minmov' => '0.0001',
    'pricescale' => '1000000',
    'active' => 1,
    'name' => 'Ethereum',
  ),
  'LTC:USDT' => 
  array (
    'minmov' => '1',
    'pricescale' => '100',
    'active' => 1,
    'name' => 'Litecoin',
  ),
  'LTC:BTC' => 
  array (
    'minmov' => '0.001',
    'pricescale' => '100000',
    'active' => 1,
    'name' => 'Litecoin',
  ),
  'LTC:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'Litecoin',
  ),
  'XRP:USDT' => 
  array (
    'minmov' => '0.001',
    'pricescale' => '100000',
    'active' => 1,
    'name' => 'Ripple',
  ),
  'XRP:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'Ripple',
  ),
  'XRP:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'Ripple',
  ),
  'KSM:USDT' => 
  array (
    'minmov' => '1',
    'pricescale' => '100',
    'active' => 1,
    'name' => 'Kusama',
  ),
  'KNC:USDT' => 
  array (
    'minmov' => '0.1',
    'pricescale' => '1000',
    'active' => 1,
    'name' => 'Kyber Network',
  ),
  'KNC:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'Kyber Network',
  ),
  'KNC:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'Kyber Network',
  ),
  'REP:USDT' => 
  array (
    'minmov' => '0.1',
    'pricescale' => '1000',
    'active' => 1,
    'name' => 'Augur',
  ),
  'REP:BTC' => 
  array (
    'minmov' => '0.0001',
    'pricescale' => '1000000',
    'active' => 1,
    'name' => 'Augur',
  ),
  'REP:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'Augur',
  ),
  'XTZ:USDT' => 
  array (
    'minmov' => '0.01',
    'pricescale' => '10000',
    'active' => 1,
    'name' => 'Tezos',
  ),
  'XTZ:BTC' => 
  array (
    'minmov' => '0.00001',
    'pricescale' => '10000000',
    'active' => 1,
    'name' => 'Tezos',
  ),
  'XLM:USDT' => 
  array (
    'minmov' => '0.001',
    'pricescale' => '100000',
    'active' => 1,
    'name' => 'Stellar',
  ),
  'XLM:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'Stellar',
  ),
  'XLM:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'Stellar',
  ),
  'ZRX:USDT' => 
  array (
    'minmov' => '0.01',
    'pricescale' => '10000',
    'active' => 1,
    'name' => '0rx',
  ),
  'ZRX:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => '0rx',
  ),
  'ZRX:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => '0rx',
  ),
  'OMG:USDT' => 
  array (
    'minmov' => '0.01',
    'pricescale' => '10000',
    'active' => 1,
    'name' => 'OMG Network',
  ),
  'OMG:BTC' => 
  array (
    'minmov' => '0.00001',
    'pricescale' => '10000000',
    'active' => 1,
    'name' => 'OMG Network',
  ),
  'OMG:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'OMG Network',
  ),
  'LINK:USDT' => 
  array (
    'minmov' => '0.1',
    'pricescale' => '1000',
    'active' => 1,
    'name' => 'Chainlink',
  ),
  'LINK:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'Chainlink',
  ),
  'LINK:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'Chainlink',
  ),
  'MKR:USDT' => 
  array (
    'minmov' => '1',
    'pricescale' => '100',
    'active' => 1,
    'name' => 'Maker',
  ),
  'MKR:BTC' => 
  array (
    'minmov' => '0.0001',
    'pricescale' => '1000000',
    'active' => 1,
    'name' => 'Maker',
  ),
  'EOS:USDT' => 
  array (
    'minmov' => '0.01',
    'pricescale' => '10000',
    'active' => 1,
    'name' => 'EOS',
  ),
  'EOS:BTC' => 
  array (
    'minmov' => '0.0001',
    'pricescale' => '1000000',
    'active' => 1,
    'name' => 'EOS',
  ),
  'EOS:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'EOS',
  ),
  'AION:USDT' => 
  array (
    'minmov' => '0.01',
    'pricescale' => '10000',
    'active' => 1,
    'name' => 'AION',
  ),
  'AION:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'AION',
  ),
  'AION:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'AION',
  ),
  'ADA:USDT' => 
  array (
    'minmov' => '0.001',
    'pricescale' => '100000',
    'active' => 1,
    'name' => 'Cardano',
  ),
  'ADA:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'Cardano',
  ),
  'ADA:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'Cardano',
  ),
  'TRX:USDT' => 
  array (
    'minmov' => '0.001',
    'pricescale' => '100000',
    'active' => 1,
    'name' => 'TRON',
  ),
  'TRX:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'TRON',
  ),
  'TRX:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'TRON',
  ),
  'XMR:USDT' => 
  array (
    'minmov' => '1',
    'pricescale' => '100',
    'active' => 1,
    'name' => 'Monero',
  ),
  'XMR:BTC' => 
  array (
    'minmov' => '0.0001',
    'pricescale' => '1000000',
    'active' => 1,
    'name' => 'Monero',
  ),
  'XMR:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'Monero',
  ),
  'NEO:USDT' => 
  array (
    'minmov' => '1',
    'pricescale' => '100',
    'active' => 1,
    'name' => 'NEO',
  ),
  'NEO:BTC' => 
  array (
    'minmov' => '0.001',
    'pricescale' => '100000',
    'active' => 1,
    'name' => 'NEO',
  ),
  'NEO:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'NEO',
  ),
  'ATOM:USDT' => 
  array (
    'minmov' => '0.01',
    'pricescale' => '1000',
    'active' => 1,
    'name' => 'Cosmos',
  ),
  'ATOM:BTC' => 
  array (
    'minmov' => '0.00001',
    'pricescale' => '10000000',
    'active' => 1,
    'name' => 'Cosmos',
  ),
  'ANT:USDT' => 
  array (
    'minmov' => '0.01',
    'pricescale' => '10000',
    'active' => 1,
    'name' => 'Aragon',
  ),
  'ANT:BTC' => 
  array (
    'minmov' => '0.00001',
    'pricescale' => '10000000',
    'active' => 1,
    'name' => 'Aragon',
  ),
  'ARDR:USDT' => 
  array (
    'minmov' => '0.01',
    'pricescale' => '10000',
    'active' => 1,
    'name' => 'Ardor',
  ),
  'ARDR:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'Ardor',
  ),
  'BEAM:USDT' => 
  array (
    'minmov' => '0.01',
    'pricescale' => '10000',
    'active' => 1,
    'name' => 'Beam',
  ),
  'BEAM:BTC' => 
  array (
    'minmov' => '0.00001',
    'pricescale' => '10000000',
    'active' => 1,
    'name' => 'Beam',
  ),
  'BLZ:USDT' => 
  array (
    'minmov' => '0.001',
    'pricescale' => '100000',
    'active' => 1,
    'name' => 'Bluzelle',
  ),
  'BLZ:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'Bluzelle',
  ),
  'BLZ:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'Bluzelle',
  ),
  'BNT:USDT' => 
  array (
    'minmov' => '0.01',
    'pricescale' => '10000',
    'active' => 1,
    'name' => 'Bancor',
  ),
  'BNT:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'Bancor',
  ),
  'BNT:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'Bancor',
  ),
  'BTS:USDT' => 
  array (
    'minmov' => '0.001',
    'pricescale' => '100000',
    'active' => 1,
    'name' => 'BitShares',
  ),
  'BTS:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'BitShares',
  ),
  'BTT:USDT' => 
  array (
    'minmov' => '0.00001',
    'pricescale' => '10000000',
    'active' => 1,
    'name' => 'BitTorrent Token',
  ),
  'CVC:USDT' => 
  array (
    'minmov' => '0.001',
    'pricescale' => '100000',
    'active' => 1,
    'name' => 'Civic',
  ),
  'CVC:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'Civic',
  ),
  'CVC:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'Civic',
  ),
  'DATA:USDT' => 
  array (
    'minmov' => '0.001',
    'pricescale' => '100000',
    'active' => 1,
    'name' => 'Streamr DATAcoin',
  ),
  'DATA:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'Streamr DATAcoin',
  ),
  'DATA:ETH' => 
  array (
    'minmov' => '',
    'pricescale' => '',
    'active' => 1,
    'name' => 'Streamr DATAcoin',
  ),
  'DCR:USDT' => 
  array (
    'minmov' => '0.1',
    'pricescale' => '1000',
    'active' => 1,
    'name' => 'Decred',
  ),
  'DCR:BTC' => 
  array (
    'minmov' => '0.0001',
    'pricescale' => '1000000',
    'active' => 1,
    'name' => 'Decred',
  ),
  'DENT:USDT' => 
  array (
    'minmov' => '0.00001',
    'pricescale' => '10000000',
    'active' => 1,
    'name' => 'Dent',
  ),
  'DGB:USDT' => 
  array (
    'minmov' => '0.001',
    'pricescale' => '100000',
    'active' => 1,
    'name' => 'DigiByte',
  ),
  'DGB:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'DigiByte',
  ),
  'DOCK:USDT' => 
  array (
    'minmov' => '0.0001',
    'pricescale' => '1000000',
    'active' => 1,
    'name' => 'Dock',
  ),
  'DOGE:USDT' => 
  array (
    'minmov' => '0.00001',
    'pricescale' => '10000000',
    'active' => 1,
    'name' => 'Dogecoin',
  ),
  'ENJ:USDT' => 
  array (
    'minmov' => '0.001',
    'pricescale' => '100000',
    'active' => 1,
    'name' => 'Enjin Coin',
  ),
  'ENJ:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'Enjin Coin',
  ),
  'ENJ:ETH' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'Enjin Coin',
  ),
  'FUN:USDT' => 
  array (
    'minmov' => '0.0001',
    'pricescale' => '1000000',
    'active' => 1,
    'name' => 'FunFair',
  ),
  'FUN:ETH' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'FunFair',
  ),
  'GTO:USDT' => 
  array (
    'minmov' => '0.001',
    'pricescale' => '100000',
    'active' => 1,
    'name' => 'Gifto',
  ),
  'GXS:USDT' => 
  array (
    'minmov' => '0.01',
    'pricescale' => '10000',
    'active' => 1,
    'name' => 'GXChain',
  ),
  'GXS:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'GXChain',
  ),
  'GXS:ETH' => 
  array (
    'minmov' => '0.0001',
    'pricescale' => '1000000',
    'active' => 1,
    'name' => 'GXChain',
  ),
  'ICX:USDT' => 
  array (
    'minmov' => '0.01',
    'pricescale' => '10000',
    'active' => 1,
    'name' => 'Icon',
  ),
  'ICX:BTC' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'Icon',
  ),
  'ICX:ETH' => 
  array (
    'minmov' => '0.0001',
    'pricescale' => '1000000',
    'active' => 1,
    'name' => 'Icon',
  ),
  'IOST:USDT' => 
  array (
    'minmov' => '0.0001',
    'pricescale' => '1000000',
    'active' => 1,
    'name' => 'IOST',
  ),
  'IOST:ETH' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'IOST',
  ),
  'IOTX:USDT' => 
  array (
    'minmov' => '0.0001',
    'pricescale' => '1000000',
    'active' => 1,
    'name' => 'IoTeX',
  ),
  'IOTX:ETH' => 
  array (
    'minmov' => '0.000001',
    'pricescale' => '100000000',
    'active' => 1,
    'name' => 'IoTeX',
  ),
  'ETC:USDT' => 
  array (
    'minmov' => '0.01',
    'pricescale' => '10000',
    'active' => 1,
    'name' => 'Ethereum Classic',
  ),
  'ETC:BTC' => 
  array (
    'minmov' => '0.00001',
    'pricescale' => '10000000',
    'active' => 1,
    'name' => 'Ethereum Classic',
  ),
  'ETC:ETH' => 
  array (
    'minmov' => '0.0001',
    'pricescale' => '1000000',
    'active' => 1,
    'name' => 'Ethereum Classic',
  ),
  'DASH:USDT' => 
  array (
    'minmov' => '1',
    'pricescale' => '100',
    'active' => 1,
    'name' => 'Dash',
  ),
  'DASH:BTC' => 
  array (
    'minmov' => '0.001',
    'pricescale' => '100000',
    'active' => 1,
    'name' => 'Dash',
  ),
  'SHIB:USDT' => 
  array (
    'minmov' => '0.00001',
    'pricescale' => '10000000',
    'active' => 1,
    'name' => 'SHIBA INU',
  ),
);