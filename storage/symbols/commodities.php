<?php


return [
    'XAU' => [
        'graph' => 'XAUUSD',
        'name' => 'Gold',
        'name_short' => 'Gold',
        'units' => 'OZ',
    ],
    'XAG' => [
        'graph' => 'XAGUSD',
        'name' => 'Silver',
        'name_short' => 'Silver',
        'units' => 'OZ',
    ],
    'XPT' => [
        'graph' =>'XPTUSD',
        'name' => 'Platinum',
        'name_short' => 'Platinum',
        'units' => 'OZ',
    ],
    'XPD' => [
        'graph' => 'XPDUSD',
        'name' => 'Palladium',
        'name_short' => 'Palladium',
        'units' => 'OZ',
    ]
];
