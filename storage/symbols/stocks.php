<?php

return [
    "AAPL" => [
        "name" => "Apple Inc",
        "country" => "US",
        "industry" => "Technology"
    ],
    "AMZN" => [
        "name" => "Amazon.com Inc",
        "country" => "US",
        "industry" => "Retail"
    ],
    "FB" => [
        "name" => "Facebook Inc",
        "country" => "US",
        "industry" => "Media"
    ],
    "MSFT" => [
        "name" => "Microsoft Corp",
        "country" => "US",
        "industry" => "Technology"
    ],
    "ADBE" => [
        "name" => "Adobe Inc",
        "country" => "US",
        "industry" => "Technology"
    ],
    "TSLA" => [
        "name" => "Tesla Inc",
        "country" => "US",
        "industry" => "Automobiles"
    ],
    "NVDA" => [
        "name" => "NVIDIA Corp",
        "country" => "US",
        "industry" => "Semiconductors"
    ],
    "NFLX" => [
        "name" => "Netflix Inc",
        "country" => "US",
        "industry" => "Media"
    ],
    "PYPL" => [
        "name" => "PayPal Holdings Inc",
        "country" => "US",
        "industry" => "Technology"
    ],
    "BABA" => [
        "name" => "Alibaba Group Holding Ltd",
        "country" => "US",
        "industry" => "Internet Retail"
    ],
    "INTC" => [
        "name" => "Intel Corp",
        "country" => "US",
        "industry" => "Semiconductors"
    ],
    "CMCSA" => [
        "name" => "Comcast Corp",
        "country" => "US",
        "industry" => "Media"
    ],
    "PEP" => [
        "name" => "PepsiCo Inc",
        "country" => "US",
        "industry" => "Beverages"
    ],
    "CSCO" => [
        "name" => "Cisco Systems Inc",
        "country" => "US",
        "industry" => "Communications"
    ],
    "GOOGL" => [
        "name" => "Alphabet Inc",
        "country" => "US",
        "industry" => "Media"
    ],
    "AMGN" => [
        "name" => "Amgen Inc",
        "country" => "US",
        "industry" => "Biotechnology"
    ],
    "AVGO" => [
        "name" => "Broadcom Inc",
        "country" => "US",
        "industry" => "Semiconductors"
    ],
    "COST" => [
        "name" => "Costco Wholesale Corp",
        "country" => "US",
        "industry" => "Retail"
    ],
    "QCOM" => [
        "name" => "Qualcomm Inc",
        "country" => "US",
        "industry" => "Semiconductors"
    ],
    "TMUS" => [
        "name" => "T-Mobile US Inc",
        "country" => "US",
        "industry" => "Telecommunication"
    ],
    "TXN" => [
        "name" => "Texas Instruments Inc",
        "country" => "US",
        "industry" => "Semiconductors"
    ],
    "CHTR" => [
        "name" => "Charter Communications Inc",
        "country" => "US",
        "industry" => "Media"
    ],
    "ADI" => [
        "name" => "Analog Devices Inc",
        "country" => "US",
        "industry" => "Semiconductors"
    ],
    "ADP" => [
        "name" => "Automatic Data Processing Inc",
        "country" => "US",
        "industry" => "Technology"
    ],
    "ADSK" => [
        "name" => "Autodesk Inc",
        "country" => "US",
        "industry" => "Technology"
    ],
    "ALGN" => [
        "name" => "Align Technology Inc",
        "country" => "US",
        "industry" => "Health Care"
    ],
    "ALXN" => [
        "name" => "Alexion Pharmaceuticals Inc",
        "country" => "US",
        "industry" => "Biotechnology"
    ],
    "AMAT" => [
        "name" => "ApplUKd Materials Inc",
        "country" => "US",
        "industry" => "Semiconductors"
    ],
    "AMD" => [
        "name" => "Advanced Micro Devices Inc",
        "country" => "US",
        "industry" => "Semiconductors"
    ],
    "ANSS" => [
        "name" => "ANSYS Inc",
        "country" => "US",
        "industry" => "Technology"
    ],
    "ATVI" => [
        "name" => "Activision Blizzard Inc",
        "country" => "US",
        "industry" => "Media"
    ],
    "BIIB" => [
        "name" => "Biogen Inc",
        "country" => "US",
        "industry" => "Biotechnology"
    ],
    "BKNG" => [
        "name" => "Booking Holdings Inc",
        "country" => "US",
        "industry" => "Retail"
    ],
    "BMRN" => [
        "name" => "Biomarin Pharmaceutical Inc",
        "country" => "US",
        "industry" => "Biotechnology"
    ],
    "CDNS" => [
        "name" => "Cadence Design Systems Inc",
        "country" => "US",
        "industry" => "Technology"
    ],
    "CDW" => [
        "name" => "CDW Corp",
        "country" => "US",
        "industry" => "Electrical Equipment"
    ],
    "CERN" => [
        "name" => "Cerner Corp",
        "country" => "US",
        "industry" => "Health Care"
    ],
    "CPRT" => [
        "name" => "Copart Inc",
        "country" => "US",
        "industry" => "Commercial Services & SupplUKs"
    ],
    "CSX" => [
        "name" => "CSX Corp",
        "country" => "US",
        "industry" => "Road & Rail"
    ],
    "CTAS" => [
        "name" => "Cintas Corp",
        "country" => "US",
        "industry" => "Commercial Services & SupplUKs"
    ],
    "CTSH" => [
        "name" => "Cognizant Technology Solutions Corp",
        "country" => "US",
        "industry" => "Technology"
    ],
    "CTXS" => [
        "name" => "Citrix Systems Inc",
        "country" => "US",
        "industry" => "Technology"
    ],
    "DLTR" => [
        "name" => "Dollar Tree Inc",
        "country" => "US",
        "industry" => "Retail"
    ],
    "DOCU" => [
        "name" => "DocuSign Inc",
        "country" => "US",
        "industry" => "Technology"
    ],
    "DXCM" => [
        "name" => "DexCom Inc",
        "country" => "US",
        "industry" => "Health Care"
    ],
    "EA" => [
        "name" => "Electronic Arts Inc",
        "country" => "US",
        "industry" => "Media"
    ],
    "EBAY" => [
        "name" => "eBay Inc",
        "country" => "US",
        "industry" => "Retail"
    ],
    "EXC" => [
        "name" => "Exelon Corp",
        "country" => "US",
        "industry" => "UtilitUKs"
    ],
    "EXPE" => [
        "name" => "Expedia Group Inc",
        "country" => "US",
        "industry" => "Retail"
    ],
    "FAST" => [
        "name" => "Fastenal Co",
        "country" => "US",
        "industry" => "Trading CompanUKs & Distributors"
    ],
    "FISV" => [
        "name" => "Fiserv Inc",
        "country" => "US",
        "industry" => "Technology"
    ],
    "FOXA" => [
        "name" => "Fox Corp",
        "country" => "US",
        "industry" => "Media"
    ],
    "GILD" => [
        "name" => "Gilead ScUKnces Inc",
        "country" => "US",
        "industry" => "Biotechnology"
    ],
    "IDXX" => [
        "name" => "IDEXX LaboratorUKs Inc",
        "country" => "US",
        "industry" => "Health Care"
    ],
    "ILMN" => [
        "name" => "Illumina Inc",
        "country" => "US",
        "industry" => "Life ScUKnces Tools & Services"
    ],
    "INCY" => [
        "name" => "Incyte Corp",
        "country" => "US",
        "industry" => "Biotechnology"
    ],
    "INTU" => [
        "name" => "Intuit Inc",
        "country" => "US",
        "industry" => "Technology"
    ],
    "ISRG" => [
        "name" => "Intuitive Surgical Inc",
        "country" => "US",
        "industry" => "Health Care"
    ],
    "KHC" => [
        "name" => "Kraft Heinz Co",
        "country" => "US",
        "industry" => "Food Products"
    ],
    "PRU" => [
        "name" => "Prudential Financial Inc",
        "country" => "US",
        "industry" => "Insurance"
    ],
    "RHHBY" => [
        "name" => "Roche Holding",
        "country" => "EU"
    ],
    "VOD" => [
        "name" => "Vodafone",
        "country" => "UK"
    ],
    "ACM" => [
        "name" => "AECOM",
        "country" => "US",
        "industry" => "Construction"
    ],
    "ADAP" => [
        "name" => "Adaptimmune Therapeutics PLC",
        "country" => "UK",
        "industry" => "Biotechnology"
    ],
    "AEF" => [
        "name" => "Aberdeen Emerging Markets Equity Income Fund Inc",
        "country" => "US",
        "industry" => "N/A"
    ],
    "AFMD" => [
        "name" => "Affimed NV",
        "country" => "EU",
        "industry" => "Biotechnology"
    ],
    "AKTX" => [
        "name" => "Akari Therapeutics PLC",
        "country" => "US",
        "industry" => "Pharmaceuticals"
    ],
    "ALKS" => [
        "name" => "Alkermes Plc",
        "country" => "UK",
        "industry" => "Biotechnology"
    ],
    "AUTL" => [
        "name" => "Autolus Therapeutics PLC",
        "country" => "UK",
        "industry" => "Biotechnology"
    ],
    //"AXGT" => [
    //    "name" => "Axovant Gene TherapUKs Ltd",
    //    "country" => "UK",
    //    "industry" => "Biotechnology"
    //],
    "AY" => [
        "name" => "Atlantica Sustainable Infrastructure PLC",
        "country" => "UK",
        "industry" => "Utilities"
    ],
    "BCYC" => [
        "name" => "Bicycle Therapeutics PLC",
        "country" => "UK",
        "industry" => "Biotechnology"
    ],
    "GAM" => [
        "name" => "General American Investors Co Inc",
        "country" => "US",
        "industry" => "N/A"
    ],
    "GAN" => [
        "name" => "GameAccount Network",
        "country" => "UK",
    ],
    "ACIU" => [
        "name" => "AC Immune SA",
        "country" => "EU",
    ],
    "GSM" => [
        "name" => "Ferroglobe PLC",
        "country" => "UK",
        "industry" => "Metals & Mining"
    ],
    "HUD" => [
        "name" => "Hudson Ltd",
        "country" => "US",
        "industry" => "Retail"
    ],
    "LRCX" => [
        "name" => "Lam Research Corp",
        "country" => "US"
    ],
    "MAR" => [
        "name" => "Marriott International Inc",
        "country" => "US"
    ],
    "MCHP" => [
        "name" => "Microchip Technology Inc",
        "country" => "US"
    ],
    "MDLZ" => [
        "name" => "Mondelez International Inc",
        "country" => "US"
    ],
    "MELI" => [
        "name" => "Mercadolibre Inc",
        "country" => "US"
    ],
    "MNST" => [
        "name" => "Monster Beverage Corp",
        "country" => "US"
    ],
    "MRNA" => [
        "name" => "Moderna Inc",
        "country" => "US"
    ],
    "MU" => [
        "name" => "Micron Technology Inc",
        "country" => "US"
    ],
    "MXIM" => [
        "name" => "Maxim Integrated Products Inc",
        "country" => "US"
    ],
    "NXPI" => [
        "name" => "NXP Semiconductors NV",
        "country" => "EU"
    ],
    "ORLY" => [
        "name" => "O'Reilly Automotive Inc",
        "country" => "US"
    ],
    "PAYX" => [
        "name" => "Paychex Inc",
        "country" => "US"
    ],
    "PCAR" => [
        "name" => "Paccar Inc",
        "country" => "US"
    ],
    "REGN" => [
        "name" => "Regeneron Pharmaceuticals Inc",
        "country" => "US"
    ],
    "ROST" => [
        "name" => "Ross Stores Inc",
        "country" => "US"
    ],
    "SBUX" => [
        "name" => "Starbucks Corp",
        "country" => "US"
    ],
    "SGEN" => [
        "name" => "Seattle Genetics Inc",
        "country" => "US"
    ],
    "SIRI" => [
        "name" => "Sirius XM Holdings Inc",
        "country" => "US"
    ],
    "SNPS" => [
        "name" => "Synopsys Inc",
        "country" => "US"
    ],
    "SPLK" => [
        "name" => "Splunk Inc",
        "country" => "US"
    ],
    "SWKS" => [
        "name" => "Skyworks Solutions Inc",
        "country" => "US"
    ],
    "TTWO" => [
        "name" => "Take-Two Interactive Software Inc",
        "country" => "US"
    ],
    "ULTA" => [
        "name" => "Ulta Beauty Inc",
        "country" => "US"
    ],
    "VRSK" => [
        "name" => "Verisk Analytics Inc",
        "country" => "US"
    ],
    "VRSN" => [
        "name" => "Verisign Inc",
        "country" => "US"
    ],
    "VRTX" => [
        "name" => "Vertex Pharmaceuticals Inc",
        "country" => "US"
    ],
    "WBA" => [
        "name" => "Walgreens Boots Alliance Inc",
        "country" => "US"
    ],
    "WDAY" => [
        "name" => "Workday Inc",
        "country" => "US"
    ],
    "WDC" => [
        "name" => "Western Digital Corp",
        "country" => "US"
    ],
    "XEL" => [
        "name" => "Xcel Energy Inc",
        "country" => "US"
    ],
    "XLNX" => [
        "name" => "Xilinx Inc",
        "country" => "US"
    ],
    "ZM" => [
        "name" => "Zoom Video Communications Inc",
        "country" => "US"
    ],
    "KLAC" => [
        "name" => "KLA Corp",
        "country" => "US"
    ],
    "LBTYK" => [
        "name" => "Liberty Global Inc",
        "country" => "UK"
    ],
    "AAL" => [
        "name" => "American Airlines Group Inc",
        "country" => "US"
    ],
    "ACN" => [
        "name" => "Accenture",
        "country" => "UK"
    ],
    "AZN" => [
        "name" => "AstraZeneca",
        "country" => "UK"
    ],
//            "BATS" => [
//                "name" => "British American Tobacco",
//                "country" => "UK"
//            ],
    "BP" => [
        "name" => "BP",
        "country" => "UK"
    ],
    "GSK" => [
        "name" => "GlaxoSmithKline",
        "country" => "UK"
    ],
    "LRLCY" => [
        "name" => "L’Oréal",
        "country" => "EU"
    ],
    "LVMUY" => [
        "name" => "LVMH",
        "country" => "EU"
    ],
    "MDT" => [
        "name" => "Medtronic",
        "country" => "UK"
    ],
    "NSRGY" => [
        "name" => "Nestlé",
        "country" => "EU"
    ],
    "NVO" => [
        "name" => "Novo Nordisk",
        "country" => "EU"
    ],
    "NVS" => [
        "name" => "Novartis",
        "country" => "EU"
    ],
    "AMRN" => [
        "name" => "Amarin Corporation",
        "country" => "EU"
    ],
    "CYAD" => [
        "name" => "Celyad Oncology",
        "country" => "EU"
    ],
    "GLPG" => [
        "name" => "Galapagos NV",
        "country" => "EU"
    ],
    "MTLS" => [
        "name" => "Materialise NV",
        "country" => "EU"
    ],
    "ASND" => [
        "name" => "Ascendis Pharma",
        "country" => "EU"
    ],
    "FWP" => [
        "name" => "Forward Pharma",
        "country" => "EU"
    ],
    "ZEAL" => [
        "name" => "Zealand Pharma",
        "country" => "EU"
    ],
    "CLLS" => [
        "name" => "Cellectis",
        "country" => "EU"
    ],
    "CRTO" => [
        "name" => "Criteo",
        "country" => "EU"
    ],
    "DBVT" => [
        "name" => "DBV Technologies",
        "country" => "EU"
    ],
    "EDAP" => [
        "name" => "EDAP TMS S.A.",
        "country" => "EU"
    ],
    "ERYP" => [
        "name" => "ERYTECH Pharma S.A.",
        "country" => "EU"
    ],
    "GNFT" => [
        "name" => "GENFIT",
        "country" => "EU"
    ],
    "SQNS" => [
        "name" => "Sequans Communications",
        "country" => "EU"
    ],
    "BFRA" => [
        "name" => "Biofrontera",
        "country" => "EU"
    ],
    "BNTX" => [
        "name" => "BioNTech",
        "country" => "EU"
    ],
    "MOR" => [
        "name" => "MorphoSys",
        "country" => "EU"
    ],
    "LOV" => [
        "name" => "Spark Networks",
        "country" => "EU"
    ],
    "VJET" => [
        "name" => "Voxeljet",
        "country" => "EU"
    ],
    "APA" => [
        "name" => "Apache Corp.",
        "country" => "US"
    ],
    "CME" => [
        "name" => "CME Group",
        "country" => "US"
    ],
    //"GWPH" => [
    //    "name" => "GW Pharmaceuticals",
    //    "country" => "UK"
    //],
    "MREO" => [
        "name" => "Mereo BioPharma",
        "country" => "UK"
    ],
    "MTP" => [
        "name" => "Midatech Pharma",
        "country" => "UK"
    ],
    "NCNA" => [
        "name" => "NuCana",
        "country" => "UK"
    ],
    "ORTX" => [
        "name" => "Orchard Therapeutics",
        "country" => "UK"
    ],
    "SMMT" => [
        "name" => "Summit Therapeutics",
        "country" => "UK"
    ],
    "VRNA" => [
        "name" => "Verona Pharma",
        "country" => "UK"
    ],
    'NOK' => [
        'name' => 'Nokia',
        'country' => 'EU'
    ],
    'LYG' => [
        'name' => 'Lloyds Banking Group',
        'country' => 'UK'
    ],
    'RDS.A' => [
        'name' => 'Royal Dutch Shell Class A',
        'country' => 'EU'
    ],
    'DB' => [
        'name' => 'Deutsche Bank',
        'country' => 'EU'
    ],
    'EQNR' => [
        'name' => 'Statoil (Equinor)',
        'country' => 'EU'
    ],
    'MT' => [
        'name' => 'ArcelorMittal',
        'country' => 'EU'
    ],
    'ING' => [
        'name' => 'ING (ING Group)',
        'country' => 'EU'
    ],
    'TOT' => [
        'name' => 'Total (Total SE)',
        'country' => 'EU'
    ],
    'BCS' => [
        'name' => 'Barclays',
        'country' => 'UK'
    ],
    'BBVA' => [
        'name' => 'Banco Bilbao Vizcaya Argentaria',
        'country' => 'EU'
    ],
    'BUD' => [
        'name' => 'Anheuser-Busch InBev (AB InBev)',
        'country' => 'EU'
    ],
    'TS' => [
        'name' => 'Tenaris',
        'country' => 'EU'
    ],
    //'UN' => [
    //    'name' => 'Unilever NV',
    //    'country' => 'UK'
    //],
    'TEF' => [
        'name' => 'Telefónica',
        'country' => 'EU'
    ],
    'ABB' => [
        'name' => 'ABB Ltd',
        'country' => 'EU'
    ],
    'E' => [
        'name' => 'Eni',
        'country' => 'EU'
    ],
    'DEO' => [
        'name' => 'Diageo',
        'country' => 'UK'
    ],
    'PHG' => [
        'name' => 'Koninklijke Philips NV',
        'country' => 'EU'
    ],
    'NGG' => [
        'name' => 'National Grid',
        'country' => 'UK'
    ],
    'FMS' => [
        'name' => 'Fresenius Medical Care',
        'country' => 'EU'
    ],
    'SAN' => [
        'name' => 'Banco Santander SA',
        'country' => 'EU'
    ],
    'CRH' => [
        'name' => 'CRH PLC',
        'country' => 'EU'
    ],
    'LIN' => [
        'name' => 'The Linde Group',
        'country' => 'EU'
    ],
    'CS' => [
        'name' => 'Credit Suisse Group AG',
        'country' => 'EU'
    ],
    'HSBC' => [
        'name' => 'HSBC Holdings plc',
        'country' => 'UK'
    ],
    'ORAN' => [
        'name' => 'Orange',
        'country' => 'EU'
    ],
    'PUK' => [
        'name' => 'Prudential PLC',
        'country' => 'UK'
    ],
    'UBS' => [
        'name' => 'UBS',
        'country' => 'EU'
    ],
    'RIO' => [
        'name' => 'Rio Tinto',
        'country' => 'UK'
    ],
    'ALLE' => [
        'name' => 'Allegion PLC',
        'country' => 'EU'
    ],
    'AON' => [
        'name' => 'Aon',
        'country' => 'UK'
    ],
    'CCC' => [
        'name' => 'CLARIVATE PLC',
        'country' => 'UK'
    ],
    'CCEP' => [
        'name' => 'Coca-Cola European Partners PLC',
        'country' => 'UK'
    ],
    'CPRI' => [
        'name' => 'Capri Holdings',
        'country' => 'US'
    ],
    'DAVA' => [
        'name' => 'Endava',
        'country' => 'UK'
    ],
    'FTCH' => [
        'name' => 'Farfetch',
        'country' => 'UK'
    ],
    'FTI' => [
        'name' => 'TechnipFMC',
        'country' => 'UK'
    ],
    'SAP' => [
        'name' => 'SAP',
        'country' => 'EU'
    ],
    'AER' => [
        'name' => 'AerCap',
        'country' => 'EU'
    ],
    'AGRO' => [
        'name' => 'Adecoagro SA',
        'country' => 'EU'
    ],
    'ALV' => [
        'name' => 'Autoliv',
        'country' => 'EU'
    ],
    'BTI' => [
        'name' => 'British American Tobacco',
        'country' => 'UK'
    ],
    'IHG' => [
        'name' => 'InterContinental Hotels Group',
        'country' => 'UK'
    ],
    'PSO' => [
        'name' => 'Pearson PLC',
        'country' => 'UK'
    ],
    'RELX' => [
        'name' => 'RELX Group',
        'country' => 'UK'
    ],
    'SNN' => [
        'name' => 'Smith & Nephew',
        'country' => 'UK'
    ],
    'MMM' => [
        'name' => '3M',
        'country' => 'US'
    ],
    'ABT' => [
        'name' => 'Abbott Laboratories',
        'country' => 'US'
    ],
    'AA' => [
        'name' => 'Alcoa',
        'country' => 'US'
    ],
    'ALL' => [
        'name' => 'Allstate Corp',
        'country' => 'US'
    ],
    'MO' => [
        'name' => 'Altria Group',
        'country' => 'US'
    ],
    'AXP' => [
        'name' => 'American Express',
        'country' => 'US'
    ],
    'AIG' => [
        'name' => 'American International Group Inc',
        'country' => 'US'
    ],
    'T' => [
        'name' => 'AT&T Inc.',
        'country' => 'US'
    ],
    'BAC' => [
        'name' => 'Bank of America Corp',
        'country' => 'US'
    ],
    'BK' => [
        'name' => 'Bank of New York Mellon Corp',
        'country' => 'US'
    ],
    'BAX' => [
        'name' => 'Baxter International Inc.',
        'country' => 'US'
    ],
    'BRK.A' => [
        'name' => 'Berkshire Hathaway Inc. Class A',
        'country' => 'US'
    ],
    'BA' => [
        'name' => 'Boeing',
        'country' => 'US'
    ],
    'BMY' => [
        'name' => 'Bristol-Myers Squibb Co',
        'country' => 'US'
    ],
    'CCL' => [
        'name' => 'Carnival Corporation & plc',
        'country' => 'US'
    ],
    'CAT' => [
        'name' => 'Caterpillar',
        'country' => 'US'
    ],
    'CVX' => [
        'name' => 'Chevron',
        'country' => 'US'
    ],
    'C' => [
        'name' => 'Citigroup Inc',
        'country' => 'US'
    ],
    'CL' => [
        'name' => 'Colgate-Palmolive',
        'country' => 'US'
    ],
    'COP' => [
        'name' => 'ConocoPhillips',
        'country' => 'US'
    ],
    'GLW' => [
        'name' => 'Corning Incorporated',
        'country' => 'US'
    ],
    'CVS' => [
        'name' => 'CVS Health',
        'country' => 'US'
    ],
    'DE' => [
        'name' => 'Deere & Company',
        'country' => 'US'
    ],
    "DVN" => [
        'name' => 'Devon Energy',
        'country' => 'US'
    ],
    //"APHA" => [
    //    'name' => 'Aphria Inc',
    //    'country' =>  'US'
    //],
    "CGC" => [
        'name' => 'Canopy Growth Corporation',
        'country' => 'US'
    ],
    "ACB" => [
        'name' => 'Aurora Cannabis',
        'country' => 'US'
    ],
    "ZYNE" => [
        'name' => 'Zynerba Pharmaceuticals',
        'country' => 'US'
    ],
    "CARA" => [
        'name' => 'Cara Therapeutics',
        'country' => 'US'
    ],
    "CRBP" => [
        'name' => 'Corbus Pharmaceuticals',
        'country' => 'US'
    ],
    "AMC" => [
        'name' => 'AMC Entertainment Holdings Inc',
        'country' => 'US'
    ]
];
