<?php 
 return array (
  'AAPL' => 
  array (
    'name' => 'Apple Inc',
    'country' => 'US',
    'industry' => 'Technology',
    'active' => 1,
  ),
  'AMZN' => 
  array (
    'name' => 'Amazon.com Inc',
    'country' => 'US',
    'industry' => 'Retail',
    'active' => 1,
  ),
  'FB' => 
  array (
    'name' => 'Facebook Inc',
    'country' => 'US',
    'industry' => 'Media',
    'active' => 1,
  ),
  'MSFT' => 
  array (
    'name' => 'Microsoft Corp',
    'country' => 'US',
    'industry' => 'Technology',
    'active' => 1,
  ),
  'ADBE' => 
  array (
    'name' => 'Adobe Inc',
    'country' => 'US',
    'industry' => 'Technology',
    'active' => 1,
  ),
  'TSLA' => 
  array (
    'name' => 'Tesla Inc',
    'country' => 'US',
    'industry' => 'Automobiles',
    'active' => 1,
  ),
  'NVDA' => 
  array (
    'name' => 'NVIDIA Corp',
    'country' => 'US',
    'industry' => 'Semiconductors',
    'active' => 1,
  ),
  'NFLX' => 
  array (
    'name' => 'Netflix Inc',
    'country' => 'US',
    'industry' => 'Media',
    'active' => 1,
  ),
  'PYPL' => 
  array (
    'name' => 'PayPal Holdings Inc',
    'country' => 'US',
    'industry' => 'Technology',
    'active' => 1,
  ),
  'BABA' => 
  array (
    'name' => 'Alibaba Group Holding Ltd',
    'country' => 'US',
    'industry' => 'Internet Retail',
    'active' => 1,
  ),
  'INTC' => 
  array (
    'name' => 'Intel Corp',
    'country' => 'US',
    'industry' => 'Semiconductors',
    'active' => 1,
  ),
  'CMCSA' => 
  array (
    'name' => 'Comcast Corp',
    'country' => 'US',
    'industry' => 'Media',
    'active' => 1,
  ),
  'PEP' => 
  array (
    'name' => 'PepsiCo Inc',
    'country' => 'US',
    'industry' => 'Beverages',
    'active' => 1,
  ),
  'CSCO' => 
  array (
    'name' => 'Cisco Systems Inc',
    'country' => 'US',
    'industry' => 'Communications',
    'active' => 1,
  ),
  'GOOGL' => 
  array (
    'name' => 'Alphabet Inc',
    'country' => 'US',
    'industry' => 'Media',
    'active' => 1,
  ),
  'AMGN' => 
  array (
    'name' => 'Amgen Inc',
    'country' => 'US',
    'industry' => 'Biotechnology',
    'active' => 1,
  ),
  'AVGO' => 
  array (
    'name' => 'Broadcom Inc',
    'country' => 'US',
    'industry' => 'Semiconductors',
    'active' => 1,
  ),
  'COST' => 
  array (
    'name' => 'Costco Wholesale Corp',
    'country' => 'US',
    'industry' => 'Retail',
    'active' => 1,
  ),
  'QCOM' => 
  array (
    'name' => 'Qualcomm Inc',
    'country' => 'US',
    'industry' => 'Semiconductors',
    'active' => 1,
  ),
  'TMUS' => 
  array (
    'name' => 'T-Mobile US Inc',
    'country' => 'US',
    'industry' => 'Telecommunication',
    'active' => 1,
  ),
  'TXN' => 
  array (
    'name' => 'Texas Instruments Inc',
    'country' => 'US',
    'industry' => 'Semiconductors',
    'active' => 1,
  ),
  'CHTR' => 
  array (
    'name' => 'Charter Communications Inc',
    'country' => 'US',
    'industry' => 'Media',
    'active' => 1,
  ),
  'ADI' => 
  array (
    'name' => 'Analog Devices Inc',
    'country' => 'US',
    'industry' => 'Semiconductors',
    'active' => 1,
  ),
  'ADP' => 
  array (
    'name' => 'Automatic Data Processing Inc',
    'country' => 'US',
    'industry' => 'Technology',
    'active' => 1,
  ),
  'ADSK' => 
  array (
    'name' => 'Autodesk Inc',
    'country' => 'US',
    'industry' => 'Technology',
    'active' => 1,
  ),
  'ALGN' => 
  array (
    'name' => 'Align Technology Inc',
    'country' => 'US',
    'industry' => 'Health Care',
    'active' => 1,
  ),
  'ALXN' => 
  array (
    'name' => 'Alexion Pharmaceuticals Inc',
    'country' => 'US',
    'industry' => 'Biotechnology',
    'active' => 1,
  ),
  'AMAT' => 
  array (
    'name' => 'ApplUKd Materials Inc',
    'country' => 'US',
    'industry' => 'Semiconductors',
    'active' => 1,
  ),
  'AMD' => 
  array (
    'name' => 'Advanced Micro Devices Inc',
    'country' => 'US',
    'industry' => 'Semiconductors',
    'active' => 1,
  ),
  'ANSS' => 
  array (
    'name' => 'ANSYS Inc',
    'country' => 'US',
    'industry' => 'Technology',
    'active' => 1,
  ),
  'ATVI' => 
  array (
    'name' => 'Activision Blizzard Inc',
    'country' => 'US',
    'industry' => 'Media',
    'active' => 1,
  ),
  'BIIB' => 
  array (
    'name' => 'Biogen Inc',
    'country' => 'US',
    'industry' => 'Biotechnology',
    'active' => 1,
  ),
  'BKNG' => 
  array (
    'name' => 'Booking Holdings Inc',
    'country' => 'US',
    'industry' => 'Retail',
    'active' => 1,
  ),
  'BMRN' => 
  array (
    'name' => 'Biomarin Pharmaceutical Inc',
    'country' => 'US',
    'industry' => 'Biotechnology',
    'active' => 1,
  ),
  'CDNS' => 
  array (
    'name' => 'Cadence Design Systems Inc',
    'country' => 'US',
    'industry' => 'Technology',
    'active' => 1,
  ),
  'CDW' => 
  array (
    'name' => 'CDW Corp',
    'country' => 'US',
    'industry' => 'Electrical Equipment',
    'active' => 1,
  ),
  'CERN' => 
  array (
    'name' => 'Cerner Corp',
    'country' => 'US',
    'industry' => 'Health Care',
    'active' => 1,
  ),
  'CPRT' => 
  array (
    'name' => 'Copart Inc',
    'country' => 'US',
    'industry' => 'Commercial Services & SupplUKs',
    'active' => 1,
  ),
  'CSX' => 
  array (
    'name' => 'CSX Corp',
    'country' => 'US',
    'industry' => 'Road & Rail',
    'active' => 1,
  ),
  'CTAS' => 
  array (
    'name' => 'Cintas Corp',
    'country' => 'US',
    'industry' => 'Commercial Services & SupplUKs',
    'active' => 1,
  ),
  'CTSH' => 
  array (
    'name' => 'Cognizant Technology Solutions Corp',
    'country' => 'US',
    'industry' => 'Technology',
    'active' => 1,
  ),
  'CTXS' => 
  array (
    'name' => 'Citrix Systems Inc',
    'country' => 'US',
    'industry' => 'Technology',
    'active' => 1,
  ),
  'DLTR' => 
  array (
    'name' => 'Dollar Tree Inc',
    'country' => 'US',
    'industry' => 'Retail',
    'active' => 1,
  ),
  'DOCU' => 
  array (
    'name' => 'DocuSign Inc',
    'country' => 'US',
    'industry' => 'Technology',
    'active' => 1,
  ),
  'DXCM' => 
  array (
    'name' => 'DexCom Inc',
    'country' => 'US',
    'industry' => 'Health Care',
    'active' => 1,
  ),
  'EA' => 
  array (
    'name' => 'Electronic Arts Inc',
    'country' => 'US',
    'industry' => 'Media',
    'active' => 1,
  ),
  'EBAY' => 
  array (
    'name' => 'eBay Inc',
    'country' => 'US',
    'industry' => 'Retail',
    'active' => 1,
  ),
  'EXC' => 
  array (
    'name' => 'Exelon Corp',
    'country' => 'US',
    'industry' => 'UtilitUKs',
    'active' => 1,
  ),
  'EXPE' => 
  array (
    'name' => 'Expedia Group Inc',
    'country' => 'US',
    'industry' => 'Retail',
    'active' => 1,
  ),
  'FAST' => 
  array (
    'name' => 'Fastenal Co',
    'country' => 'US',
    'industry' => 'Trading CompanUKs & Distributors',
    'active' => 1,
  ),
  'FISV' => 
  array (
    'name' => 'Fiserv Inc',
    'country' => 'US',
    'industry' => 'Technology',
    'active' => 1,
  ),
  'FOXA' => 
  array (
    'name' => 'Fox Corp',
    'country' => 'US',
    'industry' => 'Media',
    'active' => 1,
  ),
  'GILD' => 
  array (
    'name' => 'Gilead ScUKnces Inc',
    'country' => 'US',
    'industry' => 'Biotechnology',
    'active' => 1,
  ),
  'IDXX' => 
  array (
    'name' => 'IDEXX LaboratorUKs Inc',
    'country' => 'US',
    'industry' => 'Health Care',
    'active' => 1,
  ),
  'ILMN' => 
  array (
    'name' => 'Illumina Inc',
    'country' => 'US',
    'industry' => 'Life ScUKnces Tools & Services',
    'active' => 1,
  ),
  'INCY' => 
  array (
    'name' => 'Incyte Corp',
    'country' => 'US',
    'industry' => 'Biotechnology',
    'active' => 1,
  ),
  'INTU' => 
  array (
    'name' => 'Intuit Inc',
    'country' => 'US',
    'industry' => 'Technology',
    'active' => 1,
  ),
  'ISRG' => 
  array (
    'name' => 'Intuitive Surgical Inc',
    'country' => 'US',
    'industry' => 'Health Care',
    'active' => 1,
  ),
  'KHC' => 
  array (
    'name' => 'Kraft Heinz Co',
    'country' => 'US',
    'industry' => 'Food Products',
    'active' => 1,
  ),
  'PRU' => 
  array (
    'name' => 'Prudential Financial Inc',
    'country' => 'US',
    'industry' => 'Insurance',
    'active' => 1,
  ),
  'RHHBY' => 
  array (
    'name' => 'Roche Holding',
    'country' => 'EU',
    'active' => 1,
  ),
  'VOD' => 
  array (
    'name' => 'Vodafone',
    'country' => 'UK',
    'active' => 1,
  ),
  'ACM' => 
  array (
    'name' => 'AECOM',
    'country' => 'US',
    'industry' => 'Construction',
    'active' => 1,
  ),
  'ADAP' => 
  array (
    'name' => 'Adaptimmune Therapeutics PLC',
    'country' => 'UK',
    'industry' => 'Biotechnology',
    'active' => 1,
  ),
  'AEF' => 
  array (
    'name' => 'Aberdeen Emerging Markets Equity Income Fund Inc',
    'country' => 'US',
    'industry' => 'N/A',
    'active' => 1,
  ),
  'AFMD' => 
  array (
    'name' => 'Affimed NV',
    'country' => 'EU',
    'industry' => 'Biotechnology',
    'active' => 1,
  ),
  'AKTX' => 
  array (
    'name' => 'Akari Therapeutics PLC',
    'country' => 'US',
    'industry' => 'Pharmaceuticals',
    'active' => 1,
  ),
  'ALKS' => 
  array (
    'name' => 'Alkermes Plc',
    'country' => 'UK',
    'industry' => 'Biotechnology',
    'active' => 1,
  ),
  'AUTL' => 
  array (
    'name' => 'Autolus Therapeutics PLC',
    'country' => 'UK',
    'industry' => 'Biotechnology',
    'active' => 1,
  ),
  'AY' => 
  array (
    'name' => 'Atlantica Sustainable Infrastructure PLC',
    'country' => 'UK',
    'industry' => 'Utilities',
    'active' => 1,
  ),
  'BCYC' => 
  array (
    'name' => 'Bicycle Therapeutics PLC',
    'country' => 'UK',
    'industry' => 'Biotechnology',
    'active' => 1,
  ),
  'GAM' => 
  array (
    'name' => 'General American Investors Co Inc',
    'country' => 'US',
    'industry' => 'N/A',
    'active' => 1,
  ),
  'GAN' => 
  array (
    'name' => 'GameAccount Network',
    'country' => 'UK',
    'active' => 1,
  ),
  'ACIU' => 
  array (
    'name' => 'AC Immune SA',
    'country' => 'EU',
    'active' => 1,
  ),
  'GSM' => 
  array (
    'name' => 'Ferroglobe PLC',
    'country' => 'UK',
    'industry' => 'Metals & Mining',
    'active' => 1,
  ),
  'HUD' => 
  array (
    'name' => 'Hudson Ltd',
    'country' => 'US',
    'industry' => 'Retail',
    'active' => 1,
  ),
  'LRCX' => 
  array (
    'name' => 'Lam Research Corp',
    'country' => 'US',
    'active' => 1,
  ),
  'MAR' => 
  array (
    'name' => 'Marriott International Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'MCHP' => 
  array (
    'name' => 'Microchip Technology Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'MDLZ' => 
  array (
    'name' => 'Mondelez International Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'MELI' => 
  array (
    'name' => 'Mercadolibre Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'MNST' => 
  array (
    'name' => 'Monster Beverage Corp',
    'country' => 'US',
    'active' => 1,
  ),
  'MRNA' => 
  array (
    'name' => 'Moderna Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'MU' => 
  array (
    'name' => 'Micron Technology Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'MXIM' => 
  array (
    'name' => 'Maxim Integrated Products Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'NXPI' => 
  array (
    'name' => 'NXP Semiconductors NV',
    'country' => 'EU',
    'active' => 1,
  ),
  'ORLY' => 
  array (
    'name' => 'O\'Reilly Automotive Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'PAYX' => 
  array (
    'name' => 'Paychex Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'PCAR' => 
  array (
    'name' => 'Paccar Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'REGN' => 
  array (
    'name' => 'Regeneron Pharmaceuticals Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'ROST' => 
  array (
    'name' => 'Ross Stores Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'SBUX' => 
  array (
    'name' => 'Starbucks Corp',
    'country' => 'US',
    'active' => 1,
  ),
  'SGEN' => 
  array (
    'name' => 'Seattle Genetics Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'SIRI' => 
  array (
    'name' => 'Sirius XM Holdings Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'SNPS' => 
  array (
    'name' => 'Synopsys Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'SPLK' => 
  array (
    'name' => 'Splunk Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'SWKS' => 
  array (
    'name' => 'Skyworks Solutions Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'TTWO' => 
  array (
    'name' => 'Take-Two Interactive Software Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'ULTA' => 
  array (
    'name' => 'Ulta Beauty Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'VRSK' => 
  array (
    'name' => 'Verisk Analytics Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'VRSN' => 
  array (
    'name' => 'Verisign Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'VRTX' => 
  array (
    'name' => 'Vertex Pharmaceuticals Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'WBA' => 
  array (
    'name' => 'Walgreens Boots Alliance Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'WDAY' => 
  array (
    'name' => 'Workday Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'WDC' => 
  array (
    'name' => 'Western Digital Corp',
    'country' => 'US',
    'active' => 1,
  ),
  'XEL' => 
  array (
    'name' => 'Xcel Energy Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'XLNX' => 
  array (
    'name' => 'Xilinx Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'ZM' => 
  array (
    'name' => 'Zoom Video Communications Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'KLAC' => 
  array (
    'name' => 'KLA Corp',
    'country' => 'US',
    'active' => 1,
  ),
  'LBTYK' => 
  array (
    'name' => 'Liberty Global Inc',
    'country' => 'UK',
    'active' => 1,
  ),
  'AAL' => 
  array (
    'name' => 'American Airlines Group Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'ACN' => 
  array (
    'name' => 'Accenture',
    'country' => 'UK',
    'active' => 1,
  ),
  'AZN' => 
  array (
    'name' => 'AstraZeneca',
    'country' => 'UK',
    'active' => 1,
  ),
  'BP' => 
  array (
    'name' => 'BP',
    'country' => 'UK',
    'active' => 1,
  ),
  'GSK' => 
  array (
    'name' => 'GlaxoSmithKline',
    'country' => 'UK',
    'active' => 1,
  ),
  'LRLCY' => 
  array (
    'name' => 'L’Oréal',
    'country' => 'EU',
    'active' => 1,
  ),
  'LVMUY' => 
  array (
    'name' => 'LVMH',
    'country' => 'EU',
    'active' => 1,
  ),
  'MDT' => 
  array (
    'name' => 'Medtronic',
    'country' => 'UK',
    'active' => 1,
  ),
  'NSRGY' => 
  array (
    'name' => 'Nestlé',
    'country' => 'EU',
    'active' => 1,
  ),
  'NVO' => 
  array (
    'name' => 'Novo Nordisk',
    'country' => 'EU',
    'active' => 1,
  ),
  'NVS' => 
  array (
    'name' => 'Novartis',
    'country' => 'EU',
    'active' => 1,
  ),
  'AMRN' => 
  array (
    'name' => 'Amarin Corporation',
    'country' => 'EU',
    'active' => 1,
  ),
  'CYAD' => 
  array (
    'name' => 'Celyad Oncology',
    'country' => 'EU',
    'active' => 1,
  ),
  'GLPG' => 
  array (
    'name' => 'Galapagos NV',
    'country' => 'EU',
    'active' => 1,
  ),
  'MTLS' => 
  array (
    'name' => 'Materialise NV',
    'country' => 'EU',
    'active' => 1,
  ),
  'ASND' => 
  array (
    'name' => 'Ascendis Pharma',
    'country' => 'EU',
    'active' => 1,
  ),
  'FWP' => 
  array (
    'name' => 'Forward Pharma',
    'country' => 'EU',
    'active' => 1,
  ),
  'ZEAL' => 
  array (
    'name' => 'Zealand Pharma',
    'country' => 'EU',
    'active' => 1,
  ),
  'CLLS' => 
  array (
    'name' => 'Cellectis',
    'country' => 'EU',
    'active' => 1,
  ),
  'CRTO' => 
  array (
    'name' => 'Criteo',
    'country' => 'EU',
    'active' => 1,
  ),
  'DBVT' => 
  array (
    'name' => 'DBV Technologies',
    'country' => 'EU',
    'active' => 1,
  ),
  'EDAP' => 
  array (
    'name' => 'EDAP TMS S.A.',
    'country' => 'EU',
    'active' => 1,
  ),
  'ERYP' => 
  array (
    'name' => 'ERYTECH Pharma S.A.',
    'country' => 'EU',
    'active' => 1,
  ),
  'GNFT' => 
  array (
    'name' => 'GENFIT',
    'country' => 'EU',
    'active' => 1,
  ),
  'SQNS' => 
  array (
    'name' => 'Sequans Communications',
    'country' => 'EU',
    'active' => 1,
  ),
  'BFRA' => 
  array (
    'name' => 'Biofrontera',
    'country' => 'EU',
    'active' => 1,
  ),
  'BNTX' => 
  array (
    'name' => 'BioNTech',
    'country' => 'EU',
    'active' => 1,
  ),
  'MOR' => 
  array (
    'name' => 'MorphoSys',
    'country' => 'EU',
    'active' => 1,
  ),
  'LOV' => 
  array (
    'name' => 'Spark Networks',
    'country' => 'EU',
    'active' => 1,
  ),
  'VJET' => 
  array (
    'name' => 'Voxeljet',
    'country' => 'EU',
    'active' => 1,
  ),
  'APA' => 
  array (
    'name' => 'Apache Corp.',
    'country' => 'US',
    'active' => 1,
  ),
  'CME' => 
  array (
    'name' => 'CME Group',
    'country' => 'US',
    'active' => 1,
  ),
  'MREO' => 
  array (
    'name' => 'Mereo BioPharma',
    'country' => 'UK',
    'active' => 1,
  ),
  'MTP' => 
  array (
    'name' => 'Midatech Pharma',
    'country' => 'UK',
    'active' => 1,
  ),
  'NCNA' => 
  array (
    'name' => 'NuCana',
    'country' => 'UK',
    'active' => 1,
  ),
  'ORTX' => 
  array (
    'name' => 'Orchard Therapeutics',
    'country' => 'UK',
    'active' => 1,
  ),
  'SMMT' => 
  array (
    'name' => 'Summit Therapeutics',
    'country' => 'UK',
    'active' => 1,
  ),
  'VRNA' => 
  array (
    'name' => 'Verona Pharma',
    'country' => 'UK',
    'active' => 1,
  ),
  'NOK' => 
  array (
    'name' => 'Nokia',
    'country' => 'EU',
    'active' => 1,
  ),
  'LYG' => 
  array (
    'name' => 'Lloyds Banking Group',
    'country' => 'UK',
    'active' => 1,
  ),
  'RDS.A' => 
  array (
    'name' => 'Royal Dutch Shell Class A',
    'country' => 'EU',
    'active' => 1,
  ),
  'DB' => 
  array (
    'name' => 'Deutsche Bank',
    'country' => 'EU',
    'active' => 1,
  ),
  'EQNR' => 
  array (
    'name' => 'Statoil (Equinor)',
    'country' => 'EU',
    'active' => 1,
  ),
  'MT' => 
  array (
    'name' => 'ArcelorMittal',
    'country' => 'EU',
    'active' => 1,
  ),
  'ING' => 
  array (
    'name' => 'ING (ING Group)',
    'country' => 'EU',
    'active' => 1,
  ),
  'TOT' => 
  array (
    'name' => 'Total (Total SE)',
    'country' => 'EU',
    'active' => 1,
  ),
  'BCS' => 
  array (
    'name' => 'Barclays',
    'country' => 'UK',
    'active' => 1,
  ),
  'BBVA' => 
  array (
    'name' => 'Banco Bilbao Vizcaya Argentaria',
    'country' => 'EU',
    'active' => 1,
  ),
  'BUD' => 
  array (
    'name' => 'Anheuser-Busch InBev (AB InBev)',
    'country' => 'EU',
    'active' => 1,
  ),
  'TS' => 
  array (
    'name' => 'Tenaris',
    'country' => 'EU',
    'active' => 1,
  ),
  'TEF' => 
  array (
    'name' => 'Telefónica',
    'country' => 'EU',
    'active' => 1,
  ),
  'ABB' => 
  array (
    'name' => 'ABB Ltd',
    'country' => 'EU',
    'active' => 1,
  ),
  'E' => 
  array (
    'name' => 'Eni',
    'country' => 'EU',
    'active' => 1,
  ),
  'DEO' => 
  array (
    'name' => 'Diageo',
    'country' => 'UK',
    'active' => 1,
  ),
  'PHG' => 
  array (
    'name' => 'Koninklijke Philips NV',
    'country' => 'EU',
    'active' => 1,
  ),
  'NGG' => 
  array (
    'name' => 'National Grid',
    'country' => 'UK',
    'active' => 1,
  ),
  'FMS' => 
  array (
    'name' => 'Fresenius Medical Care',
    'country' => 'EU',
    'active' => 1,
  ),
  'SAN' => 
  array (
    'name' => 'Banco Santander SA',
    'country' => 'EU',
    'active' => 1,
  ),
  'CRH' => 
  array (
    'name' => 'CRH PLC',
    'country' => 'EU',
    'active' => 1,
  ),
  'LIN' => 
  array (
    'name' => 'The Linde Group',
    'country' => 'EU',
    'active' => 1,
  ),
  'CS' => 
  array (
    'name' => 'Credit Suisse Group AG',
    'country' => 'EU',
    'active' => 1,
  ),
  'HSBC' => 
  array (
    'name' => 'HSBC Holdings plc',
    'country' => 'UK',
    'active' => 1,
  ),
  'ORAN' => 
  array (
    'name' => 'Orange',
    'country' => 'EU',
    'active' => 1,
  ),
  'PUK' => 
  array (
    'name' => 'Prudential PLC',
    'country' => 'UK',
    'active' => 1,
  ),
  'UBS' => 
  array (
    'name' => 'UBS',
    'country' => 'EU',
    'active' => 1,
  ),
  'RIO' => 
  array (
    'name' => 'Rio Tinto',
    'country' => 'UK',
    'active' => 1,
  ),
  'ALLE' => 
  array (
    'name' => 'Allegion PLC',
    'country' => 'EU',
    'active' => 1,
  ),
  'AON' => 
  array (
    'name' => 'Aon',
    'country' => 'UK',
    'active' => 1,
  ),
  'CCC' => 
  array (
    'name' => 'CLARIVATE PLC',
    'country' => 'UK',
    'active' => 1,
  ),
  'CCEP' => 
  array (
    'name' => 'Coca-Cola European Partners PLC',
    'country' => 'UK',
    'active' => 1,
  ),
  'CPRI' => 
  array (
    'name' => 'Capri Holdings',
    'country' => 'US',
    'active' => 1,
  ),
  'DAVA' => 
  array (
    'name' => 'Endava',
    'country' => 'UK',
    'active' => 1,
  ),
  'FTCH' => 
  array (
    'name' => 'Farfetch',
    'country' => 'UK',
    'active' => 1,
  ),
  'FTI' => 
  array (
    'name' => 'TechnipFMC',
    'country' => 'UK',
    'active' => 1,
  ),
  'SAP' => 
  array (
    'name' => 'SAP',
    'country' => 'EU',
    'active' => 1,
  ),
  'AER' => 
  array (
    'name' => 'AerCap',
    'country' => 'EU',
    'active' => 1,
  ),
  'AGRO' => 
  array (
    'name' => 'Adecoagro SA',
    'country' => 'EU',
    'active' => 1,
  ),
  'ALV' => 
  array (
    'name' => 'Autoliv',
    'country' => 'EU',
    'active' => 1,
  ),
  'BTI' => 
  array (
    'name' => 'British American Tobacco',
    'country' => 'UK',
    'active' => 1,
  ),
  'IHG' => 
  array (
    'name' => 'InterContinental Hotels Group',
    'country' => 'UK',
    'active' => 1,
  ),
  'PSO' => 
  array (
    'name' => 'Pearson PLC',
    'country' => 'UK',
    'active' => 1,
  ),
  'RELX' => 
  array (
    'name' => 'RELX Group',
    'country' => 'UK',
    'active' => 1,
  ),
  'SNN' => 
  array (
    'name' => 'Smith & Nephew',
    'country' => 'UK',
    'active' => 1,
  ),
  'MMM' => 
  array (
    'name' => '3M',
    'country' => 'US',
    'active' => 1,
  ),
  'ABT' => 
  array (
    'name' => 'Abbott Laboratories',
    'country' => 'US',
    'active' => 1,
  ),
  'AA' => 
  array (
    'name' => 'Alcoa',
    'country' => 'US',
    'active' => 1,
  ),
  'ALL' => 
  array (
    'name' => 'Allstate Corp',
    'country' => 'US',
    'active' => 1,
  ),
  'MO' => 
  array (
    'name' => 'Altria Group',
    'country' => 'US',
    'active' => 1,
  ),
  'AXP' => 
  array (
    'name' => 'American Express',
    'country' => 'US',
    'active' => 1,
  ),
  'AIG' => 
  array (
    'name' => 'American International Group Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'T' => 
  array (
    'name' => 'AT&T Inc.',
    'country' => 'US',
    'active' => 1,
  ),
  'BAC' => 
  array (
    'name' => 'Bank of America Corp',
    'country' => 'US',
    'active' => 1,
  ),
  'BK' => 
  array (
    'name' => 'Bank of New York Mellon Corp',
    'country' => 'US',
    'active' => 1,
  ),
  'BAX' => 
  array (
    'name' => 'Baxter International Inc.',
    'country' => 'US',
    'active' => 1,
  ),
  'BRK.A' => 
  array (
    'name' => 'Berkshire Hathaway Inc. Class A',
    'country' => 'US',
    'active' => 1,
  ),
  'BA' => 
  array (
    'name' => 'Boeing',
    'country' => 'US',
    'active' => 1,
  ),
  'BMY' => 
  array (
    'name' => 'Bristol-Myers Squibb Co',
    'country' => 'US',
    'active' => 1,
  ),
  'CCL' => 
  array (
    'name' => 'Carnival Corporation & plc',
    'country' => 'US',
    'active' => 1,
  ),
  'CAT' => 
  array (
    'name' => 'Caterpillar',
    'country' => 'US',
    'active' => 1,
  ),
  'CVX' => 
  array (
    'name' => 'Chevron',
    'country' => 'US',
    'active' => 1,
  ),
  'C' => 
  array (
    'name' => 'Citigroup Inc',
    'country' => 'US',
    'active' => 1,
  ),
  'CL' => 
  array (
    'name' => 'Colgate-Palmolive',
    'country' => 'US',
    'active' => 1,
  ),
  'COP' => 
  array (
    'name' => 'ConocoPhillips',
    'country' => 'US',
    'active' => 1,
  ),
  'GLW' => 
  array (
    'name' => 'Corning Incorporated',
    'country' => 'US',
    'active' => 1,
  ),
  'CVS' => 
  array (
    'name' => 'CVS Health',
    'country' => 'US',
    'active' => 1,
  ),
  'DE' => 
  array (
    'name' => 'Deere & Company',
    'country' => 'US',
    'active' => 1,
  ),
  'DVN' => 
  array (
    'name' => 'Devon Energy',
    'country' => 'US',
    'active' => 1,
  ),
  'CGC' => 
  array (
    'name' => 'Canopy Growth Corporation',
    'country' => 'US',
    'active' => 1,
  ),
  'ACB' => 
  array (
    'name' => 'Aurora Cannabis',
    'country' => 'US',
    'active' => 1,
  ),
  'ZYNE' => 
  array (
    'name' => 'Zynerba Pharmaceuticals',
    'country' => 'US',
    'active' => 1,
  ),
  'CARA' => 
  array (
    'name' => 'Cara Therapeutics',
    'country' => 'US',
    'active' => 1,
  ),
  'CRBP' => 
  array (
    'name' => 'Corbus Pharmaceuticals',
    'country' => 'US',
    'active' => 1,
  ),
  'AMC' => 
  array (
    'name' => 'AMC Entertainment Holdings Inc',
    'country' => 'US',
    'active' => 1,
  ),
);