<?php

return [
    'BTC' => [
        'name' => 'Bitcoin',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100'
            ],
        ]
    ],
    'BCH' => [
        'name' => 'Bitcoin Cash',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100'
            ],
            'BTC' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000'
            ],
        ]
    ],
    'BNB' => [
        'name' => 'Binance Coin',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100'
            ],
        ]
    ],
    'ETH' => [
        'name' => 'Ethereum',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100'
            ],
            'BTC' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000'
            ],
        ]
    ],
    'LTC' => [
        'name' => 'Litecoin',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100'
            ],
            'BTC' => [
                'minmov' => '0.001',
                'pricescale' => '100000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'XRP' => [
        'name' => 'Ripple',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'KSM' => [
        'name' => 'Kusama',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100'
            ],
        ]
    ],
    'KNC' => [
        'name' => 'Kyber Network',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.1',
                'pricescale' => '1000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'REP' => [
        'name' => 'Augur',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.1',
                'pricescale' => '1000'
            ],
            'BTC' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'XTZ' => [
        'name' => 'Tezos',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000'
            ],
            'BTC' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000'
            ],
        ]
    ],
    'XLM' => [
        'name' => 'Stellar',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'ZRX' => [
        'name' => '0rx',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'OMG' => [
        'name' => 'OMG Network',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000'
            ],
            'BTC' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'LINK' => [
        'name' => 'Chainlink',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.1',
                'pricescale' => '1000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'MKR' => [
        'name' => 'Maker',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100'
            ],
            'BTC' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000'
            ],
        ]
    ],
    'EOS' => [
        'name' => 'EOS',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000'
            ],
            'BTC' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'AION' => [
        'name' => 'AION',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'ADA' => [
        'name' => 'Cardano',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'TRX' => [
        'name' => 'TRON',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'XMR' => [
        'name' => 'Monero',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100'
            ],
            'BTC' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'NEO' => [
        'name' => 'NEO',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100'
            ],
            'BTC' => [
                'minmov' => '0.001',
                'pricescale' => '100000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'ATOM' => [
        'name' => 'Cosmos',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '1000'
            ],
            'BTC' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000'
            ],
        ]
    ],
    'ANT' => [
        'name' => 'Aragon',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000'
            ],
            'BTC' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000'
            ],
        ]
    ],
    'ARDR' => [
        'name' => 'Ardor',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
        ]
    ],
    'BEAM' => [
        'name' => 'Beam',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000'
            ],
            'BTC' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000'
            ],
        ]
    ],
    'BLZ' => [
        'name' => 'Bluzelle',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'BNT' => [
        'name' => 'Bancor',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'BTS' => [
        'name' => 'BitShares',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
        ]
    ],
    'BTT' => [
        'name' => 'BitTorrent Token',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000'
            ],
        ]
    ],
    'CVC' => [
        'name' => 'Civic',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'DATA' => [
        'name' => 'Streamr DATAcoin',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => ''
            ],
        ]
    ],
    'DCR' => [
        'name' => 'Decred',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.1',
                'pricescale' => '1000'
            ],
            'BTC' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000'
            ],
        ]
    ],
    'DENT' => [
        'name' => 'Dent',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000'
            ],
        ]
    ],
    'DGB' => [
        'name' => 'DigiByte',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
        ]
    ],
    'DOCK' => [
        'name' => 'Dock',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000'
            ],
        ]
    ],
    'DOGE' => [
        'name' => 'Dogecoin',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000'
            ],
        ]
    ],
    'ENJ' => [
        'name' => 'Enjin Coin',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
            'ETH' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
        ]
    ],
    'FUN' => [
        'name' => 'FunFair',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000'
            ],
            'ETH' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
        ]
    ],
    'GTO' => [
        'name' => 'Gifto',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000'
            ],
        ]
    ],
    'GXS' => [
        'name' => 'GXChain',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
            'ETH' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000'
            ],
        ]
    ],
    'ICX' => [
        'name' => 'Icon',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000'
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
            'ETH' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000'
            ],
        ]
    ],
    'IOST' => [
        'name' => 'IOST',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000'
            ],
            'ETH' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
        ]
    ],
    'IOTX' => [
        'name' => 'IoTeX',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000'
            ],
            'ETH' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000'
            ],
        ]
    ],
    'ETC' => [
        'name' => 'Ethereum Classic',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000'
            ],
            'BTC' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000'
            ],
            'ETH' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000'
            ],
        ]
    ],
    'DASH' => [
        'name' => 'Dash',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100',
            ],
            'BTC' => [
                'minmov' => '0.001',
                'pricescale' => '100000',
            ]
        ]
    ],
    'SHIB' => [
        'name' => 'SHIBA INU',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000'
            ],
        ]
    ]
];
