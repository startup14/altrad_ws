FROM php:7.4-apache
COPY . /var/www/html/amazon_trade/
WORKDIR /var/www/html/amazon_trade/
RUN set -ex; \
  savedAptMark="$(apt-mark showmanual)"; \
  apt-get update; \
  apt-get install -y --no-install-recommends \
    libjpeg-dev \
    libpng-dev \
    libzip-dev \
    libbz2-dev \
    libcurl4-openssl-dev \
    libsqlite3-dev \
    libmemcached-dev \
    libonig-dev \
    unzip \
    wget \
    git \
    supervisor \
    openssl \
    vim less nano \
  && \
    openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
     -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt \
     -subj "/C=UK/ST=Warwickshire/L=Leamington/O=OrgName/OU=IT Department/CN=api.coinmarketsolutions.io" && \
    openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048 \
  && \
  echo docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr; \
  debMultiarch="$(dpkg-architecture --query DEB_BUILD_MULTIARCH)"; \
  docker-php-ext-install -j "$(nproc)" \
    mysqli \
    pdo_mysql \
    pdo_sqlite \
    bz2 \
    curl \
    fileinfo \
    gd \
    mbstring \
    exif \
;\
  wget https://raw.githubusercontent.com/composer/getcomposer.org/76a7060ccb93902cd7576b67264ad91c8a2700e2/web/installer \
    -O - -q | php -- --filename=composer --install-dir=/usr/bin ; \
  apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
  rm -rf /var/lib/apt/lists/*

RUN pecl install -o -f redis \
  &&  rm -rf /tmp/pear \
  &&  docker-php-ext-enable redis

RUN mv supervisor /etc/supervisor.d && \
    echo 'files = /etc/supervisor.d/*.ini' >> /etc/supervisor/supervisord.conf

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
		echo 'max_execution_time = 7200'; \
        echo 'memory_limit = 128M'; \
		echo 'max_input_time = 7200';\
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

RUN a2enmod rewrite expires ;\
    a2enmod proxy ;\
    a2enmod proxy_http ;\
    a2enmod proxy_wstunnel ;\
    a2enmod proxy_balancer ;\
    a2enmod ssl

RUN composer install
RUN chown www-data:www-data /var/www/html/ -R

EXPOSE 443

CMD supervisord; apache2-foreground
