<?php

return [
    'saving_account_closed' => [
        'heading' => 'Saving account has expired!',
        'text1' => 'Dear client,',
        'text2' => 'We inform your Saving account has expired.',
        'text3' => 'The deposited amount and interest rate will be credited to your trading account on',
        'text4' => 'Thank you for your trust!'
    ],

    'saving_account_create' => [
        'heading' => 'Successful deposit!',
        'text1' => 'Dear client,',
        'text2' => 'Congratulations on opening your Saving Account!',
        'list1' => 'Opening date of Saving Account',
        'list2' => 'Saving account terms',
        'list3' => 'Closing date of Saving Account',
        'list4' => 'The amount',
        'list5' => 'Interest rate',
        'months_until' => 'months until',
        'per_month' => 'per month',
        'text3' => 'The depositer amount and interested rate will be credited to your trading account at the end of the term.',
        'text4' => 'Thank you for your trust!'
    ]
];
