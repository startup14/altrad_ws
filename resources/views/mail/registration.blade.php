<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
<table align="center" border="0" class="m_-3345404828790214225mlEmailContainer" cellpadding="0" cellspacing="0"
       width="100%" style="border-collapse:collapse" bgcolor="#131722">
    <tbody>
    <tr>
        <td align="center">
            <table align="center" width="640" class="m_-3345404828790214225mlContentTable" cellpadding="0"
                   cellspacing="0" border="0" style="min-width:640px;width:640px">
                <tbody>
                <tr>
                    <td class="m_-3345404828790214225mlContentTable">
                        <table width="640" class="m_-3345404828790214225mlContentTable" cellspacing="0"
                               cellpadding="0" border="0" bgcolor="#2ECC71" align="center" style="width:640px">
                            <tbody>
                            <tr>
                                <td class="m_-3345404828790214225mlContentImage">
                                    <a style="border:none;display:block"
                                       href="https://coinmarketsolutions.io/"
                                       target="_blank">
                                        <img border="0"
                                             src="https://api.coinmarketsolutions.io/registration.jpg"
                                             width="640" alt="" style="display:block" class="CToWUd">
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" border="0" bgcolor="#1B2535" class="m_-3345404828790214225mlContentTable"
                   cellspacing="0" cellpadding="0" style=";width:640px" width="640">
                <tbody>
                <tr>
                    <td class="m_-3345404828790214225mlContentTable" height="35"></td>
                </tr>
                </tbody>
            </table>
            <table align="center" border="0" bgcolor="#1B2535" class="m_-3345404828790214225mlContentTable"
                   cellspacing="0" cellpadding="0" style=";min-width:640px;width:640px" width="640"
                   id="m_-3345404828790214225ml-block-158923106">
                <tbody>
                <tr>
                    <td>
                        <table width="640" class="m_-3345404828790214225mlContentTable" bgcolor="#1B2535"
                               cellspacing="0" cellpadding="0" border="0" align="center" style="width:640px">
                            <tbody>
                            <tr>
                                <td align="center" class="m_-3345404828790214225mlContentContainer"
                                    style="padding:5px 50px 5px 50px">
                                    <h1
                                        style="margin:0px;font-family:Helvetica;font-weight:bold;font-size:34px;text-decoration:none;line-height:40px;color:#ffba0b">
                                        You trading account has been successfully registered in our
                                        system! </h1>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" border="0" bgcolor="#1B2535" class="m_-3345404828790214225mlContentTable"
                   cellspacing="0" cellpadding="0" style=";min-width:640px;width:640px" width="640"
                   id="m_-3345404828790214225ml-block-158923088">
                <tbody>
                <tr>
                    <td>
                        <table width="640" class="m_-3345404828790214225mlContentTable" bgcolor="#1B2535"
                               cellspacing="0" cellpadding="0" border="0" align="center" style=";width:640px">
                            <tbody>
                            <tr>
                                <td align="left" class="m_-3345404828790214225mlContentContainer"
                                    style="padding:15px 50px 5px 50px;font-family:Helvetica;font-size:16px;color:#fff;line-height:27px">
                                    <p style="margin:0px 0px 10px 0px;line-height:27px">
                                        Dear {{$user->first_name . ' ' . $user->last_name}},</p>
                                    <p> Congratulations on opening a trading account with
                                        CoinMarketSolutions.
                                    </p>
                                    <p>Your trading account has been successfully registered in our system.</p>
                                    <p>These are details to log in into your trading account</p>
                                    <p>Login: <a style="color: white !important;">{{$user->email}}</a></p>
                                    <p>Password: {{$password}}</p>
                                    <p>We wish you a good journey in trading.</p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" border="0" bgcolor="#1B2535" class="m_-3345404828790214225mlContentTable"
                   cellspacing="0" cellpadding="0" style=";min-width:640px;width:640px" width="640"
                   id="m_-3345404828790214225ml-block-158923124">
                <tbody>
                <tr>
                    <td>
                        <table width="640" class="m_-3345404828790214225mlContentTable" bgcolor="#1B2535"
                               cellspacing="0" cellpadding="0" border="0" align="center" style=";width:640px">
                            <tbody>
                            <tr>
                                <td width="540" class="m_-3345404828790214225mlContentContainer"
                                    cellspacing="0" cellpadding="0" border="0"
                                    style="padding:0px 0px 0px 50px">
                                    <table width="540" class="m_-3345404828790214225mlContentBlock"
                                           cellspacing="0" cellpadding="0" border="0" align="left"
                                           style="width:540px">
                                        <tbody>
                                        <tr>
                                            <td class="m_-3345404828790214225mlContentButton"
                                                align="center" style="padding:15px 0px 15px 0px">

                                                <a href="https://trade.coinmarketsolutions.io/login"
                                                   style="background-color:#03A658;color:#ffffff;font-family:Helvetica;font-size:15px;line-height:21px;display:inline-block;border-radius:17px;text-align:center;text-decoration:none;font-weight:bold;display:block;margin:0px 0px;padding:10px 0px;width:240px"
                                                   target="_blank">
                                                    Trade </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" border="0" bgcolor="#1B2535" class="m_-3345404828790214225mlContentTable"
                   cellspacing="0" cellpadding="0" style=";min-width:640px;width:640px" width="640"
                   id="m_-3345404828790214225ml-block-158923127">
                <tbody>
                <tr>
                    <td>
                        <table width="640" class="m_-3345404828790214225mlContentTable" bgcolor="#1B2535"
                               cellspacing="0" cellpadding="0" border="0" align="center" style=";width:640px">
                            <tbody>
                            <tr>
                                <td class="m_-3345404828790214225mlContentContainer"
                                    style="padding:15px 50px 0px 50px">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0"
                                           style="border-collapse:initial;border-top:1px solid #d8d8d8">
                                        <tbody>
                                        <tr>
                                            <td width="100%" height="15" style="line-height:15px">
                                                <img
                                                    src="https://ci4.googleusercontent.com/proxy/nynQ2flAsBB2agg4dglrKRSgvZ5REzNrYCLZ_8DA-p5FxL767GXpuGDkamST5HYATyYy7bR-D5XIr39SFPmeYsP3h_qa=s0-d-e1-ft#https://bucket.mlcdn.com/images/default/spacer.gif"
                                                    width="1" height="1" style="display:block"
                                                    alt="" class="CToWUd">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" border="0" bgcolor="#1B2535" class="m_-3345404828790214225mlContentTable"
                   cellspacing="0" cellpadding="0" style=";min-width:640px;width:640px" width="640"
                   id="m_-3345404828790214225ml-block-158923100">
                <tbody>
                <tr>
                    <td>
                        <table width="640" class="m_-3345404828790214225mlContentTable" bgcolor="#1B2535"
                               cellspacing="0" cellpadding="0" border="0" align="center" style=";width:640px">
                            <tbody>
                            <tr>
                                <td align="left" class="m_-3345404828790214225mlContentContainer"
                                    style="padding:15px 50px 5px 50px;font-family:Helvetica;font-size:16px;color:#fff;line-height:27px">
                                    <p style="margin:0px 0px 10px 0px;line-height:27px">CoinMarketSolutions
                                        team</p>
                                    <p><a href="mailto:support@coinmarketsolutions.io" style="color:#fff">support@coinmarketsolutions.io</a>
                                    </p>
                                    <p><a href="tel:+442034816297" style="color:#fff;text-decoration:none">tel:
                                            +442034816297</a></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" border="0" bgcolor="#1B2535" class="m_-3345404828790214225mlContentTable"
                   cellspacing="0" cellpadding="0" style=";width:640px" width="640">
                <tbody>
                <tr>
                    <td style="min-width:640px;width:640px" class="m_-3345404828790214225mlContentTable"
                        height="35" width="640"></td>
                </tr>
                </tbody>
            </table>


        </td>
    </tr>
    </tbody>
</table>
</body>

</html>
