<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Saving Account Created</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&amp;display=swap" rel="stylesheet">
</head>
<body style="margin: 0;
        font-family: 'Google Sans', sans-serif;">
<div id="mail" style="background-color: #292D38;">
    <div class="container" style="background-color:#161E29;width:700px;position:relative;padding-bottom: 200px;margin:auto;">
        <img src="{{ env('APP_URL') }}/mail-cover.png" alt="cover img" style="width: 100%"><div id="content" style="padding: 0 4rem;">
            <h1 style="margin: 2.5rem auto;
        width: fit-content;
        color: #fff;
        font-size: 24px;
        font-weight: 900;">{{ __('mails.saving_account_create.heading') }}</h1>

            <p style="color:#fff;font-size:16px;margin:5px;font-weight:300;">{{ __('mails.saving_account_create.text1') }}</p>
            <p style="color:#fff;font-size:16px;margin:5px;font-weight:300;">{{ __('mails.saving_account_create.text2') }}</p>

            <div class="block" style="margin:2rem 0;">
                <p style="color:#fff;font-size:16px;margin:5px;font-weight:300;">- {{ __('mails.saving_account_create.list1') }}: <b>{{\Carbon\Carbon::parse($sa['created_at'])->toDateString()}}</b></p>
                <p style="color:#fff;font-size:16px;margin:5px;font-weight:300;">- {{ __('mails.saving_account_create.list2') }}: <b>{{$sa['period']}} {{ __('mails.saving_account_create.months_until') }} {{\Carbon\Carbon::parse($sa['close_date'])->toDateString()}}</b></p>
                <p style="color:#fff;font-size:16px;margin:5px;font-weight:300;">- {{ __('mails.saving_account_create.list3') }}: <b>{{\Carbon\Carbon::parse($sa['close_date'])->toDateString()}}</b></p>
                <p style="color:#fff;font-size:16px;margin:5px;font-weight:300;">- {{ __('mails.saving_account_create.list4') }}: <b>{{$sa['sum']}} {{strtoupper($sa['unit'])}}</b></p>
                <p style="color:#fff;font-size:16px;margin:5px;font-weight:300;">- {{ __('mails.saving_account_create.list5') }}: <b>{{$sa['percent']}}% {{ __('mails.saving_account_create.per_month') }}</b></p>
            </div>

            <div class="block" style="margin:2rem 0;">
                <p style="color:#fff;font-size:16px;margin:5px;font-weight:300;">{{ __('mails.saving_account_create.text3') }}</p>
            </div>

            <div class="block" style="margin:2rem 0;">
                <p style="color:#fff;font-size:16px;margin:5px;font-weight:300;">{{ __('mails.saving_account_create.text4') }}</p>
            </div>
        </div>

        <div id="footer" style="padding:0 4rem;margin-top:0;">
            <span style="margin:5px 0;color:#fff;font-size:13px;font-weight:400;">CoinMarketSolutions team</span>
            <div class="group" style="margin:5px 0;display:flex;justify-content:center;width:fit-content;align-items:center;">
                <img src="{{ env('APP_URL') }}/mail_icon.png" alt="mail icon" style="margin-right: 10px;"><a href="mailto:support@coinmarketsolutions.io" style="color:#fff;font-size:13px;font-weight:400;">support@coinmarketsolutions.io</a>
            </div>
            <div class="group" style="margin:5px 0;display:flex;justify-content:center;width:fit-content;align-items:center;">
                <img src="{{ env('APP_URL') }}/phone_icon.png" alt="mail icon" style="margin-right: 10px;"><a href="tel:+442034816297" style="color:#fff;font-size:13px;font-weight:400;">tel: +442034816297</a>
            </div>
        </div>

        <img id="charts-img" src="{{ env('APP_URL') }}/mail-charts.png" style="position: absolute;bottom: 0;left: 100px;" alt="charts background">

    </div>
</div>
</body>
</html>
