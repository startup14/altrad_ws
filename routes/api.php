<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->middleware('api')->group(function () {
    Route::get('currency-rate', 'CurrencyRateController');
    Route::get('currency-book', 'CurrencyBookController');
    Route::get('signals', 'GetSignalsController');
    Route::get('online-info', 'OnlineInfoController'); // todo: method not found in Controller
    Route::get('klines', 'HistoricalDataController');
    Route::get('exchangeInfo', 'ExchangeInfoController');

    Route::get('available-actives', 'AvailableActivesController'); // usage not found in Ronins-front
    Route::post('available-actives-periods', 'AvailableActivesWithPeriodsDataController'); // usage not found in Ronins-front

    Route::get('repl/{cmd}/{params?}', 'ReplController@console');
});

Route::group([
    'middleware' => 'api',
], function () {
    Route::post('available-actives-daily', 'ApiController@getAvailableActivesWithDailyData'); // usage not found in Ronins-front
    // following routes use no Redis, migration not required
    Route::get('deposit/check', 'UserController@checkOrderCapitalist'); // todo: method not found in Controller
    Route::get('users/online', 'UserController@getOnlineUsers');
    Route::get('users/id-by-email', 'ApiController@getUserIdByEmail');
    Route::post('check-order-status', 'UserController@checkOrderStatus');
    Route::put('account/{email}/change-status', 'ApiController@accountStatus');
    Route::post('reset-password', 'UserController@resetPassword');
    Route::get('users-online', 'ApiController@usersOnline');
    Route::get('copy-traders', 'ApiController@copyTraders');
    Route::get('copy-trader-history', 'ApiController@copyTraderOrderHistory');
    Route::get('time', 'ApiController@serverTime');
    Route::get('crm-copy-traders', 'ApiController@crmCopyTraders');
    Route::get('styles', 'ApiController@requestStyle');
    Route::get('pamm-accounts', 'ApiController@pammAccounts');
    Route::get('pamm-info', 'ApiController@pammInfo');
    Route::get('robots', 'ApiController@robots');
    Route::get('robot-info', 'ApiController@robotInfo');
    Route::get('settings/dynamic-spread', 'ApiController@getDynamicSpreadSettings');
    Route::post('settings/dynamic-spread/update', 'ApiController@updateDynamicSpreadSettings');
    Route::post('settings/update-setting', 'ApiController@updateSettings');
    //Chat-bot endpoints
    Route::get('chatbot/clients/{client_id}/positions', 'ChatBotApiController@clientPositions');
    Route::post('chatbot/clients/{client_id}/activation-quote', 'ChatBotApiController@activationQuote');
    Route::get('chatbot/clients/{client_id}/account-status', 'ChatBotApiController@getAccountStatus');
    Route::post('chatbot/clients/{client_id}/account-status', 'ChatBotApiController@changeAccountStatus');
    Route::get('chatbot/clients/{clientid}/list-of-banned-quotes', 'ChatBotApiController@listOfBlockStocks');


    // Currency List
    Route::get('currency-list/get', 'ApiController@getCurrencyList');
    Route::post('currency-list/update', 'ApiController@updateCurrencyStatus');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::post('register', 'JWTAuthController@register');
    Route::post('login', 'JWTAuthController@login')->name('login');
    Route::delete('logout', 'JWTAuthController@logout');
    Route::delete('session', 'JWTAuthController@endSession');
    Route::post('refresh', 'JWTAuthController@refresh');
    Route::get('profile', 'JWTAuthController@profile');
    Route::post('user/profile/update', 'UserController@updateProfile');
    Route::get('user/history', 'UserController@userHistory');
    Route::post('deposit', 'UserController@deposit');
    Route::post('deposit/quickcard', 'UserController@depositQuickCard');
    Route::post('deposit/m-capital', 'UserController@depositMcapital');
    Route::post('deposit/realisto', 'UserController@realisto');
    Route::get('deposit/realisto/status/failed', 'UserController@failedRealiteco');
    Route::get('deposit/realisto/status/success', 'UserController@successRealiteco');
    Route::post('deposit/enot', 'UserController@enotPayment');
    Route::get('/deposit/enot/callback', 'UserController@enotCallBackUrl');
    Route::post('user/withdraw/create', 'UserController@createWithdraw');
    Route::post('user/withdraw/canceled', 'UserController@canceledWithdraw');
    Route::put('user/withdraw/update', 'UserController@updateWithdraw');

    Route::get('user/details', 'TradingController@getAllUserInfo'); // redised
    Route::post('trading/positions/delete', 'TradingController@deletePosition'); // redised
    Route::get('trading/positions/get', 'TradingController@getPositions'); // no redis
    Route::post('trading/positions/create', 'TradingController@createPosition'); // redised
    Route::post('trading/positions/cancel', 'TradingController@cancelPosition'); // no redis
    Route::get('trading/orders/all', 'TradingController@getAllOrders'); // no redis
    Route::post('trading/orders/transfer', 'TradingController@transferOrderToPosition'); // no redis
    Route::post('trading/orders/create', 'TradingController@createOrder'); // no redis
    Route::delete('trading/orders/{id}/delete', 'TradingController@deleteOrder'); // no redis
    Route::put('trading/orders/{id}/update', 'TradingController@updateOrder'); // not found!!!
    Route::get('trading/orders/get', 'TradingController@getOrders'); // no redis
    Route::get('trading/orders/history', 'TradingController@ordersHistory'); // no redis
    Route::put('trading/positions/{id}/update', 'TradingController@updatePosition'); // no redis
    Route::put('trading/copy-traders/follow', 'TradingController@followTrader'); // no redis
    Route::put('trading/copy-traders/unfollow', 'TradingController@unfollowTrader'); // no redis
    Route::post('broker/trading/position/super-delete', 'TradingController@superDelete');
    Route::post('broker/trading/position/update', 'TradingController@brokerPositionUpdate');
    Route::post('broker/trading/position/delete', 'TradingController@brokerPositionDelete');
    Route::post('broker/trading/history/delete', 'TradingController@brokerHistoryDelete');
    Route::post('broker/trading/history/restore', 'TradingController@brokerHistoryRestore');
    Route::post('broker/trading/order/delete', 'TradingController@brokerOrderDelete');
    Route::get('broker/trading/history/preview', 'TradingController@brokerHistoryMarginCallPreview');
    Route::post('broker/trading/history/preview/confirm', 'TradingController@brokerHistoryMarginCallPreviewConfirm');

    Route::post('debt/create', 'UserController@createDebtRequest');
    Route::post('debt/approve', 'UserController@approveDebtRequest');
    Route::post('saving-account/create', 'UserController@savingAccountCreate');
    Route::get('saving-account/all', 'UserController@getAllSavingAccounts');
    Route::post('saving-account/withdraw', 'UserController@withdrawSavingMoney');
    Route::post('trading-bots/settings/update', 'ProductController@updateTradingRobotSettings');
    Route::get('trading-bots/info', 'ProductController@getTradingRobotInformation');
    Route::get('trading-bots/chart-period', 'ProductController@getTradingRobotChartForPeriod');

    //Broker ENDPOINTS
//    Route::get('broker/users/all', 'ApiController@getAllUsers');
    Route::get('broker/user/{email}/details', 'ApiController@getUserInformation');
    Route::put('broker/user/{email}/details/update', 'ApiController@brokerUpdateInfo');
    Route::post('broker/user/{email}/currency-fee/set', 'ApiController@setCurrencyFee');
    Route::post('broker/user/{email}/currency-fee/remove', 'ApiController@removeCurrencyFee');

    //Manipulation Trade
    Route::put('broker/trading/history/{id}/delete', 'ApiController@historyDelete');

    Route::put('agent/user/{email}/details/update', 'ApiController@updateDetails');
    Route::put('support/user/balance/update', 'ApiController@updateBalance');
    Route::put('support/user/{email}/details/update', 'ApiController@updateInfo');
    Route::post('support/user/credit/create', 'ApiController@createCredit');
    Route::post('admin/users/import', 'ApiController@importUsers');

    // Banned Actives
    Route::post('blocked-actives/user/{email}', 'UserController@updateUserBannedActives');

    // Anonymize users
    Route::put('user/{email}/anonymize', 'ApiController@anonymize');
});
