<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return abort(404);
});

//Route::get('/mail', function() {
//    $user = \App\User::where('email', 'second.client@dd.cc')->first();
//
//    $sa = [
//      'id' => 2,
//      'user_id' => 2,
//      'sum' => 150,
//      'period' => 3,
//      'doc' => null,
//      'status_approve' => 'in_process',
//      'type' => 'income',
//      'percent' => 2,
//      'close_date' => '2021-06-04',
//      'status' => 'active',
//      'unit' => 'usdt',
//      'rate_before' => 1,
//      'rate_after' => null,
//      'created_at' => '2021-03-04 16:39:50',
//      'updated_at' => '2021-03-04 16:39:50'
//    ];
//
//    \Illuminate\Support\Facades\Mail::to('archyzero@gmail.com')->send(new \App\Mail\SavingCreated($sa));
//});
//
Route::get('/preview', function() {
    $sa = [
        'id' => 2,
        'user_id' => 2,
        'sum' => 150,
        'period' => 3,
        'doc' => null,
        'status_approve' => 'in_process',
        'type' => 'income',
        'percent' => 2,
        'close_date' => '2021-06-04',
        'status' => 'active',
        'unit' => 'usdt',
        'rate_before' => 1,
        'rate_after' => null,
        'created_at' => '2021-03-04 16:39:50',
        'updated_at' => '2021-03-04 16:39:50'
    ];
   return view('mail.saving_close', compact('sa'));
});
