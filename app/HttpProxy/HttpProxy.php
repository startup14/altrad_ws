<?php

namespace App\HttpProxy;

use Illuminate\Http\Client\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

final class HttpProxy implements HttpInterface
{
    public const DEFAULT_ERROR_CODE = 500;

    public static function get($url, $data = [], $onError = null)
    {
        return self::send('get', $url, $data, $onError);
    }

    public static function post($url, $data = [], $onError = null)
    {
        return self::send('post', $url, $data, $onError);
    }

    public static function patch($url, $data = [], $onError = null)
    {
        return self::send('patch', $url, $data, $onError);
    }

    protected static function send($method, $url, $data = [], $onError = null)
    {
        for ($i=3; $i--;) {
            try {
                $response = Http::{$method}($url, $data);
                self::checkStatusCode($response);
                break;
            } catch (\Exception $e) {
                $exception = $e;
                sleep(1);
                continue;
            }
        }

        if (isset($exception)) {
            if (isset($onError)) {
                $onError($exception);
            } else {
                self::onError($exception, $url, $data);
            }
        } else {
            return $response->json();
        }
    }

    protected static function onError(\Exception $e, $url, $data): JsonResponse
    {
        $code    = $e->getCode() ?: self::DEFAULT_ERROR_CODE;
        $message = $e->getMessage();

        return self::handleException(new \Exception($message, $code), $url, $data);
    }

    protected static function handleException(\Exception $e, $url, $data): JsonResponse
    {
        self::errorReportForBot($e, $url, $data);

        return response()->json([
            'code'    => $e->getCode(),
            'message' => $e->getMessage(),
        ]);
    }

    protected static function checkStatusCode(Response $response): void
    {
        // Determine if the status code was >= 400
        $sc1 = $response->failed();
        // Determine if the response has a 400 level status code
        $sc2 = $response->clientError();
        // Determine if the response has a 500 level status code
        $sc3 = $response->serverError();

        if ($sc1 || $sc2 || $sc3) {
            $response->throw();
        }
    }

    protected static function errorReportForBot($error, $url, $data): void
    {
        //$errorMessage = json_decode($error->content(), true, 512, JSON_THROW_ON_ERROR)['message'];
        $errorMessage = $error->getMessage();
        $debugBacktrace = json_encode(debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 2), JSON_THROW_ON_ERROR);
        $user = auth()->user() ?: null;
        $email = $user->email ?? self::getEmail($url);

        $userData = json_encode($data);

        info('error', ['errorMessage' => $errorMessage, 'debugBacktrace' => $debugBacktrace,]);

        info("Some user with email {$email} try to perform some action. But this attempt was failed. User data that was failed to execution: {$userData}");

        //$botUrlForError = env('APP_URL') . '/api/chatbot/http-error'; # todo: send error report to Telegram bot

        //self::post($botUrlForError, ['errorMessage' => $errorMessage, 'debugBacktrace' => $debugBacktrace,]);
    }

    protected static function getEmail($url)
    {
        $uri_path = $url;
        $uri_segments = explode('/', $uri_path);

        if (Str::contains($uri_path, $uri_segments)) {
            $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';

            foreach ($uri_segments as $uri_segment) {
                if (preg_match($pattern, $uri_segment) === 1) {
                    $emailPart = $uri_segment;
                }
            }

            $email = $emailPart;
        } else {
            $email = 'There is no email data';
        }

        return $email;
    }
}
