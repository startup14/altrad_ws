<?php

namespace App\HttpProxy;

interface HttpInterface
{
    public static function get(string $url, array $data = [], $onError = null);

    public static function post(string $url, array $data = [], $onError = null);

    public static function patch(string $url, array $data = [], $onError = null);
}
