<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Contracts\CryptoProviderInterface;
use App\Contracts\StocksProviderInterface;
use App\Contracts\CommoditiesProviderInterface;

use App\QuoteProviders\Polygon\PolygonAPI;
use App\QuoteProviders\TwelveData\TwelveDataAPI;
//use App\QuoteProviders\CryptoCompare\CryptoCompareAPI;
use App\QuoteProviders\Binance\BinanceAPI;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(CryptoProviderInterface::class, BinanceAPI::class);
        //$this->app->bind(CryptoProviderInterface::class, CryptoCompareAPI::class);
        $this->app->bind(StocksProviderInterface::class, TwelveDataAPI::class);
        $this->app->bind(CommoditiesProviderInterface::class, PolygonAPI::class);
    }
}
