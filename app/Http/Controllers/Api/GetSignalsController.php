<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class GetSignalsController
{
    public function __invoke(): array
    {
        $_keys = redis_keys_by_raw_pattern('*' . REDIS_STREAM_SIGNALS . '*');
        $currencies = Cache::get(REDIS_STREAM_SIGNALS);
        $time = Carbon::now()->addHour()->minutes(00)->seconds(00)->diffInSeconds(Carbon::now());

        return compact ('currencies', 'time', '_keys');
    }
}
