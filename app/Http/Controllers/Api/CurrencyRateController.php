<?php

namespace App\Http\Controllers\Api;


use App\ClientBannedActive;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CurrencyRateController
{
    /**
     * @throws \JsonException
     */
    public function __invoke(): array
    {
        $banned = [];

        $crypto = fallback_crypto(redis_hgetall_for_keyspace(REDIS_STREAM_CRYPTO), $banned);
        $commodities = fallback_commodities(redis_hgetall_for_keyspace(REDIS_STREAM_COMMO), $banned);
        $stocks = fallback_stocks(redis_hgetall_for_keyspace(REDIS_STREAM_STOCKS), $banned);

        return compact('crypto', 'commodities', 'stocks');
    }
}
