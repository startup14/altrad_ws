<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class OnlineInfoController
{
    public function __invoke(Request $request): array
    {
        return [
            'device' => Cache::get('device'),
            'online' => User::where('online', '1')->count('id'),
        ];

    }
}
