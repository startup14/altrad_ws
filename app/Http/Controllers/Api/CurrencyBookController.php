<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CurrencyBookController
{
    public function __invoke(Request $request): array
    {
        $type = $request->get('type');
        $fsym = $request->get('fsym');
        $tsym = $request->get('tsym');

        $orders_key = "orders:$fsym";
        $trades_key = "trades:$fsym";

        if ($type === 'crypto') {
            $orders_key .= ":$tsym";
            $trades_key .= ":$tsym";
        }

        $orders = Cache::get($orders_key);
        $trades = Cache::get($trades_key);

        return compact('orders', 'trades');
    }
}
