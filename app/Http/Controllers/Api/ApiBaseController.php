<?php

namespace App\Http\Controllers\Api;

use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ApiBaseController
{
    public static function warningResponse(string $message, int $code = ResponseAlias::HTTP_NOT_FOUND): array
    {
        return compact('code', 'message');
    }
}
