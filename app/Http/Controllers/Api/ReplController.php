<?php

namespace App\Http\Controllers\Api;

use Exception;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class ReplController
{
    public function console(string $cmd, ?string $params = null)
    {
        $cmd = str_replace('_', ':', $cmd);
        if ($params) {
            $cmd = sprintf("%s %s", $cmd, $params);
        }
        try {
            echo '<code>';
            Artisan::call($cmd, []);
            echo '</code>';
        } catch (Exception $e) {
            Log::error($e->getMessage());
            $code = $e->getCode();
            $message = $e->getMessage();
            $class = get_class($e);
            return compact('code', 'message', 'class');
        }

        return '';
    }
}
