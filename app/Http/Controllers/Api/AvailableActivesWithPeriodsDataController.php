<?php

namespace App\Http\Controllers\Api;

use App\Contracts\CryptoProviderInterface as CryptoProvider;
use App\HistoricalData;
use App\QuoteProviders\AlphaVantage\AlphaVantageAPI;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AvailableActivesWithPeriodsDataController
{
    private function changesCrypto(array $ticker): array
    {
        $response = [];

        $startTime = Carbon::now()->subMonths(6)->getPreciseTimestamp(3);

        $provider = app(CryptoProvider::class);
        $kline = $provider
            ->interval('1d')
            ->startTime($startTime)
            ->getKline("{$ticker}USDT");

        foreach ($kline as $data) {
            $response[] = $data['close'];
        }

        return $response;
    }

    private function changesStock(array $ticker): array
    {
        $response = [];

        $provider = app(AlphaVantageAPI::class);
        $kline = $provider->getKline($ticker, '1d');

        $timestamp = Carbon::now()->subMonths(6)->getPreciseTimestamp(3);

        foreach ($kline as $line => $data) {
            if ($data['open_time'] <= $timestamp) {
                break;
            }
            $response[] = $data['adj_close'];
        }

        return $response;
    }

    private function fixPeriod($period)
    {
        return $period ? round(($period->open / $period->close - 1) * 100, 2) : 0;
    }

    private function getPeriodChanges($ticker, $ticker_name, $price, $group): array
    {
        $hour = $day = $week = null;
        $var_and_periods = ['hour' => '1h', 'day' => '1d', 'week' => '1w'];

        foreach ($var_and_periods as $var_name => $period) {
            $$var_name = HistoricalData::where('time_period', $period)
                ->where('symbol', 'like', "$ticker%")
                ->latest('updated_at')
                ->first();
            $$var_name = $this->fixPeriod($$var_name);
        }

        $months_change = [];

        $changesMethod = sprintf('changes%s', ucfirst($group));
        if (method_exists($this, $changesMethod)) {
            $group_changes = $this->$changesMethod($ticker);
            $months_change = array_merge($months_change, $group_changes);
        }

        return [
            'name' => $ticker_name,
            'price' => $price,
            'hour_change' => $hour,
            '24h_change' => $day,
            '7d_change' => $week,
            '6M_prices' => $months_change,
            'type' => $group,
        ];
    }

    public function __invoke(Request $request): array
    {
        $tickers = $request->get('tickers');

        $crypto = redis_hgetall_for_keyspace('crypto');
        $stocks = redis_hgetall_for_keyspace('stocks');

        $data = [];

        foreach ($tickers as $ticker) {
            if ($ticker['type'] === 'crypto') {
                if (!array_key_exists($ticker['ticker'], $crypto)) {
                    continue;
                }

                $crypto_ticker = $crypto[$ticker['ticker']];
                $data[] = $this->getPeriodChanges(
                    $ticker['ticker'], $crypto_ticker['name'],
                    $crypto_ticker['quotes']['USDT']['p'], $ticker['type']);
            } elseif ($ticker['type'] === 'stock') {
                if (!array_key_exists($ticker['ticker'], $stocks)) {
                    continue;
                }
                $stock_ticker = $stocks[$ticker['ticker']];
                $data[] = $this->getPeriodChanges($ticker['ticker'], $stock_ticker['name'], $stock_ticker['p'], false);
            }
        }

        return $data;
    }
}
