<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;

// todo: Strategy Pattern for extract methods

final class ExchangeInfoController
{
    private const DEFAULT_QUOTE = 'USDT';

    private function extractCryptoSymbols(string $template): array
    {
        $source = include storage_path("symbols/$template");
        $result = [];
        foreach ($source as $key => $fields) {
            if (!$fields['active']) {
                continue;
            }
            [$base, $quote] = explode(REDIS_KEY_SEPARATOR, $key);
            $result[] = [
                'baseAsset' => $base,
                'quoteAsset' => $quote,
                'symbol' => $base . $quote,
                'minmov' => $fields['minmov'],
                'pricescale' => $fields['pricescale']
            ];
        }
        return $result;
    }

    private function extractStocksSymbols(string $template): array
    {
        $source = include storage_path("symbols/$template");
        $result = [];
        foreach ($source as $key => $fields) {
            if (!$fields['active']) {
                continue;
            }
            $result[] = [
                'baseAsset' => $key,
                'quoteAsset' => self::DEFAULT_QUOTE,
                'symbol' => $key,
                'minmov' => '1',
                'pricescale' => '100'
            ];
        }
        return $result;
    }

    private function extractCommoditiesSymbols(string $template): array
    {
        $source = include storage_path("symbols/$template");
        $result = [];
        foreach ($source as $key => $fields) {
            if (!$fields['active']) {
                continue;
            }
            $result[] = [
                'baseAsset' => $key,
                'quoteAsset' => self::DEFAULT_QUOTE,
                'symbol' => $key,
                'minmov' => '1',
                'pricescale' => '100'
            ];
        }
        return $result;
    }

    public function __invoke(Request $request): array
    {
        $symbols = [];
        $markets = [];

        $keyspaces = [
            REDIS_STREAM_CRYPTO => 'crypto-v2.php',
            REDIS_STREAM_STOCKS => 'stocks-v2.php',
            REDIS_STREAM_COMMO => 'commodities-v2.php',
        ];

        foreach ($keyspaces as $keyspace => $template) {
            $markets[] = $keyspace;
            $extractMethod = sprintf('extract%sSymbols', ucfirst($keyspace));
            $more = $this->$extractMethod($template);
            array_push($symbols, ...$more);
        }

        return [
            'serverTime' => Carbon::now()->timestamp . '000',
            'timezone' => 'UTC',
            'markets' => $markets,
            'symbols' => $symbols
        ];

    }
}
