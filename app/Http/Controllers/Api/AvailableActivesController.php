<?php

namespace App\Http\Controllers\Api;


class AvailableActivesController
{
    public function __invoke(): array
    {
        $response = [];

        $groups = ['stocks' => 'stock', 'crypto' => 'crypto'];

        foreach ($groups as $group => $type) {
            $data = redis_hgetall_for_keyspace($group);
            foreach ($data as $redis_key => $fields) {
                $symbol = redis_key_to_base_and_quote($redis_key)['base'];
                $response[] = [
                    'name' => $fields['name'],
                    'ticker' => $symbol,
                    'type' => $type,
                ];
            }
        }

        return $response;
    }
}
