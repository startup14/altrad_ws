<?php

namespace App\Http\Controllers\Api;

use App\Api\Contracts\KlinesProvider;
use App\Api\Tasks\GetHistoryForKlines;
use App\Api\Transformers\HistoryDataTransformer;
use App\Api\Transformers\HistoryKlineTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;

final class HistoricalDataController extends ApiBaseController
{
    private function guessKeyspace(string $symbol): string
    {
        return redis_guess_keyspace($symbol);
    }

    private function klinesProvider(string $type): KlinesProvider
    {
        $provider = "App\Api\Tasks\KLines" . ucfirst($type);
        return (new $provider);
    }

    public function __invoke(Request $request): array
    {
        $symbol = $request->query('symbol');
        $interval = $request->query('interval');
        $base = $request->query('base');
        $from_ts = $request->query('ts');

        $type = $this->guessKeyspace($base);
        if (!$type) {
            return ApiBaseController::warningResponse("Cant determine symbol type [$symbol, $base]");
        }

        $data = [];
        $from_ts_2 = null;

        if (is_null($from_ts)) {
            $data = (new GetHistoryForKlines())->grab(
                        $symbol, $interval, (new HistoryDataTransformer)
                    );
            $from_ts = empty($data) ? Carbon::now()->timestamp . '000' : $data[0][0];

            if ($type === 'commodities') {
                // wtf: ?
                if (empty($data)) {
                    $from_ts = Carbon::now()->format('Y-m-d');
                } else {
                    $from_ts = Carbon::createFromTimestampMs($data[0][0])->format('Y-m-d');
                    $from_ts_2 = $data[0][0];
                }
            }
        }

        --$from_ts;

        $klines = $this->klinesProvider($type)
            ->klines($symbol, $interval, [$from_ts, $from_ts_2], (new HistoryKlineTransformer()));

        if (array_key_exists('code', $klines)) {
            // code field in Response returns when error occurs
            return $klines;
        }

        $data = array_merge($data, $klines);

        $keys = array_map(static fn ($val) => $val[0], $data);

        array_multisort($keys, $data);

        return $data;
    }
}
