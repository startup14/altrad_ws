<?php

namespace App\Http\Controllers;

use App\ClientBannedActive;
use App\CurrencyList;
use App\Positions;
use App\User;
use Illuminate\Http\Request;

class ChatBotApiController extends Controller
{
    private $token = 'token';

    public function clientPositions(Request $request, $client_id)
    {
        $token = $request->get('token');

        if($token != $this->token) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid token'
            ],500);
        }

        $positions = Positions::where('user_id', $client_id)->get();

        return response()->json([
            'success' => true,
            'message' => $positions
        ],200);
    }

    public function activationQuote(Request $request)
    {
        if($request->data['token'] != $this->token) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid token'
            ],500);
        }
        $clientId = $request->data['client_id'];
        $quote = $request->data['quote'];
        $action = $request->data['action'];
        if($action == 'deactivate') {
            $findQuote = CurrencyList::where('slug', $quote)->first();
            if(!$findQuote) {
                return response()->json([
                    'success' => true,
                    'message' => "There is no quote in list!"
                ],200);
            }
            $findBannedQuote = ClientBannedActive::where('pair',$findQuote->slug)->where('user_id', $clientId)->first();
            if($findBannedQuote) {
                return response()->json([
                    'success' => true,
                    'message' => "This quote in bucket!"
                ],200);
            }

            ClientBannedActive::create([
                'user_id' => $clientId,
                'pair' => $quote
            ]);
            return response()->json([
                'success' => true,
                'message' => "deactivated $quote"
            ],200);
        } elseif($action == 'activate') {
            $findQuote = CurrencyList::where('slug', $quote)->first();
            if(!$findQuote) {
                return response()->json([
                    'success' => false,
                    'message' => "This quote is not disabled!"
                ],200);
            }
            $ban = ClientBannedActive::where('pair', $findQuote->slug)->where('user_id', $clientId)->first();
            if(!$ban) {
                return response()->json([
                    'success' => false,
                    'message' => "This quote is not disabled!"
                ],200);
            }
            $ban->delete();
            return response()->json([
                'success' => true,
                'message' => "activated $quote"
            ],200);
        }
    }

    public function getAccountStatus(Request $request, $client_id)
    {
        if($request->token != $this->token) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid token'
            ],500);
        }

        $user = User::find($client_id);
        $status = '';

        switch ($user->account_status) {
            case 'can_trade':
              $status = 'Can trade without rules';
                break;
            case 'can_deposit':
               $status = 'Only can deposit';
                break;
            case 'pending':
                $status = 'Account on verification';
                break;
            case 'can_trade_weekly':
                $status = 'Can trade weekly';
                break;
        }

        return response()->json([
            'success' => true,
            'message' => $status
        ],200);

    }

    public function changeAccountStatus(Request $request)
    {
        if($request->data['token'] != $this->token) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid token'
            ],500);
        }

        $statuses = ['can_deposit', 'can_trade', 'can_trade_weekly', 'pending'];
        $status = $request->data['status'];
        $client_id = $request->data['client_id'];

        $user = User::find($client_id);

        if(!in_array($status, $statuses)) {
            return response()->json([
                'success' => false,
                'message' => 'Wrong status!'
            ],200);
        }

        if(!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User doesn`t exiest!'
            ],200);
        }

        $user->update(['account_status' => $status]);

        return response()->json([
            'success' => true,
            'message' => " $status"
        ],200);
    }

    public function listOfBlockStocks(Request $request, $client_id)
    {
        if($request->token != $this->token) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid token'
            ],500);
        }

        $bannedStocks = ClientBannedActive::where('user_id', $client_id)->get();
        $data = [];
        if(!count($bannedStocks)) {
            return response()->json([
                'success' => false,
                'message' => 'Empty'
            ]);
        }

        foreach ($bannedStocks as $stock) {
            $blockedStocks =  CurrencyList::where('slug', $stock->pair)->get();
            foreach ($blockedStocks as $item) {
                $data[] = $item->slug;
            }
        }

        return response()->json([
            'success' => true,
            'message' => $data
        ]);
    }

}
