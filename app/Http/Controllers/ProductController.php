<?php

namespace App\Http\Controllers;

use App\BoughtTradingBot;
use App\OrdersHistory;
use App\RobotSetting;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductController extends Controller
{
    public function updateTradingRobotSettings(Request $request)
    {
        try {
            $settings = RobotSetting::where('bought_bot_id', $request->bot_id)->first();
        } catch (ModelNotFoundException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'No such trading bot'
            ]);
        }

        $settings->fill($request->all())->save();

        return response()->json([
            'success' => true,
            'changes' => $settings->getChanges()
        ]);
    }

    public function getTradingRobotInformation(Request $request)
    {
        if (auth()->check()){
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }

        try {
            $trading_bot = BoughtTradingBot::where('user_id', $user->id)->where('bot_id', $request->bot_id)->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'No such trading bot'
            ]);
        }

        $trading_bot->settings = RobotSetting::where('bought_bot_id', $request->bot_id)->first();
        $trading_bot->chart = OrdersHistory::where('user_id', $user->id)->where('description', $trading_bot->id)->selectRaw("SUM(front_balance - amount) as profit, DATE_FORMAT(created_at, '%Y-%m') as date)")->groupBy('date')->get();
        return $trading_bot;
    }

    public function getTradingRobotChartForPeriod(Request $request)
    {
        if (auth()->check()){
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }

        try {
            $trading_bot = BoughtTradingBot::where('user_id', $user->id)->where('bot_id', $request->bot_id)->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'No such trading bot'
            ]);
        }

        return OrdersHistory::where('user_id', $user->id)->where('description', $trading_bot->id)->whereBetween('created_at', [Carbon::createFromTimestamp($request->from), Carbon::createFromTimestamp($request->to)])->selectRaw("SUM(front_balance - amount) as profit, DATE_FORMAT(created_at, '%Y-%m') as date)")->groupBy('date')->get();
    }
}
