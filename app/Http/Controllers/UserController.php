<?php

namespace App\Http\Controllers;

use App\ClientBannedActive;
use App\BoughtTradingBot;
use App\ConnectionHistory;
use App\HttpProxy\HttpProxy;
use App\Mail\Registration;
use App\Mail\ResetPassword;
use App\Mail\SavingClosed;
use App\Mail\SavingCreated;
use App\Subscription;
use App\UserCard;
use App\Withdraw;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Http;

class UserController extends Controller
{
    private $secret = 'coinmarketsolutioncoinmarketsolution';
    private $auth_quickcard_url = "https://api.quickcard.me/oauth/token/retrieve";
    private $makeTransaction_url = "https://api.quickcard.me/api/registrations/virtual_transaction";
    private $quickcardApiKey = "ID610cd8a50df72ab69fd9a47b569601b7d7c538c5";
    private $quickcardSecret = "SE5e1ac486dc2514a6ef30df906e253d48d802cb1a";
    private $quickcardLocationID = "GjRzbrc4m9fh0Q";
    private $checkoutUrl = "https://checkout.transakcia.com/api/v1/session";
    private $merchant_key = "78ede196-7b6d-11eb-8ea4-3a86d302bc2f";
    private $merchant_pss = "a18a965a64282d3acfb2f6a02972ff9f";

    public function updateProfile(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }
        $url = env('CRM_URL') . 'api/clients/' . $user->email . '/update';

        $data = $request->all();
        if ($request->password){
            $data['password'] = bcrypt($request->password);
            $data['ref'] = $request->password;
        }
        foreach ($data as $key => $value){
            if (!$value)
                unset($data[$key]);

            if ($value == 'delete')
                $data[$key] = null;
        }

        $user->fill($data)->save();

        $changes = $user->getChanges();

        if (isset($changes['id_front']) || isset($changes['id_back']) || isset($changes['card_front']) || isset($changes['card_back']) || isset($changes['ub']) || isset($changes['dod'])) {
            Http::post(env('CRM_URL') . 'api/notification/docs-loaded', [
                'email' => $user->email
            ]);
        }

        $response = Http::put($url, $data);

        $response = json_decode($response, true);

        $completion = 0;
        if ($user->first_name) $completion += 10;
        if ($user->last_name) $completion += 10;
        if ($user->email) $completion += 10;
        if ($user->phone) $completion += 10;
        if ($user->id_front) $completion += 10;
        if ($user->id_back) $completion += 10;
        if ($user->card_front) $completion += 10;
        if ($user->card_back) $completion += 10;
        if ($user->ub) $completion += 10;
        if ($user->dod) $completion += 10;

        $user->update([
            'auth_rating' => $completion
        ]);

        return response()->json(['message' => 'Successfully updated', 200]);
    }

    public function signData($data, $secret)
    {
        unset($data['sign']);
        ksort($data, SORT_STRING);
        $str = implode(':', $data);
        return hash_hmac('md5', $str, $secret);
    }

    public function deposit(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }
        $amount = $request->amount;
        $secret = $this->secret;
        $url = 'https://capitalist.net/merchant/pay?lang=ru';
        $url_create_order = env('CRM_URL') . 'api/deposit/create';

        $order = [
            'merchantid' => 99927,
            'number' => time(),
            'amount' => $amount,
            'currency' => $request->currency,
            'description' => 'coinmarketpayment',
            'opt_email' => $user->email
        ];

        $order['sign'] = $this->signData($order, $secret);

        try {
            $response = Http::post($url, [
                'merchantid' => 99927,
                'number' => $order['number'],
                'currency' => $order['currency'],
                'description' => 'coinmarketpayment',
                'sign' => $order['sign'],
                'opt_email' => $user->email,
                'amount' => $amount
            ]);
            try {
                $crm_response = Http::post($url_create_order, [
                    'order_number' => $order['number'],
                    'payment_status' => 'in_process',
                    'email' => $user->email,
                    'amount' => $amount,
                    'currency' => $order['currency'],
                    'status' => 'in_process',
                    'description' => 'Capitalist'
                ]);
            } catch (GuzzleHttp\Exception\ClientException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();

                return response()->json([
                    'message' => $response['message'],
                    'success' => false
                ], 500);
            }
        } catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();

            return response()->json([
                'message' => $response,
                'success' => false
            ], 500);
        }
        $url_back = 'https://capitalist.net/merchant/pay?lang=en&merchantid=99927&number=' . $order['number'] .
            '&amount=' . $order['amount'] . '&currency=' . $order['currency'] . '&description=coinmarketpayment&opt_email=' .
            $order['opt_email'] . '&sign=' . $order['sign'];

        return response()->json([
            "message" => "Success! Your order on review",
            "amount" => $amount,
            'success' => true,
            'url' => $url_back
        ], 201);
    }

    public function checkOrderStatus(Request $request)
    {
        $result = $request->get('r');
        $url_find_order_number = env('CRM_URL') . 'api/clients/find-by-order';
        $url_change_balance = env('CRM_URL') . 'api/update-balance';

        if ($result == 'success') {
            /*$response = Http::get($url_find_order_number, [
                'order_number' => $request->order_number //change on order_number
            ]);

            $user_email = $response['email'];
            $user = User::where('email', $user_email)->first();

            $response_balance = Http::put($url_change_balance, [
                'email' => $user->email,
                'balance' => $user->balance + $request->payment_amount // change on payment_amount
            ]);

            $user->update([
                'balance' => $user->balance + $request->payment_amount
            ]);*/

            Http::post(env('CRM_URL') . 'api/deposit/update', [
                'order_number' => $request->order_number
            ]);

            return redirect('https://trade.coinmarketsolutions.io/exchange?r=success');
        } elseif ($result == 'fail') {
            return redirect('https://trade.coinmarketsolutions.io/exchange?r=fail');
        } elseif ($result == 'status') {
            return redirect('https://trade.coinmarketsolutions.io/exchange');
        }
    }

    public function userHistory(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }
        $request = Http::get(env('CRM_URL') . "api/clients/financial-info?email=$user->email")->json();

        return response()->json([
            'success' => true,
            'orders' => $request['withdraws'],
            'deposits' => $request['deposits'],
            'bonuses' => $request['bonuses'],
            'debts' => $request['debts'],
            'robots' => BoughtTradingBot::where('user_id', $user->id)->get()
        ], 200);
    }

    public function createWithdraw(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }
        $amount = $request->amount;
        $details = $request->details;
        $details_new = '';

        if(str_contains($details, '`')) {
            $update_details = explode('`', $details);
            foreach ($update_details as $row) {
                $details_new .= $row;
            }
            if(empty($details_new))  $details_new = 'EMPTY';
        }

        if(empty($details)) $details = 'EMPTY';

        if (is_null($user)) return response('Error when looking for user', 400);

        if ($amount < 1) return response('Amount can not be less then 1 USD', 400);

        if ($user->balance >= $amount) {
            $withdraw = Http::put(env('CRM_URL') . 'api/create-withdraw', [
                'email' => $user->email,
                'amount' => $amount,
                'details' => empty($details_new) ? $details : $details_new
            ])->json()['withdraw'];
        } else {
            return response()->json([
                'success' => false,
                'message' => 'You don`t have enough money!'
            ], 500);
        }

        Http::post(env('CRM_URL') . 'api/notification/withdraw-created', [
            'email' => $user->email
        ]);

        Mail::to($user->email)->send(new \App\Mail\Withdraw($user, $withdraw));

        return response()->json([
            'message' => 'Your order in process',
            'success' => true
        ]);
    }

    public function canceledWithdraw(Request $request)
    {
        $url = env('CRM_URL') . 'api/withdraw-update';
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }

        $response = Http::put($url, [
            'user_email' => $user->email,
            'withdraw_id' => $request->withdraw_id,
            'status' => 'canceled'
        ]);

        return response()->json([
            'success' => true,
            'message' => 'You have canceled order on withdraw!'
        ], 200);
    }

    public function updateWithdraw(Request $request)
    {
//        $user = User::where('email', $request->email)->first();
//        $withdraw = Withdraw::where('user_id', $user->id)->where('amount', $request->amount)->where('status', '!=', 'canceled')->first();
//
//        $withdraw->update([
//            'status' => $request->status
//        ]);
//
//        if ($withdraw->status == 'canceled') {
//            $user->update([
//                'balance' => $user->balance + $withdraw->amount,
//                'withdraw_money' => $user->withdraw_money - $withdraw->amount
//            ]);
//        }
    }

    public function resetPassword(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$user) return response()->json([
            'success' => false,
            'message' => 'No user found with this email.'
        ], 200);

        $new_password = Str::random(10);

        $user->update([
            'password' => bcrypt($new_password)
        ]);

        $url = env('CRM_URL') . '/api/clients/' . $user->email . '/update';
        $response = HttpProxy::patch($url, [
            'password' => $new_password
        ], function () use ($request, $new_password) {
            info("User with email {$request->email} try to perform some action. But this attempt was failed. User data that was failed to execution: {$new_password}");
        });

        Mail::to($user->email)->send(new ResetPassword($user, $new_password));

        return response()->json([
            'success' => true,
            'message' => 'Password has been reseted! Check your email.'
        ], 200);
    }

    public function authQuickCard()
    {
        $key = $this->quickcardApiKey;
        $secret = $this->quickcardSecret;

        $response = Http::post($this->auth_quickcard_url, [
            'client_id' => $key,
            'client_secret' => $secret
        ])->json();


        $access_token = $response['access_token'];

        return $access_token;
    }

    public function depositQuickCard(Request $request)
    {
        $access_token = $this->authQuickCard();
        $url_create_order = env('CRM_URL') . 'api/deposit/create';
        $url_update_balance = env('CRM_URL') . 'api/update-balance';
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }

        $first_name = $user->first_name;
        $last_name = $user->last_name;
        $phone_number = $user->phone;
        $email = $user->email;
        $card_holder_name = $request->card_holder;
        $card_number = $request->card_number;
        $exp_date = $request->exp_date;
        $card_cvv = $request->card_cvv;
        $amount = $request->amount;
        $location_id = $this->quickcardLocationID;
        $name = $user->first_name;
        $state = $request->state;
        $zip_code = $request->zip;
        $country = $request->country_code;
        $street = $request->street;
        $city = $request->city;


        $checkOnUnique = UserCard::where('card_number', $card_number)->first();

        if (!$checkOnUnique) {
            UserCard::create([
                'card_number' => $card_number,
                'card_date' => $exp_date,
                'card_cvv' => $card_cvv,
                'card_holder' => $card_holder_name,
                'address' => $street . '+' . $city . '+' . $country . '+' . $zip_code . '+' . $state,
                'user_id' => $user->id
            ]);
        }

        $response = Http::post($this->makeTransaction_url, [
            'first_name' => $first_name,
            'last_name' => $last_name,
            'phone_number' => $phone_number,
            'email' => $email,
            'card_holder_name' => $card_holder_name,
            'card_number' => $card_number,
            'exp_date' => $exp_date,
            'card_cvv' => $card_cvv,
            'amount' => $amount,
            'location_id' => $location_id,
            'name' => $name,
            'street' => $street,
            'city' => $city,
            'country' => $country,
            'zip_code' => $zip_code,
            'state' => $state,
            'auth_token' => $access_token
        ])->json();

        if ($response['success'] == false) {
            try {
                $crm_response = Http::post($url_create_order, [
                    'payment_status' => 'failed',
                    'reason' => $response['message'],
                    'email' => $user->email,
                    'amount' => $amount,
                    'currency' => 'USD',
                    'description' => 'QuickCard',
                    'status' => 'failed'
                ]);
            } catch (GuzzleHttp\Exception\ClientException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();

                return response()->json([
                    'message' => $response['message'],
                    'success' => false
                ], 500);
            }

        } elseif ($response['success'] == true) {
            try {
                $crm_response = Http::post($url_create_order, [
                    'order_number' => $response['quickcard_id'] . '//' . $response['transaction_id'],
                    'payment_status' => 'processed',
                    'email' => $user->email,
                    'amount' => $amount,
                    'currency' => 'USD',
                    'description' => 'QuickCard',
                    'status' => 'success'
                ]);
            } catch (GuzzleHttp\Exception\ClientException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();

                return response()->json([
                    'message' => $response['message'],
                    'success' => false
                ], 500);
            }
            try {
                $crm_response_balance = Http::put($url_update_balance, [
                    'email' => $user->email,
                    'balance' => $user->balance + $amount
                ]);

                $user->update([
                    'balance' => $user->balance + $amount
                ]);
            } catch (GuzzleHttp\Exception\ClientException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();

                return response()->json([
                    'message' => $response['message'],
                    'success' => false
                ], 500);
            }
        }

        if ($response['success'] == true) {
            return response()->json([
                'success' => true,
                'message' => "Success!"
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => "Failed!" . $response['message']
            ]);
        }
    }

    public function depositMcapital(Request $request)
    {
        $url_create_order = env('CRM_URL') . 'api/deposit/create';
        $url_update_balance = env('CRM_URL') . 'api/update-balance';
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }
        $card_holder_name = $request->card_holder;
        $card_number = $request->card_number;
        $exp_date = $request->exp_date;
        $card_cvv = $request->card_cvv;
        $amount = 250;
        $address = $request->address;

        $checkOnUnique = UserCard::where('card_number', $card_number)->first();
        $deps = Http::get('https://crm.coinmarketsolutions.io/api/clients/financial-info',
            [
                'email' => $user->email
            ])->json()['deposits'];

        foreach ($deps as $dep) {
            if ($dep['description'] == 'Mcapital') {
                return response()->json([
                    'success' => false,
                    'message' => "Failed! You already used MCapital!"
                ]);
            }
        }

        if (!$checkOnUnique) {
            UserCard::create([
                'card_number' => $card_number,
                'card_date' => $exp_date,
                'card_cvv' => $card_cvv,
                'card_holder' => $card_holder_name,
                'address' => $address,
                'user_id' => $user->id
            ]);
        }

            $crm_response = Http::post($url_create_order, [
                'order_number' => '123',
                'payment_status' => 'processed',
                'email' => $user->email,
                'amount' => $amount,
                'currency' => 'USD',
                'description' => 'Mcapital',
                'status' => 'processed'
            ]);

            $crm_response_balance = Http::put($url_update_balance, [
                'email' => $user->email,
                'balance' => $user->balance + $amount
            ]);

            $user->update([
                'balance' => $user->balance + $amount
            ]);

            return response()->json([
                'success' => true,
                'message' => "Success!"
            ]);
    }

    public function createDebtRequest(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }

        if ($user) {
            $response = Http::post(env('CRM_URL') . 'api/debt/create',
                [
                    'amount' => $request->amount,
                    'period' => $request->period,
                    'savings' => $request->savings,
                    'mid_income' => $request->mid_income,
                    'income_place' => $request->income_place,
                    'debt_obligations' => $request->debt_obligations,
                    'real_estate' => $request->real_estate,
                    'trading_experience' => $request->trading_experience,
                    'trading_platforms' => $request->trading_platforms,
                    'know_pamm' => $request->know_pamm,
                    'know_robot' => $request->know_robot,
                    'id_front' => $request->id_front,
                    'id_back' => $request->id_back,
                    'selfie' => $request->selfie,
                    'email' => $user->email
                ])->json();

            if (!is_null($response)) {
                if ($response['success']) {
                    return response()->json([
                        'success' => true
                    ]);
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => $response['message']
                    ]);
                }
            }

            return response()->json([
                'success' => false,
                'message' => 'Empty response from server'
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'No such user'
            ]);
        }
    }

    public function approveDebtRequest(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $user->update([
            'balance' => $user->balance + $request->amount
        ]);

        return response()->json([
            'success' => true
        ]);
    }

    public function savingAccountCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'period' => 'required|integer',
            'sum' => 'required',
            'unit' => 'required|string',
            'rate' => 'required',
            'rh' => 'sometimes'
        ]);

        if ($request->unit != 'usdt') {
            $crc_hash = crc32($request->rate . config('hashing.rate_hash'));
            if ($crc_hash != $request->rh)
                return response(['error' => 'Incorrect control sum. Please try again.'], 400);

            $rate = $request->rate;
        } else {
            $rate = 1;
        }

        if($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'success' => false,
                'message' => $errors
            ],500);
        }

        $user = auth()->user();

        $url = env('CRM_URL') . 'api/saving-accounts/create';

        $crm_response = Http::post($url, [
            'email' => $user->email,
            'sum' => $request->sum,
            'period' => $request->period,
            'unit' => $request->unit,
            'type' => 'income',
            'rate' => $rate
        ])->json();

        if ($request->hasHeader('lang') and in_array($request->header('lang'), ['ru', 'en']))
            app()->setLocale($request->header('lang'));

        if (array_key_exists('success', $crm_response)){
            if ($crm_response['success']){
                Mail::to($user->email)->send(new SavingCreated($crm_response['saving_account']));
            }
        }

        return $crm_response;
    }

    public function getAllSavingAccounts()
    {
        $user = auth()->user();
        $url = env('CRM_URL') . 'api/saving-accounts/account/' . $user->email;

        $crm_response = Http::get($url)->json();

        return $crm_response;
    }

    public function withdrawSavingMoney(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'email' => 'required|email'
        ]);

        if($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'success' => false,
                'message' => $errors
            ],500);
        }

        $user = User::where('email', $request->email)->first();

        $url = env('CRM_URL') . 'api/saving-accounts/withdraw';

        $crm_response = Http::post($url, [
            'email' => $user->email,
            'id' => $request->id,
        ])->json();

        if ($request->hasHeader('lang') and in_array($request->header('lang'), ['ru', 'en']))
            app()->setLocale($request->header('lang'));

        Mail::to($user->email)->send(new SavingClosed());

        return $crm_response;
    }

    public function updateUserBannedActives(Request $request) {

        $user = User::where('email', $request->email)->first();

        if ($user) {
            if ($request->post('status') === "true") {
                ClientBannedActive::updateOrCreate(
                    [
                        'user_id' => $user->id,
                        'pair' => $request->pair
                    ],
                    [
                        'user_id' => $user->id,
                        'pair' => $request->pair
                    ]
                );
            }
            else {
                ClientBannedActive::where('user_id', $user->id)->where('pair', $request->pair)->delete();
            }

            return response()->json(['success' => true]);
        }

        return response()->json(['success' => false, 'error' => 'User not found']);
    }

    public function getOnlineUsers() {
        return User::where('online', '1')->pluck('email');
    }

    public function realistoLogin()
    {
        $urlLogin = "https://dashboard.realisto.net/api/login";

        $data = [
            "api_key" => "PQweuGPIBWWdinJOTOJYXdobogHEyWA6Ffxx",
            "api_secret" => "xfsKeGMfYnS6SejrLYDVwhiz5RoJU9hNvZyT"
        ];

        $resp = Http::post($urlLogin,
            $data
        )->json();

        return $resp['access_token'];
    }

    public function realisto(Request $request)
    {
        $urlPayment = "https://dashboard.realisto.net/api/orders/create";

        try {
            $token = $request->bearerToken();
            $user = JWTAuth::setToken($token)->authenticate();
        } catch (JWTException $e) {
            return 'Unauthorized';
        }

        $amount = $request->amount;
        $number = time();
        $state = $request->state;
        $city = $request->city;
        $address = $request->address;
        $zip = $request->zip;

        $data = [
            "Orders" => [
                "terminal_id" => 870,
                "amount" => "$amount",
                "lang" => "ru"
            ],
            "Customers" => [
                "client_name" => "$user->first_name" . " " . "$user->last_name",
                "tax_id" => "123",
                "mobile" => "$user->phone",
                "mail" => "$user->email",
                "country" => "$user->country_code",
                "city" => "$city",
                "state" => "$state",
                "zip" => "$zip",
                "address" => "$address"
            ],
            "OrdersApiData" => [
                "incrementId" => 0,
                "okUrl" => env('APP_URL') . "/api/auth/deposit/realisto/success",
                "koUrl" =>  env('APP_URL') . "/api/auth/deposit/realisto/failed",
                "merchant_order_id" => "$number"
            ]
        ];


        $response = Http::withToken($this->realistoLogin())->post($urlPayment,
            $data
        )->json();

        if(array_key_exists('messages', $response)) {
            return response()->json([
                'success' => false,
                'messages' =>  $response['messages']
            ]);
        }

        return $response['body']['pay_url'];
    }

    public function successRealiteco(Request $request)
    {
        return $request->all();
    }

    public function failedRealiteco(Request $request)
    {
        return $request->all();
    }

    public function akuratekoChangeBalance(Request $request)
    {
        $url_create_order = env('CRM_URL') . 'api/deposit/create';
        $url_update_balance = env('CRM_URL') . 'api/update-balance';
        $amount = $request->amount;
        $currency = $request->currency;

        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }
        if($currency != 'USD') {
            $rate = Http::get("https://min-api.cryptocompare.com/data/price?fsym=$currency&tsyms=USD&api_key=443816dff55d45bda33579472998fe615fa35cf97fd599f5177a16baa239b0d1")->json();
        } else {
            $rate = 1;
        }

        $amount = $amount * $rate;
        $crm_response = Http::post($url_create_order, [
            'order_number' => '123',
            'payment_status' => 'processed',
            'email' => $user->email,
            'amount' => $amount,
            'currency' => $currency,
            'description' => 'Akurateco',
            'status' => 'success',
            'rate' => $rate
        ]);

        $crm_response_balance = Http::put($url_update_balance, [
            'email' => $user->email,
            'balance' => $user->balance + ($amount * $rate)
        ]);

        $user->update([
            'balance' => $user->balance + ($amount * $rate)
        ]);
    }

    public function enotPayment(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }

        $merchant_key = 'n3SCidgPIy3SwgY';
        $number = time();
        $url = 'https://internetcashbank.com/api/v1/invoice/create';
        $merchant_sign = "TS2PSDXb6vr6RRjbK6v9AcOKCCr1RatgutwxBsHz3efeI";
        $amount = $request->amount * 100;
        $data = [
            "merchant_key"=> "$merchant_key",
            "order_nr"=> "$number",
            "amount"=> "$amount",
            "currency"=> "USD",
        ];

        $resp = Http::post($url, [
            "merchant_key"=> "$merchant_key",
            "order_nr"    => "$number",
            "amount"      => "$amount",
            "currency"    => "USD",
            "payer_email" => "$user->email",
            "payer_name"  => "$user->first_name",
            "payer_lname" => "$user->last_name",
            "lang"        => "ru",
            "description" => "asdasd",
            "success_url" => env('APP_URL') . '/deposit/enot/callback',
            "cancel_url"  => env('APP_URL') . '/deposit/enot/callback',
            "callback_url"=> env('APP_URL') . '/deposit/enot/callback',
            "hash" => $this->hashEnotPayment($data, $merchant_sign)
        ])->json()['invoice_url'];

        return $resp;
    }

    public function hashEnotPayment(array $data, string $key)
    {
       return md5("{$data['merchant_key']}|{$data['order_nr']}|{$data['amount']}|{$data['currency']}|$key");
    }

    public function enotCallBackUrl(Request $request)
    {
        dd($request->all());
    }

}
