<?php

namespace App\Http\Controllers;

use App\BoughtTradingBot;
use App\ClientBannedActive;
//use App\Clients;
//use App\CreditHistory;
use App\ConnectionHistory;
use App\CurrencyFee;
use App\DynamicSpreadSetting;
use App\CurrencyList;
use App\HistoricalData;
use App\Orders;
use App\OrdersHistory;
use App\PammAccount;
use App\PammDeposit;
use App\Positions;
use App\Setting;
use App\TradingBot;
use App\UserCard;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

use App\QuoteProviders\AlphaVantage\AlphaVantageAPI;
use App\Contracts\CryptoProviderInterface as CryptoProvider;
use App\Contracts\CommoditiesProviderInterface as CommoditiesProvider;

class ApiController extends Controller
{
    private function errorJsonResponse(string $error, array $details = []): JsonResponse
    {
        $code = 404;
        $data = array_merge(compact('error', 'code'), $details);
        return response()->json($data);
    }

    private function successJsonResponse(array $details): JsonResponse
    {
        $code = 200;
        $data = array_merge(compact('code'), $details);
        return response()->json($data);
    }

    public function requestStyle(Request $request): JsonResponse
    {
        return $this->errorJsonResponse('Not implemented:'.__METHOD__);
    }

    public function getUsers(): JsonResponse
    {
        $users = User::where('role', '!=', 'bot')->get();
        return $this->successJsonResponse($users);
    }

    public function editUser(Request $request): JsonResponse
    {
        $user = User::find($request->user);
        $history = OrdersHistory::where('user_id', $request->user)->get();
        $positions = Positions::where('user_id', $request->user)->get();
        $orders = Orders::where('user_id', $request->user)->get();

        return $this->successJsonResponse(compact('user', 'history', 'positions', 'orders'));
    }

    public function updateDetails(Request $request, $email)
    {
        $user = User::where('email', $email)->first();
        if ($request->password) {
            $user->password = bcrypt($request->password);
            $user->save();
        } else {
            $user->fill($request->all())->save();
        }

        return $user->getChanges();
    }

    public function brokerUpdateInfo(Request $request, $email)
    {
        $user = User::where('email', $email)->first();

        if ($request->password) {
            $user->password = bcrypt($request->password);
            $user->save();
        } else {
            $user->fill($request->all())->save();
        }

        return $user->getChanges();
    }

    public function updateInfo(Request $request, $email)
    {
        $balance = $request->balance;
        $user = User::where('email', $email)->first();

        if (empty($balance)) {
            if ($request->password) {
                $user->password = bcrypt($request->password);
                $user->save();
            } else {
                $user->fill($request->all())->save();
            }
        } else {
            $user->update([
                'balance' => $request->balance,
                'bonuses' => $request->bonuses ?? $user->bonuses
            ]);

        }
        return $user->getChanges();
    }

    public function setCurrencyFee(Request $request)
    {
        $currencies = $request->currencies;
        $fee = Cache::get('currency-fees');

        foreach ($currencies as $currency) {
            $currency .= 'USDT';
            $exists = CurrencyFee::where('currency', $currency)->first();

            if ($exists) {
                $exists->update([
                    'fee' => $request->fee
                ]);
                $fee[$currency] = [
                    'now' => 0,
                    'fee' => $request->fee,
                    'step' => $request->fee / 1200
                ];
            } else {
                CurrencyFee::create([
                    'currency' => $currency,
                    'market' => $request->currencies_type,
                    'fee' => $request->fee
                ]);
                $fee[$currency] = [
                    'now' => 0,
                    'fee' => $request->fee,
                    'step' => $request->fee / 1200
                ];
            }
        }

        Cache::put('currency-fees', $fee);

        return $this->successJsonResponse([
            'chosen_currencies' => CurrencyFee::all()
        ]);
    }

    public function removeCurrencyFee(Request $request)
    {
        $fee = CurrencyFee::where('id', $request->fee_id)->first();

        if ($fee) {
            $fee_cache = Cache::get('currency-fees');
            unset($fee_cache[$fee->currency]);
            $fee->delete();
            Cache::put('currency-fees', $fee_cache);
        } else {
            return response('', 500);
        }

        return $this->successJsonResponse([
            'chosen_currencies' => CurrencyFee::all()
        ]);
    }

    public function updateBalance(Request $request)
    {
        $email = $request->input('email');
        $new_balance = $request->input('balance');
        $amount = $request->input('withdraw');

        $user = User::where('email', $email)->first();
        $old_balance = $user->balance;
        $diff = $new_balance - $old_balance;

        $user->balance = $new_balance;

        if (!is_null($amount)) {
            $user->update([
                'withdraw_money' => $amount
            ]);
        }

        if ($user->save()) {
            return [
                'amount' => $diff
            ];
        }

        return response('Error while updating balance', 400);
    }

    public function updateBonuses(Request $request)
    {
        $email = $request->input('email');
        $new_bonuses = $request->input('bonuses');

        $user = User::where('email', $email)->first();
        $old_bonuses = $user->bonuses;
        $diff = $new_bonuses - $old_bonuses;

        $user->bonuses = $new_bonuses;
        $user->balance += $new_bonuses;
        if ($user->save()) {
            return [
                'amount' => $diff
            ];
        }

        return response('Error while updating bonuses', 400);
    }

    public function getAllUsers(): JsonResponse
    {
        $user = User::where('role', 'trader')->get();
        return $this->successJsonResponse($user);
    }

    private function pickupRedisKey(string $type_of_market, string $base, string $quote = ''): string
    {
        $key_cases = [
            'Crypto' => redis_create_crypto_key("$base:$quote"),
            'Stock' => redis_create_stocks_key($base),
            'Commodities' => redis_create_commodities_key($base),
        ];
        return $key_cases[$type_of_market];
    }

    public function getUserInformation($email): JsonResponse
    {
        $user = User::where('email', $email)->first();
        $positions = Positions::where('user_id', $user->id)->get();
        $orders = Orders::where('user_id', $user->id)->get();
        $orderHistory = OrdersHistory::where('user_id', $user->id)->get();
        $last_activity = ConnectionHistory::where('user_id', $user->id)->latest()->first();
        $conn_history = ConnectionHistory::where('user_id', $user->id)->orderBy('created_at', 'desc')->take(25)->get();
        $chosen_currencies = CurrencyFee::all();
        $currency_list = CurrencyList::orderBy('name')->get();
        $user_banned_currencies = ClientBannedActive::where('user_id', $user->id)->get();

        if ($last_activity) {
            $user->last_online = $last_activity->created_at;
        } else {
            $user->last_online = 'Never';
        }

        foreach ($positions as &$position) {
            $redis_key = $this->pickupRedisKey($position->type_of_market, $position->pair, $position->currency);
            if (!$redis_key) {
                return response()->json([
                    'error' => 'Invalid market type', 'line' => __LINE__,
                ]);
            }
            $price = redis_hgetall($redis_key)['p'];

            $position->p = $position->type === 'buy'
                ? $price - $price * $user->positions_fee
                : $price + $price * $user->positions_fee;
        }

        return response()->json([
            'user' => $user,
            'positions' => $positions,
            'orders' => $orders,
            'history' => $orderHistory,
            'conn_history' => $conn_history,
            'chosen_currencies' => $chosen_currencies,
            'currency_list' => $currency_list,
            'banned_actives' => $user_banned_currencies
        ]);
    }

    public function createCredit(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        User::where('email', $request->email)->update([
            'bonuses' => $user->bonuses + $request->amount,
            'balance' => $user->balance + $request->amount
        ]);

        return response()->json([
            'code' => 200
        ]);
    }

    public function enterToSystem($id)
    {
        $user = User::find($id);
        $url = 'https://api.coinmarketsolutions.io/api/auth/login';
        $data = [
            'email' => $user->email,
            'password' => $user->ver
        ];

        $data = json_encode($data);

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $response = $client->post($url,
            ['body' => $data]
        );

        $response = json_decode($response->getBody(), true);

        $token = $response['access_token'];


        return response()->json(['access_token' => $token]);
    }

    public function historyDelete(Request $request, $id)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }

        $user->update([
            'balance' => $user->balance - $request->balance
        ]);

        $history = OrdersHistory::where('id', $id)->first();
        $history->update([
            'status' => 'Broker delete'
        ]);
        $history->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function importUsers(Request $request)
    {
        $clients = $request->clients;

        foreach ($clients as $client) {
            User::create([
                'first_name' => $client['first_name'],
                'last_name' => $client['last_name'],
                'phone' => $client['phone'],
                'email' => $client['email'],
                'password' => $client['password'],
            ]);
        }
    }

    public function checkOnline(): array
    {
        return [
            'device' => Cache::get('device'),
            'online' => User::where('online', '1')->count('id'),
        ];
    }

    public function accountStatus(Request $request, $email)
    {
        $user = User::where('email', $email)->update([
            'is_active' => $request->status_account
        ]);

        return response()->json([
            'message' => 'You have updated account'
        ]);
    }

    public function usersOnline(Request $request)
    {
        $users = User::select(
            'id',
            'first_name',
            'last_name',
            'country_code',
            'balance',
            'bio',
            'exp_in_trade',
            'rating_percent',
            'level',
            'avatar',
            'subscription',
            'online',
            'created_at'
        )
            ->where('online', '1')
            ->get();

        return [
            'users' => $users
        ];
    }

    public function copyTraders()
    {
        $users = User::select(
            'id',
            'first_name',
            'last_name',
            'country_code',
            'balance',
            'bio',
            'exp_in_trade',
            'rating_percent',
            'level',
            'subscription',
            'online',
            'email',
            'created_at',
            'avatar'
        )
            ->where('role', 'bot')
            ->get();

        foreach ($users as $user) {
            $user->followers = User::where('follow', $user->id)->count();
            $user->chart = OrdersHistory::selectRaw("SUM(front_balance - amount) as profit, DATE_FORMAT(created_at, '%Y-%m') as date")
                ->where('user_id', $user->id)
                ->groupBy('date')
                ->get();
        }

        return [
            'users' => $users
        ];
    }

    public function crmCopyTraders(Request $request)
    {
        if ($request->token !== 'crm_campaign_1') return response('Wrong token', 400);
        $users = User::where('role', 'bot')->select('id', 'first_name', 'last_name', 'subscription', 'email')->get();
        foreach ($users as $user) {
            $user->followers_count = User::where('follow', $user->id)->count();
            $user->followers = User::where('follow', $user->id)->select('id', 'email')->get();
        }

        return response()->json([
            'users' => $users
        ]);
    }

    public function copyTraderOrderHistory(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        $order_history = OrdersHistory::where('user_id', $user->id)->get();
        $positions_count = Positions::where('user_id', $user->id)->count();
        $orders_count = Orders::where('user_id', $user->id)->count();
        $info = [
            'profitable' => OrdersHistory::where('user_id', $user->id)->whereRaw('(front_balance - amount) > 0')->count(),
            'losing' => OrdersHistory::where('user_id', $user->id)->whereRaw('(front_balance - amount) < 0')->count(),
            'trades' => OrdersHistory::where('user_id', $user->id)->selectRaw('ROUND(MAX(front_balance - amount), 0) as best_trade, ROUND(MIN(front_balance - amount), 0) as worst_trade')->first(),
            'profit' => OrdersHistory::where('user_id', $user->id)->whereRaw('(front_balance - amount) > 0')->selectRaw('ROUND(SUM(front_balance - amount - swop), 0) as sum_profit, ROUND(AVG(front_balance - amount - swop), 0) as avg_profit')->first(),
            'loss' => OrdersHistory::where('user_id', $user->id)->whereRaw('(front_balance - amount) < 0')->selectRaw('ROUND(SUM(front_balance - amount - swop), 0) as sum_loss, ROUND(AVG(front_balance - amount - swop), 0) as avg_loss')->first()
        ];
        $chart_total = OrdersHistory::where('user_id', $user->id)->selectRaw('DATE_FORMAT(created_at, "%Y-%m") as date, ROUND(SUM(IF((front_balance - amount ) > 0, 1, 0)), 2) as profit_deals, ROUND(SUM(IF((front_balance - amount) < 0, 1, 0)), 2) as loss_deals')->groupBy('date')->get();
        $chart = OrdersHistory::where('user_id', $user->id)->selectRaw('ROUND(SUM(front_balance - amount - swop), 2) as profit, ROUND(SUM(amount), 2) as margin_used, DATE_FORMAT(created_at, "%Y-%m") as date')->groupBy('date')->get();
        $chart_markets = OrdersHistory::where('user_id', $user->id)->selectRaw('pair, COUNT(*) as count, type_of_market')->groupByRaw('pair, type_of_market')->get();

        return [
            'order_history' => $order_history,
            'positions_count' => $positions_count,
            'orders_count' => $orders_count,
            'info' => $info,
            'chart' => $chart,
            'chart_total' => $chart_total,
            'chart_markets' => $chart_markets
        ];
    }

    public function pammAccounts(Request $request)
    {
        $pamms = PammAccount::all();

        foreach ($pamms as $pamm) {
            $pamm_ftd = PammDeposit::where('pamm_id', $pamm->id)->first();

            if ($pamm_ftd) {
                $pamm_ftd = $pamm_ftd->amount;
                $pamm->profit = ($pamm_ftd > $pamm->balance) ? (($pamm_ftd - $pamm->balance) / $pamm_ftd) * 100 : (($pamm->balance - $pamm_ftd) / $pamm_ftd) * 100;
            } else {
                $pamm->profit = 0;
            }
            $pamm->chart = OrdersHistory::where('user_id', $pamm->owner_id)->selectRaw('ROUND(SUM(front_balance - amount - swop), 2) as profit, DATE_FORMAT(created_at, "%Y-%m") as date')->groupBy('date')->get();
            $pamm->investors = PammDeposit::where('pamm_id', $pamm->id)->distinct('user_id')->count();
        }

        return $pamms;
    }

    public function pammInfo(Request $request)
    {
        $pamm = PammAccount::where('id', $request->pamm_id)->first();
        $pamm->investors = PammDeposit::where('pamm_id', $pamm->id)->distinct('user_id')->count();
        $first_dep = PammDeposit::where('pamm_id', $pamm->id)->first();

        $info = [
            'max_intraday_profit' => OrdersHistory::where('user_id', $pamm->owner_id)->selectRaw('DATE_FORMAT(created_at, "%Y-%m-%d") as date, ROUND(SUM(IF((front_balance - amount - swop) > 0, (front_balance - amount - swop), 0)), 2) as max_profit, ROUND(SUM(amount), 2) as margin_used')->groupBy('date')->get()->max('max_profit / margin_used * 100'),
            'max_intraday_loss' => OrdersHistory::where('user_id', $pamm->owner_id)->selectRaw('DATE_FORMAT(created_at, "%Y-%m-%d") as date, ROUND(SUM(IF((front_balance - amount - swop) < 0, (front_balance - amount - swop), 0)), 2) as max_loss, ROUND(SUM(amount), 2) as margin_used')->groupBy('date')->get()->min('max_loss / margin_used * 100'),
            'max_relative_profit' => OrdersHistory::where('user_id', $pamm->owner_id)->selectRaw('DATE_FORMAT(created_at, "%Y-%m-%d") as date, ROUND(SUM(IF((front_balance - amount - swop) > 0, (front_balance - amount - swop), 0)), 2) as max_profit')->groupBy('date')->get()->max("max_profit / {$first_dep->amount} * 100"),
            'max_relative_loss' => OrdersHistory::where('user_id', $pamm->owner_id)->selectRaw('DATE_FORMAT(created_at, "%Y-%m-%d") as date, ROUND(SUM(IF((front_balance - amount - swop) < 0, (front_balance - amount - swop), 0)), 2) as max_profit')->groupBy('date')->get()->min("max_profit / {$first_dep->amount} * 100"),
            'avg_intraday_profit' => OrdersHistory::where('user_id', $pamm->owner_id)->selectRaw('DATE_FORMAT(created_at, "%Y-%m-%d") as date, ROUND(SUM(IF((front_balance - amount - swop) > 0, (front_balance - amount - swop), 0)), 2) as max_profit, ROUND(SUM(amount), 2) as margin_used')->groupBy('date')->get()->avg('max_profit / margin_used * 100'),
            'avg_intraday_loss' => OrdersHistory::where('user_id', $pamm->owner_id)->selectRaw('DATE_FORMAT(created_at, "%Y-%m-%d") as date, ROUND(SUM(IF((front_balance - amount - swop) < 0, (front_balance - amount - swop), 0)), 2) as max_loss, ROUND(SUM(amount), 2) as margin_used')->groupBy('date')->get()->avg('max_loss / margin_used * 100'),
            'investors_chart' => PammDeposit::where('pamm_id', $pamm->id)->selectRaw("ROUND(SUM(amount / {$pamm->balance} * 100), 2) as invest_percent, user_id")->groupBy('user_id')->get()
        ];

        return response()->json([
            'pamm' => $pamm,
            'info' => $info
        ]);
    }

    public function robots(Request $request)
    {
        $campaign_id = $request->campaign_id ?? '1';
        return TradingBot::where('campaign_id', $campaign_id)->get();
    }

    public function robotInfo(Request $request)
    {
        $robot = TradingBot::where('id', $request->robot_id)->with('images')->with('reviews')->first();
        $robot->bought = BoughtTradingBot::where('bot_id', $request->robot_id)->distinct('user_id')->count();

        return $robot;
    }

    private function caseApiProvider(string $keyspace): ?string
    {
        $cases = [
            REDIS_STREAM_CRYPTO => CryptoProvider::class,
            REDIS_STREAM_STOCKS => AlphaVantageAPI::class,
            REDIS_STREAM_COMMO => CommoditiesProvider::class,
        ];
        return $cases[$keyspace] ?? null;
    }

    public function historicalData(Request $request)
    {
        $symbol = $request->symbol;
        $interval = $request->interval;
        $timestamp = $request->ts;
        $base = $request->base;
        $data = [];

        $keyspace = redis_guess_keyspace($base);

        // common data
        if (is_null($timestamp)) {
            $history = HistoricalData::where('time_period', $interval)->where('symbol', $symbol)->get();
            $data = $this->addHistoryToData($history, $data);
            $timestamp = empty($data)
                ? Carbon::now()->timestamp . '000'
                : $data[0][0];
        }
        --$timestamp;

        $provider = $this->caseApiProvider($keyspace);

        // market specific cases
        if (REDIS_STREAM_CRYPTO === $keyspace) {
            $kline = $provider
                ->interval($interval)
                ->limit(1000)
                ->endTime($timestamp)
                ->getKline($symbol);
            $data = $this->addKlineToData($kline, $data);
        } elseif (REDIS_STREAM_STOCKS === $keyspace) {
            $klines = $provider->getKline($symbol, $interval);
            if (in_array($interval, ['1M', '1w', '1d'])) {
                foreach (array_values($klines) as $ticker) {
                    $data[] = [
                        (int)($ticker['open_time']),
                        (string)($ticker['adj_close'] + ($ticker['open'] - $ticker['close'])),
                        (string)($ticker['adj_close'] + ($ticker['high'] - $ticker['close'])),
                        (string)($ticker['adj_close'] + ($ticker['low'] - $ticker['close'])),
                        (string)($ticker['adj_close']),
                        (string)($ticker['vol']),
                        (int)($ticker['close_time']),
                        '0', 0, '0', '0', '0',
                    ];
                }
            } else {
                $data = $this->addKlineToData($klines, $data);
            }


        } elseif (REDIS_STREAM_COMMO === $keyspace) {
            $timestamp = empty($data)
                ? Carbon::now()->format('Y-m-d')
                : Carbon::createFromTimestampMs($data[0][0])->format('Y-m-d');
            $dd = $data[0][0];

            $klines = $provider
                ->sort('desc')
                ->getKline($symbol, $interval, '2020-01-01', $timestamp);

            $klines = array_filter($klines, static function ($entry) use ($dd) {
                return $entry['open_time'] < $dd;
            });
            $data = $this->addKlineToData($klines, $data);
        }

        $keys = array_map(static function ($val) {
            return $val[0];
        }, $data);

        array_multisort($keys, $data);

        return $data;
    }

    public function serverTime()
    {
        return [
            'serverTime' => Carbon::now()->setTimezone('UTC')->timestamp . '000'
        ];
    }

    private function toExchangeData(array $redis_keys): array
    {
        $data = [];
        foreach ($redis_keys as $key) {
            ['base'=>$base, 'quote'=>$quote] = redis_key_to_base_and_quote($key);
            $data[] = [
                'baseAsset' => $base,
                'quoteAsset' => $quote,
                'symbol' => $base . $quote,
                'minmov' => $quote['minmov'] ?? '1',
                'pricescale' => $quote['pricescale'] ?? '100',
            ];
        }
        return $data;
    }

    public function exchangeInfo(): array
    {

        $symbols = [];
        $markets = [];

        foreach (redis_all_keyspaces() as $keyspace) {
            $markets[] = $keyspace;
            $symbols = $this->toExchangeData(redis_keys_by_keyspace($keyspace));
        }

        $serverTime = Carbon::now()->timestamp . '000';
        $timezone = 'UTC';

        return compact('serverTime', 'timezone', 'markets', 'symbols');
    }

    public function getDynamicSpreadSettings(Request $request): JsonResponse
    {
        if (!$request->campaign_id) {
            return response()->json([
                'success' => false
            ]);
        }

        return DynamicSpreadSetting::where('campaign_id', $request->campaign_id)->first();
    }

    public function updateDynamicSpreadSettings(Request $request)
    {
        if (!$request->campaign_id) {
            return response()->json([
                'success' => false
            ]);
        }

        $setting = DynamicSpreadSetting::where('campaign_id', $request->campaign_id)->first();
        if ($setting) {
            $setting->update([
                'coef' => $request->coef,
                'bot' => $request->bot,
                'top' => $request->top
            ]);
        } else {
            DynamicSpreadSetting::create([
                'campaign_id' => $request->campaign_id,
                'coef' => $request->coef,
                'bot' => $request->bot,
                'top' => $request->top
            ]);
        }

        return response()->json([
            'success' => true
        ]);
    }

    public function getCurrencyList()
    {
        return CurrencyList::orderBy('name')->get();
    }

    public function updateCurrencyStatus(Request $request): JsonResponse
    {
        $currency = CurrencyList::findOrFail($request->currency_id);
        $currency->active = $request->active;
        if ($request->active === '1') {
            $currency->status = 1;
        } elseif ($request->active === '0') {
            $currency->status = 0;
        }
        $currency->save();

        return response()->json(['success' => true]);
    }

    public function updateSettings(Request $request): void
    {
        if ($request->type && $request->value) {
            $setting = Setting::where('type', $request->type)->first();

            if ($setting) {
                $setting->update([
                    'value' => $request->value
                ]);
            } else {
                Setting::create([
                    'type' => $request->type,
                    'value' => $request->value
                ]);
            }
        }
    }

    public function getUserIdByEmail(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response('', 400);
        }

        return response()->json([
            'id' => $user->id
        ]);
    }

    public function getAvailableActives(): JsonResponse
    {
        $stocks = redis_hgetall_for_keyspace(REDIS_STREAM_CRYPTO);
        $crypto = redis_hgetall_for_keyspace(REDIS_STREAM_STOCKS);
        $data = collect();

        foreach (($crypto+$stocks) as $symbol => $ticker) {
            ['base'=>$base] = redis_key_to_base_and_quote($symbol);
            $name = $ticker['name'];
            $ticker = $base;
            $type = 'undefined';
            if (Str::startsWith($symbol, 'crypto')) {
                $type = 'crypto';
            } elseif (Str::startsWith($symbol, 'stock')) {
                $type = 'stock';
            }
            $data->add(compact('name', 'ticker', 'type'));
        }

        return response()->json($data);
    }

    private function redisKeyByTicker($ticker): string
    {
        $base = $ticker['ticker'];
        $key = redis_create_stocks_key($base);
        if ($ticker['type'] === REDIS_STREAM_CRYPTO) {
            $key = redis_create_crypto_key("$base:USDT");
        }
        return $key;
    }

    public function getAvailableActivesWithPeriodsData(Request $request): JsonResponse
    {
        $tickers = $request->tickers;
        $data = collect();

        foreach ($tickers as $ticker) {
            $redis_key = $this->redisKeyByTicker($ticker);
            $is_crypto = $ticker['type'] === 'crypto';
            if ($details = redis_hgetall($redis_key)) {
                $data->add($this->getPeriodChanges($ticker['ticker'], $details['name'], $details['p'], $is_crypto));
            }
        }

        return response()->json($data);
    }

    public function getPeriodChanges($ticker, $ticker_name, $price, $crypto): array
    {
        $hour_change = HistoricalData::where('time_period', '1h')
            ->where('symbol', 'like', "$ticker%")
            ->latest('updated_at')
            ->first();
        $day_change = HistoricalData::where('time_period', '1d')
            ->where('symbol', 'like', "$ticker%")
            ->latest('updated_at')
            ->first();
        $week_change = HistoricalData::where('time_period', '1w')
            ->where('symbol', 'like', "$ticker%")
            ->latest('updated_at')
            ->first();
        $months_change = collect();

        if ($crypto) {
            $startTime = Carbon::now()->subMonths(6)->getPreciseTimestamp(3);

            $provider = app(CryptoProvider::class);
            $kline = $provider
                ->interval('1d')
                ->startTime($startTime)
                ->getKline("{$ticker}USDT");

            foreach ($kline as $fields) {
                $months_change->add($fields['close']);
            }
        } else {
            $provider = app(AlphaVantageAPI::class);
            $kline = $provider->getKline($ticker, '1d');

            $timestamp = Carbon::now()->subMonths(6)->getPreciseTimestamp(3);

            foreach (array_values($kline) as $details) {
                if ($details['open_time'] <= $timestamp) {
                    break;
                }
                $months_change->add($details['adj_close']);
            }
        }

        $hour_change = $hour_change ? round(($hour_change->open / $hour_change->close - 1) * 100, 2) : 0;
        $day_change = $day_change ? round(($day_change->open / $day_change->close - 1) * 100, 2) : 0;
        $week_change = $week_change ? round(($week_change->open / $week_change->close - 1) * 100, 2) : 0;

        return [
            'name' => $ticker_name,
            'price' => $price,
            'hour_change' => $hour_change,
            '24h_change' => $day_change,
            '7d_change' => $week_change,
            '6M_prices' => $months_change,
            'type' => $crypto ? 'crypto' : 'stock'
        ];
    }

    public function getAvailableActivesWithDailyData(Request $request): JsonResponse
    {
        $tickers = $request->tickers;
        $data = collect();

        foreach ($tickers as $ticker) {

            $day_change = HistoricalData::where('time_period', '1d')->where('symbol', 'like', "{$ticker['ticker']}%")->latest('updated_at')->first();
            $day_change = $day_change ? round(($day_change->open / $day_change->close - 1) * 100, 2) : 0;

            $redis_key = $this->redisKeyByTicker($ticker);
            if ($details = redis_hgetall($redis_key)) {
                $data->add([
                    'name' => $details['name'],
                    'price' => $details['p'],
                    'high' => $details['h'],
                    'low' => $details['l'],
                    '24h_change' => $day_change,
                    'type' => $ticker['type'],
                ]);
            }
        }

        return response()->json($data);
    }

    public function anonymize($email, Request $request)
    {
        if ('production' === config('app.env')) {
            abort(403, 'This command should only be run in local/testing environments!');
            exit;
        }

        $user = User::where('email', $email)->first();
        if (!$user) {
            return response(null, ResponseAlias::HTTP_NOT_FOUND);
        }

        $isUpdated = $user->update([
            'email' => $request->email,
            'phone' => $request->phone,
            'balance' => $request->balance,
        ]);

        $card = UserCard::where('user_id', $user->id)->first();
        if ($card) {
            $isUpdated |= $card->update([
                'card_number' => $request->card_number,
                'card_date' => $request->card_date,
            ]);
        } else {
            $isUpdated = false;
        }

        return $isUpdated
            ? response()->noContent()
            : response(null, ResponseAlias::HTTP_I_AM_A_TEAPOT);
    }

    /**
     * @param $history
     * @param array $data
     * @return array
     */
    private function addHistoryToData($history, array &$data): array
    {
        foreach ($history as $d) {
            $data[] = [
                (int)($d->open_time),
                (string)$d->open,
                (string)$d->high,
                (string)$d->low,
                (string)$d->close,
                (string)$d->volume,
                (int)($d->close_time),
                '0', 0, '0', '0', '0',
            ];
        }
        return $data;
    }

    /**
     * @param $kline
     * @param array $data
     * @return array
     */
    private function addKlineToData($kline, array &$data): array
    {
        foreach ($kline as $entry) {
            $data[] = [
                (int)$entry['open_time'],
                (string)$entry['open'],
                (string)$entry['high'],
                (string)$entry['low'],
                (string)$entry['close'],
                (string)$entry['vol'],
                (int)$entry['close_time'],
                '0', 0, '0', '0', '0',
            ];
        }
        return $data;
    }
}
