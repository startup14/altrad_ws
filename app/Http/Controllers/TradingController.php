<?php /** @noinspection UnknownInspectionInspection */

namespace App\Http\Controllers;

use App\ActiveConnection;
use App\BoughtTradingBot;
use App\ConnectionHistory;
use App\Orders;
use App\OrdersHistory;
use App\PammAccount;
use App\PammDeposit;
use App\Positions;
use App\TradingBot;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Jenssegers\Agent\Agent;

class TradingController extends Controller
{
    public function getAllUserInfo(Request $request)
    {
        $user = auth()->user();

        if (!$user) {
            return 'Unauthorized';
        }

        $orders_history = OrdersHistory::where('user_id', $user->id)->orderBy('created_at')->get();

        $fee['crypto_fee'] = $user['crypto_fee'];
        $fee['stock_fee'] = $user['stock_fee'];
        $fee['commodities_fee'] = $user['commodities_fee'];
        $fee['positions_fee'] = $user['positions_fee'];

        $positions = Positions::where('user_id', $user->id)->orderBy('created_at')->get();
        $orders = Orders::where('user_id', $user->id)->orderBy('created_at')->get();
        $depth = $this->getOrderBook(new Request([
            'type' => 'crypto',
            'fsym' => 'BTC',
            'tsym' => 'USDT'
        ]));

        unset(
            $user['crypto_fee'], $user['stock_fee'], $user['positions_fee'], $user['commodities_fee']
        );

        // Get Saving Account Percent
        $savingAccountInfo = Http::get(env('CRM_URL') . 'api/saving-accounts/settings/' . $user->email)->json();
        $agent = new Agent();

        $platform = $this->getUserPlatform($agent);

        $copy = ConnectionHistory::where('platform', $platform)
            ->where('os', $agent->platform())
            ->where('ip', $_SERVER['HTTP_CF_CONNECTING_IP'] ?? $_SERVER['REMOTE_ADDR'])
            ->where('user_id', $user->id)
            ->where('updated_at', '>', Carbon::now()->subMinutes(10))
            ->whereNull('closed_at')->latest()->first();

        if (!$copy) {
            $copy = ConnectionHistory::create([
                'user_id' => $user->id,
                'platform' => $platform,
                'os' => $agent->platform(),
                'ip' => $_SERVER['HTTP_CF_CONNECTING_IP'] ?? $_SERVER['REMOTE_ADDR'],
            ]);
        }

        return [
            'history' => $orders_history,
            'orders' => $orders,
            'positions' => $positions,
            'profile' => $user,
            'fee' => $fee,
            'depth' => $depth,
            'saving_account_info' => $savingAccountInfo,
            'session_id' => $copy->id,
        ];
    }

    public function getPositions(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }

        $positions = Positions::where('user_id', $user->id)->get();

        return response()->json($positions);
    }

    public function updatePosition(Request $request, $id): JsonResponse
    {
        $position = Positions::find($id);

        if (!$position) {
            return response()->json([
                'message' => 'Failed',
            ]);
        }

        $position->update([
            'take_profit' => $request->take_profit,
            'stop_loss' => $request->stop_loss
        ]);

        return response()->json([
            'message' => 'Success',
            'position' => $position
        ]);
    }

    public function getOrders(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }

        $positions = Orders::where('user_id', $user->id)->get();

        return response()->json($positions);
    }

    public function createPosition(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return response()->json([
                'success' => false
            ]);
        }

        if (!$user->is_active) {
            return response()->json([
                'success' => false
            ]);
        }

        $active_connection = ActiveConnection::where('user_id', $user->id)->where('activity_token', $request->activity_token)->first();

        if (!$active_connection)
            return response('No such activity token', 400);

        if (!in_array($user->account_status, ['pending', 'can_deposit'])) {
            $fail = $this->canTradeButBadType($user, $request, 'crypto') || $this->canTradeButBadType($user, $request, 'stock');
            if ($fail) {
                return response()->json([
                    'message' => 'Failed',
                    'status' => 'false',
                    'balance' => $user->balance
                ]);
            }
            $price_changed = 0;

            if ($request->type_of_market === 'Crypto') {
                $redis_key = redis_create_crypto_key("$request->pair:$request->currency");
                $fields = redis_hgetall($redis_key);
                $pair_price = $fields['p'];
                if ($request->type === 'buy') {
                    $init_price = round($pair_price + ($pair_price * $user->crypto_fee), 8);
                } elseif ($request->type === 'sell') {
                    $init_price = round($pair_price - ($pair_price * $user->crypto_fee), 8);
                }

                if ($request->currency !== 'USDT') {
                    $redis_key = redis_create_crypto_key("$request->currency:USDT");
                    $request->conversion = redis_hgetall($redis_key)['p'];
                    $request->amount = $request->conversion * $request->crypto_amount;
                }

                if ($request->lv > 1) {
                    $request->leverage = $request->amount * $request->lv;
                }

                $request->value = round($request->crypto_amount / $init_price * $request->lv, 5);
            } elseif ($request->type_of_market === 'Stock') {
                $redis_key = redis_create_stocks_key($request->pair);
                $pair_price = redis_hgetall($redis_key)['p'];
                if ($request->type === 'buy') {
                    $init_price = round($pair_price + ($pair_price * $user->stock_fee), 2);
                } elseif ($request->type === 'sell') {
                    $init_price = round($pair_price - ($pair_price * $user->stock_fee), 2);
                }

                if ($request->lv > 1) {
                    $request->leverage = round($request->value * $init_price, 2);
                    $request->amount = round($request->leverage / $request->lv, 2);
                } else {
                    $request->amount = round($request->value * $init_price, 2);
                }
            } else {
                $redis_key = redis_create_commodities_key($request->pair);
                $pair_price = redis_hgetall($redis_key)['p'];
                if ($request->type === 'buy') {
                    $init_price = round($pair_price + ($pair_price * $user->commodities_fee), 2);
                } elseif ($request->type === 'sell') {
                    $init_price = round($pair_price - ($pair_price * $user->commodities_fee), 2);
                }

                if ($request->lv > 1) {
                    $request->leverage = round($request->value * $init_price, 2);
                    $request->amount = round($request->leverage / $request->lv, 2);
                } else {
                    $request->amount = round($request->value * $init_price, 2);
                }
            }

            if ($request->entry_price > $init_price) {
                $dif = ($request->entry_price / $init_price - 1) * 100;
            } else {
                $dif = ($init_price / $request->entry_price - 1) * 100;
            }

            if ($dif > 0.05) {
                $price_changed = 1;
            }

            $data = $request->all();
            $data['conversion'] = $request->conversion;
            $data['amount'] = $request->amount;
            $data['leverage'] = $request->leverage;
            $data['value'] = $request->value;
            $data['user_id'] = $user->id;
            $data['take_profit'] = null;
            $data['stop_loss'] = null;
            $data['init_price'] = $init_price;
            $data['entry_price'] = $init_price;
            $data['limit'] = $init_price;
            $data['balance_before'] = $user->balance;
            $data['balance_after'] = $user->balance - $request->amount;

            $user->update([
                'balance' => round($user->balance - $request->amount, 2)
            ]);

            unset($data['balance']);

            $position = Positions::create($data);
            $position = Positions::where('id', $position->id)->first();
            $orderHistory = OrdersHistory::where('user_id', $user->id)->get();
            $userPositions = Positions::where('user_id', $user->id)->count();
            if ($userPositions >= $user->max_positions) {
                $response = Http::get(env('CRM_URL') . 'api/clients/assigned/telegram', [
                    'email' => $user->email
                ]);

                if ($response->successful()) {
                    $response = $response->json();

                    if ($response['success']) {
                        $telegram = $response['username'];
                        Http::get(env('BOT_URL'), [
                            'u_name' => $telegram,
                            'fname' => $user->first_name,
                            'lname' => $user->last_name,
                            'client_id' => $user->id,
                            'type' => 'positions',
                            'max_positions' => $user->max_positions
                        ]);
                    }
                }
            }

            if ($user->role === 'bot') {
                $followers = User::where('follow', $user->id)->get();

                foreach ($followers as $follower) {
                    if ($follower->balance < $position->amount) {
                        continue;
                    }

                    $copy_position = $position->replicate();
                    $copy_position->user_id = $follower->id;
                    $copy_position->balance_before = $follower->balance;
                    $copy_position->balance_after = $follower->balance - $position->amount;
                    $copy_position->save();

                    $follower->balance = round($follower->balance - $position->amount, 2);
                    $follower->save();

                    Http::put(env('CRM_URL') . 'api/update-balance', [
                        'email' => $follower->email,
                        'balance' => $follower->balance
                    ]);
                }
            } else {
                Http::put(env('CRM_URL') . 'api/update-balance', [
                    'email' => $user->email,
                    'balance' => $user->balance
                ]);
            }
        } else {
            return response()->json([
                'message' => 'Failed',
                'status' => 'false',
                'balance' => $user->balance
            ]);
        }

        return response()->json([
            'message' => 'Success',
            'status' => 'true',
            'position' => $position,
            'history' => $orderHistory,
            'balance' => $user->balance,
            'price_changed' => $price_changed
        ]);
    }

    private function canTradeButBadType($user, $request, string $market): bool
    {
        $can_trade = sprintf('can_trade_%s', strtolower($market));
        $type_of_market = ucfirst($market);

        return ($user->account_status === $can_trade) && ($request->type_of_market !== $type_of_market);
    }

    public function deletePosition(Request $request)
    {
        $position = Positions::where('id', $request->id)->first();
        if (!$position) {
            return response()->json([
                'success' => false
            ]);
        }

        $user = User::where('id', $position->user_id)->first();

        if (!$user->is_active) {
            return response()->json([
                'success' => false
            ]);
        }

        if ($request->activity_token !== 'tpsl') {
            $active_connection = ActiveConnection::where('user_id', $user->id)->where('activity_token', $request->activity_token)->first();
            if (!$active_connection) {
                return response('No such activity token', 400);
            }
        }

        if (!in_array($user->account_status, ['pending', 'can_deposit'])) {

            $fail = $this->canTradeButBadType($user, $position, 'crypto') || $this->canTradeButBadType($user, $position, 'stock');
            if ($fail) {
                return response()->json([
                    'message' => 'Failed',
                    'status' => 'false',
                    'balance' => $user->balance
                ]);
            }

            if (in_array($request->status, ['take profit', 'stop loss'])) {
                if ($position->type_of_market === 'Crypto') {
                    $close = $request->status === 'take profit' ? $position->take_profit : $position->stop_loss;
                    $redis_key = redis_create_crypto_key("$position->currency:USDT");
                    $fields = redis_hgetall($redis_key);
                    if ($position->type === 'buy') {
                        if ($position->currency === 'USDT') {
                            $balance = $position->amount + ($close - $position->limit) * $position->value - ($position->amount * $position->swop);
                        } else {
                            $conversion = $fields['p'];
                            $balance = $position->amount + ($close - $position->limit) * $conversion * $position->value - ($position->amount * $position->swop);
                        }
                    } else if ($position->currency === 'USDT') {
                        $balance = $position->amount + ($position->limit - $close) * $position->value - ($position->amount * $position->swop);
                    } else {
                        $conversion = $fields['p'];
                        $balance = $position->amount + ($position->limit - $close) * $conversion * $position->value - ($position->amount * $position->swop);
                    }

                } elseif ($position->type_of_market === 'Stock') {
                    $close = $request->status === 'take profit' ? $position->take_profit : $position->stop_loss;
                    if ($position->type === 'buy') {
                        $balance = $position->amount + ($close - $position->limit) * $position->value - ($position->amount * $position->swop);
                    } else {
                        $balance = $position->amount + ($position->limit - $close) * $position->value - ($position->amount * $position->swop);
                    }
                }
            } else {
                if ($position->type_of_market === 'Crypto') {

                    $redis_key = redis_create_crypto_key("$position->pair:$position->currency");
                    $fields = redis_hgetall($redis_key);
                    $close = $fields['p'];
                    $random = $close * $user->positions_fee;

                    $redis_usdt_key = redis_create_crypto_key("$position->currency:USDT");
                    $fields_usdt = redis_hgetall($redis_usdt_key);

                    if ($position->type === 'buy') {
                        $close -= $random;
                        if ($position->currency === 'USDT') {
                            $balance = $position->amount + ($close - $position->limit) * $position->value - ($position->amount * $position->swop);
                        } else {
                            $conversion = $fields_usdt['p'];
                            $balance = $position->amount + ($close - $position->limit) * $conversion * $position->value - ($position->amount * $position->swop);
                        }
                    } else {
                        $close += $random;
                        if ($position->currency === 'USDT') {
                            $balance = $position->amount + ($position->limit - $close) * $position->value - ($position->amount * $position->swop);
                        } else {
                            $conversion = $fields_usdt['p'];
                            $balance = $position->amount + ($position->limit - $close) * $conversion * $position->value - ($position->amount * $position->swop);
                        }
                    }
                } else {
                    if ($position->type_of_market === 'Stock') {
                        $redis_key = redis_create_stocks_key($position->pair);
                    } else {
                        $redis_key = redis_create_commodities_key($position->pair);
                    }
                    $fields = redis_hgetall($redis_key);
                    $close = $fields['p'];
                    $random = $close * $user->positions_fee;
                    if ($position->type === 'buy') {
                        $close -= $random;
                        $balance = $position->amount + ($close - $position->limit) * $position->value - ($position->amount * $position->swop);
                    } else {
                        $close += $random;
                        $balance = $position->amount + ($position->limit - $close) * $position->value - ($position->amount * $position->swop);
                    }
                }
            }

            $position->update(['status' => $request->status]);

            $balance_before = $user->balance;
            $balance_after = $user->balance + $balance;

            try {
                $order = OrdersHistory::create([
                    'status' => $position->status,
                    'class' => $position->class,
                    'entry_price' => $position->entry_price,
                    'type' => $position->type,
                    'amount' => $position->amount,
                    'limit' => $position->limit,
                    'value' => $position->value,
                    'pair' => $position->pair,
                    'currency' => $position->currency,
                    'liq' => $position->liq,
                    'lv' => $position->lv,
                    'user_id' => $position->user_id,
                    'leverage' => $position->leverage,
                    'close' => $close ?? 0,
                    'time_of_open' => Carbon::parse($position->created_at, 'UTC'),
                    'type_of_market' => $position->type_of_market,
                    'unit' => $position->unit,
                    'balance_before_open' => $position->balance_before,
                    'balance_after_open' => $position->balance_after,
                    'balance_before_close' => $balance_before,
                    'balance_after_close' => $balance_after,
                    'front_balance' => $balance,
                    'swop' => $position->swop,
                    'crypto_amount' => $position->crypto_amount,
                    'conversion' => $conversion ?? 1,
                    'description' => $request->broker
                ]);

                $user->update([
                    'balance' => round($user->balance + $balance, 2)
                ]);

                Positions::where('id', $request->id)->delete();
            } catch (Exception $e) {
                $position->restore();
                return $e->getMessage();
            }

            if ($user->role === 'bot') {
                $followers = User::where('follow', $user->id)->get();

                foreach ($followers as $follower) {
                    $fol_pos = Positions::where('user_id', $follower->id)->where('limit', $position->limit)->where('lv', $position->lv)->where('leverage', $position->leverage)->first();
                    if (!$fol_pos) {
                        continue;
                    }

                    Positions::where('user_id', $follower->id)->where('limit', $position->limit)->where('lv', $position->lv)->where('leverage', $position->leverage)->delete();

                    $copy_order = $order->replicate();
                    $copy_order->user_id = $follower->id;
                    $copy_order->balance_before_open = $fol_pos->balance_before;
                    $copy_order->balance_after_open = $fol_pos->balance_after;
                    $copy_order->balance_before_close = $follower->balance;
                    $copy_order->balance_after_close = $follower->balance + $balance;
                    $copy_order->save();

                    $follower->balance += $balance;
                    $follower->save();

                    Http::put(env('CRM_URL') . 'api/update-balance', [
                        'email' => $follower->email,
                        'balance' => $follower->balance
                    ]);
                }
            } else {
                Http::put(env('CRM_URL') . 'api/update-balance', [
                    'email' => $user->email,
                    'balance' => $user->balance
                ]);
            }
        } else {
            return response()->json([
                'message' => 'Failed',
                'status' => 'false',
                'balance' => $user->balance
            ]);
        }

        return response()->json([
            'message' => 'Success',
            'status' => 'true',
            'data' => $order,
            'balance' => $user->balance
        ]);
    }

    public function cancelPosition(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }

        $position = Positions::where('id', $request->id)->where('user_id', $user->id)->firstOrFail();

        $position->update([
            'status' => 'cancel'
        ]);
        $position->delete();

        $user->update([
            'balance' => $user->balance + $position->amount
        ]);

        Http::put(env('CRM_URL') . 'api/update-balance', [
            'email' => $user->email,
            'balance' => $user->balance
        ]);

        return response('', 200);
    }

    public function brokerPositionUpdate(Request $request)
    {
        $position = Positions::where('id', $request->pos_id)->first();

        $position->update([
            'type' => $request->type,
            'entry_price' => $request->limit,
            'limit' => $request->limit,
            'value' => $position->value
        ]);

        return response()->json([
            'changes' => $position->getChanges()
        ]);
    }

    public function brokerPositionDelete(Request $request)
    {
        $position = Positions::where('id', $request->pos_id)->first();
        $user = User::where('id', $position->user_id)->first();

        $user->update([
            'balance' => round($user->balance + $position->amount, 2)
        ]);

        Positions::where('id', $request->pos_id)->delete();

        return response()->json([
            'balance' => $user->balance,
            'limit' => $position->limit,
            'value' => $position->value,
            'pair' => $position->pair . ' / ' . $position->currency,
            'lv' => $position->lv
        ]);
    }

    public function brokerHistoryDelete(Request $request)
    {
        $position = OrdersHistory::where('id', $request->pos_id)->first();

        if ($position) {
            if ($position->status === 'margin call') {
                return response()->json($this->brokerHistoryDeleteMarginCall($position));
            } else {
                return response()->json($this->brokerHistoryDeleteRegular($position));
            }
        }

    }

    private function brokerHistoryDeleteMarginCall($position)
    {
        $user = User::where('id', $position->user_id)->first();

        $margin_call_positions = OrdersHistory::where('user_id', $user->id)
            ->whereBetween('created_at', [Carbon::parse($position->created_at)->subMinutes(1), Carbon::parse($position->created_at)->addMinutes(1)])
            ->get();
        $count = $margin_call_positions->count();

        $balance = $margin_call_positions->first()->balance_before_close;
        $margin_call_positions->each(function ($item, $key) use (&$balance) {
            $balance += $item->amount;

            $item->update([
                'status' => 'broker delete',
                'deleted_at' => Carbon::now()
            ]);
        });

        $user->update([
            'balance' => round($balance, 2)
        ]);

        return [
            'balance' => $user->balance,
            'count' => $count,
            'type' => 'margin'
        ];
    }

    private function brokerHistoryDeleteRegular($position)
    {
        $user = User::where('id', $position->user_id)->first();

        $user->update([
            'balance' => round($user->balance - round($position->front_balance, 2) + $position->amount, 2)
        ]);

        $position->update([
            'status' => 'broker delete',
            'deleted_at' => Carbon::now()
        ]);

        return [
            'balance' => $user->balance,
            'limit' => $position->limit,
            'value' => $position->value,
            'pair' => $position->pair . ' / ' . $position->currency,
            'lv' => $position->lv,
            'type' => 'regular'
        ];
    }

    public function brokerHistoryRestore(Request $request)
    {
        $history = OrdersHistory::where('id', $request->pos_id)->first();
        $user = User::where('id', $history->user_id)->first();
        $position = Positions::onlyTrashed()
            ->where('limit', $history->limit)
            ->where('pair', $history->pair)
            ->where('currency', $history->currency)
            ->where('entry_price', $history->entry_price)
            ->first();

        $history->delete();
        $position->restore();

        $user->update([
            'balance' => round($user->balance + (round($position->front_balance, 2)))
        ]);
    }

    public function brokerOrderDelete(Request $request)
    {
        $order = Orders::where('id', $request->pos_id)->first();
        Orders::where('id', $request->pos_id)->delete();

        return response()->json([
            'limit' => $order->limit,
            'value' => $order->value,
            'pair' => $order->pair . ' / ' . $order->currency,
            'lv' => $order->lv
        ]);
    }

    public function getAllOrders(): JsonResponse
    {
        $positions = Orders::all();
        return response()->json($positions);
    }

    public function createOrder(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }

        if (!in_array($user->account_status, ['pending', 'can_deposit'])) {
            if ($user->account_status === 'can_trade_crypto') {
                if ($request->type_of_market != 'Crypto') return response()->json([
                    'message' => 'Failed',
                    'status' => 'false',
                    'balance' => $user->balance
                ]);
            }
            if ($user->account_status == 'can_trade_stock') {
                if ($request->type_of_market != 'Stock') return response()->json([
                    'message' => 'Failed',
                    'status' => 'false',
                    'balance' => $user->balance
                ]);
            }
            $data = $request->all();
            $data['user_id'] = $user->id;
//            $data['value'] =  $data['limit'] /

            if ($user->balance < $data['amount']) {
                return response()->json([
                    'message' => 'Error',
                    'status' => 'false',
                ]);
            }

            $order = Orders::create($data);
            $order = Orders::where('id', $order->id)->first();
        } else {
            return response()->json([
                'message' => 'Failed',
                'status' => 'false',
            ]);
        }
        return response()->json([
            'message' => 'Success',
            'status' => 'true',
            'data' => $order
        ]);
    }

    public function deleteOrder($id): JsonResponse
    {
        Orders::find($id)->delete();

        return response()->json([
            'message' => 'Success',
            'status' => 'true'
        ]);
    }

    /*public function updateOrder(Request $request, $id)
    {
        $user = auth()->user();
        $data = $request->all();
        $data['user_id'] = $user->id;

        $order = Orders::find($id)->update($data);

        return response()->json($order);
    }*/

    public function ordersHistory(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }

        $positions = OrdersHistory::where('user_id', $user->id)->get();

        return response()->json($positions);
    }

    public function transferOrderToPosition(Request $request): JsonResponse
    {
        $order = Orders::where('id', $request->id)->first();
        Orders::where('id', $request->id)->delete();

        $user = User::where('id', $order->user_id)->first();
        $balance_before = $user->balance;
        $balance_after = $user->balance + $request->balance;

        if ($user->balance < $order->amount) {
            return response()->json([
                'message' => 'Error',
                'status' => 'false',
            ]);
        }

        $user->update([
            'balance' => $user->balance + $request->balance
        ]);

        $position = Positions::create([
            'status' => $request->status,
            'class' => $request->class,
            'entry_price' => $request->entry_price,
            'init_price' => $request->entry_price,
            'type' => $order->type,
            'amount' => $order->amount,
            'limit' => $order->limit,
            'value' => $order->value,
            'pair' => $order->pair,
            'currency' => $order->currency,
            'liq' => $order->liq,
            'lv' => $order->lv,
            'user_id' => $order->user_id,
            'leverage' => $order->leverage,
            'type_of_market' => $order->type_of_market,
            'unit' => $order->unit,
            'balance_before' => $balance_before,
            'balance_after' => $balance_after,
            'crypto_amount' => $order->crypto_amount
        ]);
        $position = Positions::where('id', $position->id)->first();

        Http::put(env('CRM_URL') . 'api/update-balance', [
            'email' => $user->email,
            'balance' => $user->balance
        ]);

        return response()->json([
            'message' => 'Success',
            'status' => 'true',
            'position' => $position,
            'balance' => $user->balance
        ]);
    }

    public function superDelete(Request $request)
    {
        $position = Positions::find($request->id);
        $position->update([
            'status' => 'Broker delete'
        ]);
        $position->delete();

        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }

        $user->update([
            'balance' => $user->balance + $request->balance
        ]);

        Http::put(env('CRM_URL') . 'api/update-balance', [
            'email' => $user->email,
            'balance' => $user->balance
        ]);

        return response()->json([
            'message' => 'Success',
            'status' => 'true',
            'balance' => $user->balance
        ]);
    }

    public function getCurrencyExchangeRate(): array
    {
        return [
            'stocks' => redis_hgetall_for_keyspace(REDIS_STREAM_STOCKS),
            'crypto' => array_map(static function ($fields) {
                return $fields;
            }, redis_hgetall_for_keyspace(REDIS_STREAM_CRYPTO)),
            'commodities' => redis_hgetall_for_keyspace(REDIS_STREAM_COMMO),
        ];
    }

    public function getSignals(): array
    {
        return [
            'currencies' => Cache::get('signals'),
            'time' => Carbon::now()->addHour()->minutes(00)->seconds(00)->diffInSeconds(Carbon::now())
        ];
    }

    public function getOrderBook(Request $request): array
    {
        $type = $request->get('type');
        $fsym = $request->get('fsym');
        $tsym = $request->get('tsym');

        $orders_key = "orders:$fsym";
        $trades_key = "trades:$fsym";

        if ($type === 'crypto') {
            $orders_key .= ",$tsym";
            $trades_key .= ",$tsym";
        }

        return [
            'orders' => Cache::get($orders_key),
            'trades' => Cache::get($trades_key)
        ];
    }

    public function followTrader(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }
        $following = User::where('id', $request->trader_id)->first();
        $user->update([
            'follow' => $following->id,
            'positions_fee' => $following->positions_fee,
            'crypto_fee' => $following->crypto_fee,
            'stock_fee' => $following->stock_fee,
            'commodities_fee' => $following->commodities_fee
        ]);

        $response = Http::put(env('CRM_URL') . "api/clients/$user->email/follow-update", [
            'positions_fee' => $following->positions_fee,
            'crypto_fee' => $following->crypto_fee,
            'stock_fee' => $following->stock_fee,
            'commodities_fee' => $following->commodities_fee
        ]);

        if (!$response->serverError()) {
            return response()->json([
                'success' => true
            ]);
        }

        return response()->json([
            'success' => false
        ]);
    }

    public function unfollowTrader(Request $request)
    {
        $email = $request->email;
        if (!$email) {
            $user = auth()->setToken($request->bearerToken())->getPayload();
        } else {
            $user = User::where('email', $email)->first();
        }

        $user->follow = null;
        $user->save();
    }

    public function buyTradingBot(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }
        $trading_bot = TradingBot::where('id', $request->bot_id)->first();
        $bought_trading_bots = BoughtTradingBot::where('user_id', $user->id)->get();
        foreach ($bought_trading_bots as $bot) {
            $bot->active = 0;
            $bot->save();
        }

        BoughtTradingBot::create([
            'bot_id' => $trading_bot->id,
            'user_id' => $user->id,
            'active' => '1',
            'active_till' => Carbon::now()->addMonth()->timestamp
        ]);

        $user->update([
            'balance' => $user->balance - $trading_bot->price,
            'positions_fee' => $trading_bot->fee
        ]);
    }

    public function removeTradingBot(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }
        $trading_bot = TradingBot::where('id', $request->bot_id)->first();

        BoughtTradingBot::where('bot_id', $trading_bot->id)->where('user_id', $user->id)->delete();
    }

    public function pauseTradingBot(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }
        $trading_bot = TradingBot::where('id', $request->bot_id)->first();

        BoughtTradingBot::where('bot_id', $trading_bot->id)->where('user_id', $user->id)->update([
            'active' => 0
        ]);
    }

    public function resumeTradingBot(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }
        $trading_bot = TradingBot::where('id', $request->bot_id)->first();
        $bought_trading_bots = BoughtTradingBot::where('user_id', $user->id)->get();
        foreach ($bought_trading_bots as $bot) {
            $bot->active = 0;
            $bot->save();
        }

        BoughtTradingBot::where('bot_id', $trading_bot->id)->where('user_id', $user->id)->update([
            'active' => 1
        ]);
    }

    public function changeTradingBotAmount(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }
        $trading_bot = BoughtTradingBot::where('user_id', $user->id)->where('bot_id', $request->bot_id)->first();

        $trading_bot->update([
            'max_amount' => $request->amount
        ]);
    }

    public function depositToPammAccount(Request $request)
    {
        if (auth()->check()) {
            $user = auth()->user();
        } else {
            return 'Unauthorized';
        }
        $pamm_account = PammAccount::where('id', $request->pamm_id)->first();

        PammDeposit::create([
            'pamm_id' => $pamm_account->id,
            'user_id' => $user->id,
            'amount' => $request->amount
        ]);

        $user->update([
            'balance' => $user->balance - $request->amount
        ]);

        $pamm_account->update([
            'balance' => $user->balance + $request->amount
        ]);
    }

    public function withdrawFromPammAccount(Request $request)
    {
//        $user = auth()->user();
//        $pamm_account = PammAccount::where('id', $request->pamm_id)->first();

        //TODO: finish withdraw from Pamm Account mechanic
    }

    public function brokerHistoryMarginCallPreview(Request $request): JsonResponse
    {
        $position = OrdersHistory::where('id', $request->pos_id)->first();
        $user = User::where('id', $position->user_id)->first();
        $margin_call_positions = OrdersHistory::where('user_id', $user->id)
            ->whereBetween('created_at', [Carbon::parse($position->created_at)->subMinutes(1), Carbon::parse($position->created_at)->addMinutes(1)])
            ->get();
        $tickers = [];

        $margin_call_positions->each(function ($item) use ($user) {
            $item->action = 'delete';

            $redis_key = '';

            if ($item->type_of_market === 'Crypto') {
                $redis_key = redis_create_crypto_key("$item->pair:$item->currency");
            } elseif ($item->type_of_market === 'Stock') {
                $redis_key = redis_create_stocks_key("$item->pair:$item->currency");
            } elseif ($item->type_of_market === 'Commodities') {
                $redis_key = redis_create_commodities_key("$item->pair:$item->currency");
            }

            if (!$redis_key) {
                return response()->json([
                    'error' => 'Undefined market type. Cant get redis key',
                ]);
            }

            $price = redis_hgetall($redis_key)['p'];
            $random = $price * $user->positions_fee;

            if ($item->type === 'buy') {
                $item->now_pl = round(($price - $random - $item->limit) * $item->value, 2);
            } else {
                $item->now_pl = round(($item->limit - $price - $random) * $item->value, 2);
            }
        });

        return response()->json([
            'balance' => $margin_call_positions->first()->balance_before_close,
            'positions' => $margin_call_positions,
            'mc_date' => $position->created_at,
            'tickers' => $tickers
        ]);
    }

    public function brokerHistoryMarginCallPreviewConfirm(Request $request)
    {
        $positions = $request->positions;
        $new_balance = $positions[0]['balance_before_close'];

        $user = User::where('id', $positions[0]['user_id'])->first();

        // $user->update([
        // 'balance' => $user->balance + $new_balance
        // ]);

        foreach ($positions as $pos) {
            if ($pos['action'] === "restore") {
                Positions::where('value', $pos['value'])
                    ->where('amount', $pos['amount'])
                    ->where('limit', $pos['limit'])
                    ->where('pair', $pos['pair'])
                    ->where('currency', $pos['currency'])
                    ->where('user_id', $pos['user_id'])
                    ->restore();
            } else {
                $new_balance += $pos['amount'];
            }

            OrdersHistory::where('user_id', $pos['user_id'])->where('id', $pos['id'])->update([
                'deleted_at' => Carbon::now(),
                'status' => 'broker delete'
            ]);
        }

        $user->update([
            'balance' => $user->balance + $new_balance
        ]);

        Http::put(env('CRM_URL') . 'api/update-balance', [
            'email' => $user->email,
            'balance' => $user->balance
        ]);

        return response()->json([
            'success' => true
        ]);
    }

    /**
     * @param Agent $agent
     * @return string
     */
    private function getUserPlatform(Agent $agent): string
    {
        //        $platform = $agent->isDesktop() ? 'Desktop' : ($agent->isPhone() ? 'Phone' : ($agent->isTablet() ? 'Tablet' : 'Undefined'));
        $case = [
            $agent->isDesktop() => 'Desktop',
            $agent->isPhone() => 'Phone',
            $agent->isTablet() => 'Tablet',
        ];
        return $case[true];
    }
}
