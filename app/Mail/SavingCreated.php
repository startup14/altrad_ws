<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SavingCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $sa;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($saving_account)
    {
        $this->sa = $saving_account;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.saving_create');
    }
}
