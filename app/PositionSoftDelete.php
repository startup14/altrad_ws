<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PositionSoftDelete extends Model
{
    protected $table = 'positions_softdelete';
    protected $fillable = [
        'status',
        'entry_price',
        'type',
        'amount',
        'limit',
        'value',
        'pair',
        'currency',
        'liq',
        'lv',
        'user_id',
        'class',
        'leverage',
        'take_profit',
        'stop_loss',
        'type_of_market',
        'swop',
        'init_price',
        'unit',
        'balance_before',
        'balance_after',
        'crypto_amount'
    ];
}
