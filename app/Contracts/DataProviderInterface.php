<?php


namespace App\Contracts;


interface DataProviderInterface
{
    public function getTickers(array $symbols): array;
    public function getTickersAsync(array $redis_keys): array;
}
