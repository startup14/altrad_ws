<?php

namespace App\Contracts;

interface PreferencesInterface
{
    /**
     * @param string $key
     * @return string|null
     */
    public function __get (string $key): ?string;
}
