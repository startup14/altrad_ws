<?php


namespace App\Contracts;


interface CryptoProviderInterface extends DataProviderInterface
{
    public function getPricesAsync(array $params, int $concurrency = 11): array;
}
