<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'status',
        'remaining',
        'type',
        'amount',
        'limit',
        'value',
        'pair',
        'currency',
        'liq',
        'lv',
        'user_id',
        'class',
        'leverage',
        'take_profit',
        'stop_loss',
        'type_of_market',
        'unit',
        'crypto_amount'
    ];
}
