<?php


namespace App\QuoteProviders\CryptoCompare;

use App\QuoteProviders\BaseApi;
use App\Contracts\CryptoProviderInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request as Psr7Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use JsonException;

final class CryptoCompareAPI extends BaseApi implements CryptoProviderInterface
{
    protected string $endpoint = "https://min-api.cryptocompare.com";

    protected string $action_uri = 'data/pricehistorical';

    protected array $queryParams = [];


    public function __construct()
    {
        $this->queryParams['api_key'] = env('CRYPTO_COMPARE_API_KEY');
    }

    private function responseError(array $response, string $base, string $quote): bool
    {
        return
            ! Arr::has($response, implode('.', [$base, $quote]))
            && Arr::has($response, ['Response'])
            && $response['Response'] === 'Error';
    }

    private function getErrorMessage(array $response): string
    {
        return $response['Message'] . print_r($response['RateLimit'], true);
    }

    protected function preparePriceRequests(array $params): array
    {
        $requests = [];

        foreach ($params as $param) {
            [
                'ts' => $ts,
                'baseSymbol' => $fsym,
                'quoteSymbol' => $tsyms
            ] = $param;
            $requests[] = new Psr7Request('get',
                $this->createUrl($this->action_uri, compact('ts', 'fsym', 'tsyms'))
            );
        }

        return $requests;
    }

    public function getPricesAsync(array $params, int $concurrency = self::CONCURRENCY): array
    {
        $fulfilled = $rejected = [];
        $guzzle = new Client;
        $requests = $this->preparePriceRequests($params);

        $pool = new Pool($guzzle, $requests, [
            'concurrency' => $concurrency,
            'fulfilled' => function (Response $response, int $index) use (&$fulfilled, $params) {
                $json = (string) $response->getBody();
                try {
                    $decoded = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
                    $fields = $params[$index];
                    $fields['price'] = $decoded[$fields['baseSymbol']][$fields['quoteSymbol']];
                    $fulfilled[] = $fields;
                } catch (JsonException $e) {
                    Log::error($e->getMessage());
                }
            },
            'rejected' => function (RequestException $reason) use (&$rejected) {
                $rejected[] = $reason->getMessage();
            }
        ]);

        $promise = $pool->promise();
        $promise->wait();

        $this->logRejected($rejected);

        return $fulfilled;
    }

    public function getPrice($base, $quote): ?float
    {
        $response = $this->send('data/pricehistorical', [
            'fsym' => $base,
            'tsyms' => $quote,
        ]);

        if (isset($response)) {
            $response = $response->json();
            if ($this->responseError($response, $base, $quote)) {
                Log::error($this->getErrorMessage($response));
                return null;
            }
            return $response[$base][$quote];
        }

        return null;
    }

    public function getTickers(array $symbols): array
    {
        return ['not implemented'];
    }

    protected function prepareRequests(array $params): array
    {
        $requests = [];

        foreach ($params as $pair) {
            ['base' => $base, 'quote' => $quote] = redis_key_to_base_and_quote($pair);
            $symbol = $quote . $base;
            $requests[] = new Psr7Request('get',
                $this->createUrl($this->action_uri, compact('symbol'))
            );
        }

        return $requests;
    }

    public function getTickersAsync(array $redis_keys): array
    {
        return ['not implemented'];
    }
}
