<?php


namespace App\QuoteProviders\Binance;

use App\Console\Ship\Decorators\BinanceKlineDecorator;
use App\Console\Ship\Decorators\BinanceTickerDecorator;
use App\QuoteProviders\BaseApi;
use App\Contracts\CryptoProviderInterface;
use GuzzleHttp\Psr7\Request as Psr7Request;

final class BinanceAPI extends BaseApi implements CryptoProviderInterface
{
    /**
     * @var string
     */
    protected string $endpoint = "https://api.binance.com/api/v3";

    protected string $action_uri = 'ticker/24hr';

    /**
     * @var array
     */
    protected array $queryParams = [];

    protected function errorResponse(array $response): array
    {
        if (array_key_exists('code', $response) && $response['code'] < 0) {
            return $response;
        }
        return [];
    }

    public function getKline(string $symbol): ?array
    {
        $response = $this->send('klines', [
            'symbol' => $symbol,
        ]);

        $tickers = [];
        if (isset($response)) {
            $response = $response->json();
            if ($error = $this->errorResponse($response)) {
                return $error;
            }
            foreach ($response as $ticker) {
                $tickers[] = (new BinanceKlineDecorator())->dressUp($ticker, $symbol, $symbol);
            }
        }

        return $tickers;
    }

    protected function prepareRequests(array $params): array
    {
        $requests = [];

        foreach ($params as $pair) {
            ['base' => $base, 'quote' => $quote] = redis_key_to_base_and_quote($pair);
            $symbol = $base . $quote;
            $requests[] = new Psr7Request('get',
                $this->createUrl($this->action_uri, compact('symbol'))
            );
        }

        return $requests;
    }

    public function getTickersAsync(array $redis_keys): array
    {
        $tickers = $this->asyncGet($redis_keys);

        return array_map(static function ($ticket) {
            return (new BinanceTickerDecorator())->dressUp($ticket);
        }, $tickers);
    }

    public function getTickers(array $symbols = [], int $limit = 0): array
    {
        $tickers = [];
        $run_times = 0;

        foreach ($symbols as [$base, $quote]) {
            $tickers[] = $this->getTicker($base, $quote);
            ++$run_times;
            if ($limit > 0 && $run_times >= $limit) {
                break;
            }
        }

        return $tickers;
    }

    public function getTicker(string $base, string $quote): ?array
    {
        $response = $this->send('ticker/24hr', [
            'symbol' => "$base$quote",
        ]);

        return isset($response)
            ? (new BinanceTickerDecorator())->dressUp($response->json())
            : null;
    }

    public function getPricesAsync(array $params, int $concurrency = 11): array
    {
        $code = 500;
        $message = 'not implemented yet';
        return compact('code', 'message');
    }
}
