<?php

namespace App\QuoteProviders\Binance;

use App\QuoteProviders\BaseWebSocket;


class WebSocket extends BaseWebSocket
{
    /**
     * @var string
     */
    protected string $endpoint = "wss://stream.binance.com:9443/stream?streams=";

    /**
     * @var string
     */
    protected string $stream;

    /**
     * @var array
     */
    private array $pairs = [];

    public function __construct(array $keys, callable $onMessage)
    {
        $this->wsProvider = 'Binance';
        $this->wsProviderTitle = 'Binance WS';

        foreach ($keys as $redisKey) {
            ['quote' => $quote, 'base' => $base] = redis_key_to_base_and_quote($redisKey);
            $graph = "$base$quote";
            $this->pairs[$graph] = compact('base', 'quote');
        }

        $stream = array_map(static function ($graph) {
            return strtolower($graph) . "@miniTicker";
        }, array_keys($this->pairs));

        $this->stream = implode('/', $stream);
        $this->endpoint .= $this->stream;

        parent::__construct(function (array $message) use ($onMessage) {
            $message = $message['data'] ?? [];
            if (isset($message['s'], $message['e']) && $message['e'] === '24hrMiniTicker') {
                $answer = $this->answerDecoration($message);
                $onMessage($answer);
            }
        });
    }

    private function answerDecoration(array $message): array
    {
        return [
            'quote' => $this->pairs[$message['s']]['quote'],
            'base'  => $this->pairs[$message['s']]['base'],
            'open'  => (DOUBLE)$message['o'],
            'close' => (DOUBLE)$message['c'],
            'high'  => (DOUBLE)$message['h'],
            'low'   => (DOUBLE)$message['l'],
            'vol'   => (DOUBLE)$message['v'],
        ];
    }

    protected function fromKeysToSymbols(array $redis_keys): array
    {
        // TODO: Implement fromKeysToSymbols() method.
        return $redis_keys;
    }

    protected function requestSubscribes(array $symbols): array
    {
        // TODO: Implement requestSubscribes() method.
        return $symbols;
    }
}
