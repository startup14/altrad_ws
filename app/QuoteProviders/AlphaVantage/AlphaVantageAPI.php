<?php

namespace App\QuoteProviders\AlphaVantage;

use App\QuoteProviders\BaseApi;
use App\Contracts\StocksProviderInterface;
use Carbon\Carbon;
use Illuminate\Http\Response;

class AlphaVantageAPI extends BaseApi implements StocksProviderInterface
{
    /**
     * @var string
     */
    protected string $endpoint = "https://www.alphavantage.co";

    /**
     * @var array
     */
    protected array $queryParams = [];

    /**
     * @var array
     */
    private array $intervalNames = [
        '1M' => 'TIME_SERIES_MONTHLY_ADJUSTED',
        '1w' => 'TIME_SERIES_WEEKLY_ADJUSTED',
        '1d' => 'TIME_SERIES_DAILY_ADJUSTED',
        //'1h' => 'TIME_SERIES_INTRADAY',
    ];

    /**
     * @var array
     */
    private array $subIntervals = [
        '1h' => '60min',
        '30m' => '30min',
        '15m' => '15min',
        '5m' => '5min',
        '1m' => '1min',
    ];

    public function __construct()
    {
        //parent::__construct();
        $this->queryParams = ['apikey' => env('ALPHAVANTAGE_API_KEY')];
    }

    public function getKline(string $symbol, string $interval): array
    {
        $params = [
            'symbol' => $symbol
        ];

        if (isset($this->intervalNames[$interval])) {
            $params['function'] = $this->intervalNames[$interval];
            switch ($interval) {
                case '1M':
                    $dif = 2629746;
                    $answerKey = 'Monthly Adjusted Time Series';
                    break;
                case '1w':
                    $dif = 604800;
                    $answerKey = 'Weekly Adjusted Time Series';
                    break;
                case '1d':
                    $dif = 86400;
                    $answerKey = 'Time Series (Daily)';
                    break;
            }
            $dateFormat = 'Y-m-d';
        } elseif (isset($this->subIntervals[$interval])) {
            $params['function'] = 'TIME_SERIES_INTRADAY';
            $params['interval'] = $this->subIntervals[$interval];
            switch ($interval) {
                case '1h':
                    $dif = 3599;
                    $answerKey = 'Time Series (60min)';
                    break;
                case '30m':
                    $dif = 1799;
                    $answerKey = 'Time Series (30min)';
                    break;
                case '15m':
                    $dif = 899;
                    $answerKey = 'Time Series (15min)';
                    break;
                case '5m':
                    $dif = 299;
                    $answerKey = 'Time Series (5min)';
                    break;
                case '1m':
                    $dif = 59;
                    $answerKey = 'Time Series (1min)';
                    break;
            }
            $dateFormat = 'Y-m-d H:i:s';
        } else {
            return [];
        }

        $response = $this->send('query', $params);
        $tickers = [];
        if (isset($response)) {
            $response = $response->json();

            if (isset($response[$answerKey])) {
                foreach ($response[$answerKey] as $date => $ticker) {
                    $timestamp = Carbon::createFromFormat(
                        $dateFormat,
                        $date,
                        $response['Meta Data']['5. Time Zone'] ?? $response['Meta Data']['6. Time Zone']
                    )->timestamp;
                    $openTime = "{$timestamp}000";
                    $closeTime = $timestamp + $dif . "999";
                    $tickers[] = $this->tickerDecoration(
                        $symbol,
                        $ticker,
                        $openTime,
                        $closeTime
                    );
                }
            }
        }
        return $tickers;

    }

    private function tickerDecoration(string $symbol, array $data, int $openTime, int $closeTime): array
    {
        $ticker = [
            'symbol' => $symbol,
            'open_time' => $openTime,
            'close_time' => $closeTime,
            'open' => (DOUBLE)$data['1. open'],
            'high' => (DOUBLE)$data['2. high'],
            'low' => (DOUBLE)$data['3. low'],
            'close' => (DOUBLE)$data['4. close'],
        ];

        if (isset($data['5. volume'])) {
            $ticker['vol'] = $data['5. volume'];
        } elseif(isset($data['6. volume'])) {
            $ticker['vol'] = $data['6. volume'];
            $ticker['adj_close'] = (DOUBLE)$data['5. adjusted close'];
        }

        return $ticker;
    }

    public function getTickers(array $symbols): array
    {
        $code = Response::HTTP_NOT_IMPLEMENTED;
        $msg = 'Not implemented yet';
        return compact('code', 'msg');
    }

    public function getTickersAsync(array $redis_keys): array
    {
        $code = Response::HTTP_NOT_IMPLEMENTED;
        $msg = 'Not implemented yet';
        return compact('code', 'msg');
    }
}
