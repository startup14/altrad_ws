<?php


namespace App\QuoteProviders\Polygon;

use App\QuoteProviders\BaseApi;
use App\Contracts\CommoditiesProviderInterface;
use Carbon\Carbon;

class PolygonAPI extends BaseApi implements CommoditiesProviderInterface
{
    /**
     * @var string
     */
    protected string $endpoint = "https://api.polygon.io";

    /**
     * @var array
     */
    protected array $queryParams = [];

    /**
     * @var array
     */
    private array $timespans = [
        '1M' => "month",
        '1w' => "week",
        '1d' => "day",
        '1h' => "hour",
    ];


    public function __construct()
    {
        //parent::__construct();

        $this->queryParams = [
            'apikey' => env('POLYGON_API_KEY'),
        ];
    }

    /**
     * Market status check
     *
     * @return array
     */
    public function checkMarketStatus(): array
    {
        $market = [
            'open' => false,
        ];

        $response = $this->send("v1/marketstatus/now");

        if (isset($response)) {
            $response = $response->json();
            if (isset($response['exchanges'], $response['serverTime'])) {
                if ($response['exchanges']['nasdaq'] === "open") {
                    $market['open'] = true;
                } else {
                    //sleep(5);
                    $market['time'] = date(
                        'H:i:s', strtotime('13:30:00') - strtotime($response['serverTime'])
                    );
                }
            }
        }

        return $market;
    }


    public function getKline(string $symbol, string $interval, string $from, string $to): ?array
    {

        if (in_array($interval, ['1M', '1w', '1d', '1h'])) {
            $multiplier = 1;
            $timespan = $this->timespans[$interval];
            switch ($interval) {
                case '1M':
                    $dif = 2629746;
                    break;
                case '1w':
                    $dif = 604800;
                    break;
                case '1d':
                    $dif = 86400;
                    break;
                case '1h':
                    $dif = 3599;
                    break;
            }
        } elseif (in_array($interval, ['30m', '15m', '5m', '1m'])) {
            $timespan = 'minute';
            $multiplier = (INT)(preg_replace("/\D+$/", '', $interval));
            $dif = $multiplier * 60 - 1;
        } else {
            return null;
        }
        $response = $this->send(
            "v2/aggs/ticker/C:{$symbol}USD/range/{$multiplier}/{$timespan}/{$from}/{$to}"
        );

        $tickers = [];
        if (isset($response)) {
            $response = $response->json();

            foreach ($response['results'] ?? [] as $ticker) {
                $tickers[] = $this->tickerDecoration($ticker, $symbol, $dif);
            }
        }
        return $tickers;
    }

    public function getTickers(array $symbols = []): array
    {
        $response = $this->send('v2/snapshot/locale/global/markets/forex/tickers', [
            'tickers' => implode(',', array_map(
                fn (string $symbol):string => "C:{$symbol}USD",
                $symbols
            )),
        ]);

        $tickers = [];
        if (isset($response)) {
            $response = $response->json();
            if (isset($response['tickers'])) {
                foreach ($response['tickers'] as $ticker) {
                    $symbol = substr($ticker['ticker'], 2, 3);
                    $tickers[$symbol] = $this->tickerDecoration($ticker['day'], $symbol);
                }
            }
        }

        return $tickers;
    }

    // todo: extract to Ship/Decorators
    private function tickerDecoration(array $data, string $symbol, ?int $dif = null): array
    {
        $ticker = [
            'symbol' => $symbol,
            'open' => round($data['o'], 2),
            'close' => round($data['c'], 2),
            'low'  => round($data['l'], 2),
            'high' => round($data['h'], 2),
            'vol' => $data['v'],
        ];
        if (isset($data['t'])) {
            $ticker['open_time'] = $data['t'];
            if ($dif) {
                $ticker['close_time'] = (Carbon::createFromTimestampMs($data['t'])->timestamp + $dif) . '999';
            }
        }

        return $ticker;
    }

    public function getTickersAsync(array $redis_keys): array
    {
        return [];
    }
}
