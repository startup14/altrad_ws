<?php

namespace App\QuoteProviders;

use Exception;
use Illuminate\Support\Facades\Log;
use React\EventLoop;
use Ratchet\Client as Ratchet;
use Ratchet\RFC6455\Messaging;

use App\Logger;
use React\EventLoop\Loop;

abstract class BaseWebSocket
{

    protected $app;
    protected EventLoop\LoopInterface $loop;
    protected Ratchet\Connector $connector;

    protected string $wsProvider = __CLASS__;
    protected string $wsProviderTitle = __CLASS__;
    protected string $wsProviderSecret;

    /**
     * @var array
     */
    private array $subscribes;

    /**
     * @var
     */
    private  $onMessage;

    abstract protected function fromKeysToSymbols(array $redis_keys): array;
    abstract protected function requestSubscribes(array $symbols): array;

    public function __construct(callable $onMessage, array $subscribes = [])
    {
        $this->onMessage = $onMessage;
        $this->subscribes = $subscribes;
        $this->connect();
    }

    private function connect(): void
    {
        $this->loop = Loop::get();
        $this->connector = new Ratchet\Connector($this->loop);
        $this->reconnect();
        $this->loop->run();
    }

    protected function reconnect(): void
    {
        ($this->connector)($this->endpoint)->then(
            function (Ratchet\WebSocket $connect) {
                $connect->on('message', function (Messaging\MessageInterface $message) {
                    try {
                        $msg = json_decode($message, true, 512, JSON_THROW_ON_ERROR);
                        ($this->onMessage)($msg);
                    } catch (Exception $exception) {
                        echo $exception->getMessage();
                    }
                });

                $connect->on('close', function ($code = null, $reason = null) {
                    $this->onClose($code, $reason);
                });

                $connect->on('ping', function (Messaging\FrameInterface $frame) use ($connect) {
                    $connect->send(new Messaging\Frame('', true, Messaging\Frame::OP_PING));
                });

                $flag = true;
                $connect->on('pong', function (Messaging\FrameInterface $frame) use ($connect, &$flag) {
                    $connect->send(new Messaging\Frame('', true, Messaging\Frame::OP_PONG));
                });

                foreach ($this->subscribes as $subscribe) {
                    $connect->send(
                        new Messaging\Frame(json_encode($subscribe, JSON_THROW_ON_ERROR), true, Messaging\Frame::OP_TEXT)
                    );
                }
            },
            function (Exception $exception) {
                Logger::create([
                    'description' => "Could not connect: {$exception->getMessage()}"
                ]);
                $this->loop->stop();
            }
        );
    }

    private function onClose($code = null, $reason = null): void
    {
        $this->loop->addTimer(3, function () use ($code, $reason) {
            Log::warning(sprintf('%s. Socket closed: [%d] %s', $this->wsProviderTitle, $code, $reason));
            $this->reconnect();
        });
    }
}
