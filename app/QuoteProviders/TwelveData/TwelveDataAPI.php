<?php


namespace App\QuoteProviders\TwelveData;

use App\Contracts\StocksProviderInterface;
use App\QuoteProviders\BaseApi;
use Exception;
use Illuminate\Http\Client\HttpClientException;
use Illuminate\Support\Facades\Log;
use LogicException;

class TwelveDataAPI extends BaseApi implements StocksProviderInterface
{
    /**
     * @var string
     */
    protected string $endpoint = "https://api.twelvedata.com";

    /**
     * @var array
     */
    protected array $queryParams = [];


    public function __construct()
    {
        //parent::__construct();

        $this->queryParams = [
            'apikey' => env('TWELVE_API_KEY'),
        ];
    }

    protected function statusError(array $response): bool
    {
        return isset($response['status']) && $response['status'] === 'error';
    }

    private function damagedTicker($ticker): bool
    {
        return is_string($ticker) || array_key_exists('code', $ticker);
    }

    /**
     */
    public function getTickers(array $symbols): array
    {
        $tickers = [];

        try {
            $response = $this->send('quote', [
                'symbol' => implode(',', $symbols),
            ]);

            if (isset($response) && $response->successful()) {
                $response = $response->json();
                if ($this->statusError($response)) {
                    throw new HttpClientException($response['message']);
                }

                foreach ($response as $symbol => $ticker) {
                    if ($this->damagedTicker($ticker)) {
                        Log::error("TwelveDataAPI error for <$symbol>: ". $ticker['message'] ?? $ticker);
                        continue;
                    }
                    if (in_array($symbol, $symbols, true)) {
                        $tickers[$symbol] = $this->tickerDecoration($ticker);
                    }
                }
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }

        return $tickers;
    }

    private function tickerDecoration(array $ticker): array
    {
        if (! array_key_exists('symbol', $ticker)) {
            throw new LogicException('Ticker have not symbol: ' . print_r($ticker, true));
        }

        return [
            'symbol' => $ticker['symbol'],
            'open' => isset($ticker['open'])
                ? round((DOUBLE)$ticker['open'], 2)
                : null,
            'close' => isset($ticker['close'])
                ? round((DOUBLE)$ticker['close'], 2)
                : null,
            'prev_close' => isset($ticker['previous_close'])
                ? round((DOUBLE)$ticker['previous_close'], 2)
                : null,
            'high' => isset($ticker['high'])
                ? round((DOUBLE)$ticker['high'], 2)
                : null,
            'low' => isset($ticker['low'])
                ? round((DOUBLE)$ticker['low'], 2)
                : null,
            'vol' => $ticker['volume'] ?? null,
        ];
    }

    public function getTickersAsync(array $redis_keys): array
    {
        throw new LogicException('Sorry, not implemented yet: ' . __METHOD__);
    }
}
