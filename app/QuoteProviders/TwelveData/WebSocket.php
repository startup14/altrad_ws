<?php

namespace App\QuoteProviders\TwelveData;

use App\QuoteProviders\BaseWebSocket;

class WebSocket extends BaseWebSocket
{
    /**
     * @var string
     */
    protected string $endpoint = "wss://ws.twelvedata.com/v1/quotes/price";

    /**
     * @var string
     */
    protected string $stream;

    protected function fromKeysToSymbols(array $redis_keys): array
    {
        return array_map(static function (string $key) {
            return redis_key_to_base_and_quote($key)['base'];
        }, $redis_keys);
    }

    protected function requestSubscribes(array $symbols): array
    {
        return [
            [
                'action' => 'subscribe',
                'params' => [
                    'symbols' => implode(',', $symbols),
                ]
            ]
        ];
    }

    public function __construct(array $redis_keys, callable $onMessage)
    {
        $this->endpoint .= "?apikey=" . env('TWELVE_API_KEY');

        $this->wsProvider = 'Twelve';
        $this->wsProviderTitle = 'Twelve WS';

        $symbols = $this->fromKeysToSymbols($redis_keys);
        $subscribes = $this->requestSubscribes($symbols);

        parent::__construct(function (array $message) use ($onMessage) {
            $answer = $this->answerDecoration($message);
            $onMessage($answer);
        }, $subscribes);
    }

    private function answerDecoration(array $message = []): array
    {
        if ($message['event'] === 'price') {
            $message['close'] = (double)$message['price'];
            unset($message['price']);
        }
        if (isset($message['day_volume'])) {
            $message['vol'] = $message['day_volume'];
            unset($message['day_volume']);
        }

        return $message;
    }
}
