<?php


namespace App\QuoteProviders;

use App\Logger;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Response as Psr7Response;
use Illuminate\Http\Client\Response as HttpResponse;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use JsonException;

abstract class BaseApi
{
    public const CONCURRENCY = 11;

    protected array $queryParams;

    public function __call(string $name, array $args): self
    {
        $this->queryParams[$name] = $args[0];

        return $this;
    }

    protected function createUrl(string $path, array $queryParams = []): string
    {
        $endpoint = "$this->endpoint/$path";

        $params = array_merge($this->queryParams, $queryParams);

        if (count($params)) {

            $query = implode('&',
                array_map(static function ($key, $value) {
                    return "$key=$value";
                }, array_keys($params), array_values($params))
            );
            $endpoint .= "?$query";
        }

        return $endpoint;
    }

    /**
     * @param string $path
     * @param array $queryParams
     * @return HttpResponse|null
     */
    protected function send(string $path, array $queryParams = []): ?HttpResponse
    {
        $endpoint = "$this->endpoint/$path";

        $params = array_merge($this->queryParams, $queryParams);

        if (count($params)) {

            $query = implode('&',
                array_map(static function ($key, $value) {
                    return "$key=$value";
                }, array_keys($params), array_values($params))
            );
            $endpoint .= "?$query";
        }

        try {
            return Http::get($endpoint);
        } catch (\Exception $exception) {
            Logger::create([
                'description' => "Request error - $endpoint"
            ]);
        }
        return null;
    }

    protected function keyCodeIn(array $response, array $codes): bool
    {
        return array_key_exists('code', $response) && in_array($response['code'], $codes, false);
    }

    protected function errorResponse(array $response): array
    {
        return [];
    }

    protected function prepareRequests(array $params): array
    {
        return [];
    }

    protected function logRejected(array $rejected): void
    {
        foreach ($rejected as $item) {
            Log::error($item);
        }
    }

    protected function asyncGet(array $params, int $concurrency = self::CONCURRENCY): array
    {
        $guzzleClient = new GuzzleClient();

        $requests = $this->prepareRequests($params);

        $fulfilled = [];
        $rejected = [];

        $requestPool = new Pool($guzzleClient, $requests, [
            'concurrency' => $concurrency,
            'fulfilled' => function (Psr7Response $response, int $index) use (&$fulfilled, $params)
            {
                $json = (string) $response->getBody();
                try {
                    $decoded = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
                    ['base' => $base, 'quote' => $quote] = redis_key_to_base_and_quote($params[$index]);
                    $decoded = array_merge(compact('base', 'quote'), $decoded);
                    $fulfilled[] = $decoded;
                } catch (JsonException $e) {
                    Log::error($e->getMessage());
                }
            },
            'rejected' => function (RequestException $reason) use (&$rejected)
            {
                $rejected[] = $reason->getMessage();
            }
        ]);

        $promise = $requestPool->promise();
        $promise->wait();

        $this->logRejected($rejected);

        return $fulfilled;
    }
}
