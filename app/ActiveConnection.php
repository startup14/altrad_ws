<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActiveConnection extends Model
{
    protected $fillable = [
        'user_id',
        'activity_token'
    ];
}
