<?php

namespace App\Services;

use App\Preferences;
use App\Contracts\PreferencesInterface;

class PreferencesService implements PreferencesInterface
{
    /**
     * @var array
     */
    private $preferences = [];

    public function __construct()
    {
        $preferences = Preferences::all();

        foreach ($preferences as $item) {
            $this->preferences[$item->key] = $item->value;
        }
    }

    /**
     * @param string $key
     * @return string|null
     */
    public function __get (string $key): ?string
    {
        return $this->preferences[$key] ?? null;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function __set(string $key, string $value): void
    {
        $this->preferences[$key] = $value;
    }
}
