<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Spatie\ShortSchedule\ShortSchedule;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\SetPrices::class,
        Commands\OrderCheck::class,
        Commands\PositionCheck::class,
        Commands\SetOrderBook::class,
        Commands\CheckSwop::class,
        Commands\updateMarketStatus::class,
        Commands\requestStocks::class,
        Commands\checkTokenForValid::class,
        Commands\updateCurrencyFee::class,
        Commands\saveCryptoChart::class,
        Commands\saveStockChart::class,
        Commands\RemoveOldHistoricalData::class,
        Commands\updateCryptoData::class,
        Commands\CheckBannedActives::class,
        Commands\CheckAbnormalProfit::class,

        Commands\RefreshStocks::class,
        Commands\RefreshCommodities::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
    }

    protected function shortSchedule(ShortSchedule $schedule): void
    {
        $schedule->command('signals:generate')->everySeconds(60);
        $schedule->command('swop:check')->everySeconds(60);
        $schedule->command('book:set')->everySeconds(2)->withoutOverlapping();
        $schedule->command('order:check')->everySecond();
        $schedule->command('position:check')->everySecond()->withoutOverlapping();
        $schedule->command('market:status')->everySecond()->withoutOverlapping();

        $schedule->command('copy-trader-position:generate')->everySeconds(60);
        $schedule->command('copy-trader-position:close')->everySeconds(60);
        $schedule->command('margin-call:check')->everySecond()->withoutOverlapping();
        $schedule->command('token:check')->everySeconds(30);
        $schedule->command('currency-fee:update')->everySecond();

//        $schedule->command('commodities:refresh')->everySecond(3)->withoutOverlapping();
//        $schedule->command('stocks:refresh')->everySecond(15)->withoutOverlapping(); // migrated & debugged

        $schedule->command('crypto-chart:store')->everySecond()->withoutOverlapping();
        $schedule->command('stock-chart:store')->everySecond()->withoutOverlapping();
        $schedule->command('commodities-chart:store')->everySecond(3)->withoutOverlapping();

        $schedule->command('historical-data:clear')->everySeconds(60);

        $schedule->command('crypto:data')->everySeconds(60)->withoutOverlapping();
        $schedule->command('stock:data')->everySeconds(60)->withoutOverlapping();
        $schedule->command('commodities:data')->everySeconds(60)->withoutOverlapping();

        $schedule->command('actives:check')->everySecond()->withoutOverlapping();
        $schedule->command('abnormal-profit:check')->everySeconds(60)->withoutOverlapping();
        $schedule->command('inactive-sessions:end')->everySeconds(60)->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
