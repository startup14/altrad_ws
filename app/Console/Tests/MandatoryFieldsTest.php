<?php

uses(
    Tests\TestCase::class,
);

test('all required fields set', function (array $required_keys, bool $fix = false) {

    $keyspaces = redis_all_keyspaces();
    $damaged = [];

    foreach ($keyspaces as $keyspace) {
        $cache = redis_hgetall_for_keyspace($keyspace);

        foreach ($cache as $key => $fields) {
            $all_keys_set = all_required_keys($fields, $required_keys);
            if (! $all_keys_set) {
                $damaged[$key] = $fields;
            }
        }
        if ($fix) {
            foreach ($damaged as $key => $fields) {
                $fields['p'] = 'n/a';
                $fields['active'] = 0;
                redis_hmset($key, $fields);
                echo sprintf('Key %s fixed\n', $key);
            }
            $damaged = [];
        }
        $this->assertEmpty(
            $damaged,
            sprintf("Damaged keys, total: %d, [%s]",
                count($damaged),
                implode(', ',  array_keys($damaged))
            )
        );
    }

})->with([
    [['p'], false],
]);
