<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\RefreshCommand;
use App\Console\Ship\Transformers\DefaultTickerTransformer;
use App\Console\Traits\CommandUsesRedis;

use App\Contracts\StocksProviderInterface as StocksProvider;

// https://api.twelvedata.com/quote?symbol=AAPL,AMZN&apikey=88e43b98effb4571b46fec0616b283af&interval=1da

class RefreshStocks extends RefreshCommand
{
    use CommandUsesRedis;

    protected $signature = 'stocks:refresh';
    protected $description = 'Command description';

    private StocksProvider $provider;

    private int $array_chunk_size = 13;

    public function __construct()
    {
        parent::__construct();
        $this->provider = app(StocksProvider::class);
    }

    public function handle(): int
    {

        $stocks = $this->ensureHashKeysExist(REDIS_STREAM_STOCKS, 'stocks:init');

        $stock_keys = redis_keys_for_keyspace_base(REDIS_STREAM_STOCKS);

        $chunks = array_chunk(
            $stock_keys,
            $this->array_chunk_size, true
        );

        $updated = [];

        foreach ($chunks ?? [] as $chunk) {
            $tickers = $this->provider->getTickers($chunk);

            if (isset($tickers)) {
                foreach ($tickers as $ticker) {
                    $redis_key = redis_create_stocks_key($ticker['symbol']);
                    $parent = $stocks[$redis_key];
                    $trait = (new DefaultTickerTransformer)->transform($ticker);
                    $updated[$redis_key] = array_merge($parent, $trait);
                }
            }
        }

        redis_hmsetall($updated);
        $proceeded = count($updated);

        $this->logProcessed($proceeded);

        return $proceeded;
    }
}
