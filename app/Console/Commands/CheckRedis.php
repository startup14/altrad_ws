<?php

namespace App\Console\Commands;

use App\Console\Traits\SmartOutput;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class CheckRedis extends Command
{
    use SmartOutput;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Redis Alive';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        try {
            $redis_info = Redis::info('server');
        } catch (Exception $e) {
            Log::error($e->getMessage());
            echo $e->getMessage();
            return -1;
        }

        $output = array_map(static function ($key, $value) {
            return "$key: $value";
            }, array_keys($redis_info), array_values($redis_info)
        );

        $this->smartOutput($output);

        return 0;
    }
}
