<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\CommandUsesHistorical;
use App\HistoricalData;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class saveCryptoChart extends BaseCommand
{
    use CommandUsesHistorical;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crypto-chart:store';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle(): int
    {
        $cryptos = redis_hgetall_for_keyspace('crypto');

        foreach ($cryptos as $redis_key => $quote) {
            ['quote' => $quoteSymbol, 'base' => $baseSymbol] = redis_key_to_base_and_quote($redis_key);
            if (!in_array($quoteSymbol, ['name', 'active'])) {
                $this->storeDataForChart("$baseSymbol$quoteSymbol", $quote);
            }
        }

        return 0;
    }

    /**
     * @throws \Exception
     */
    public function storeDataForChart($symbol, $data): bool
    {
        $price = $this->ensureKeyExists('p', $data);
        $periods = $this->getPeriods();
        $sym_fee = Cache::get('currency-fees');

        foreach ($periods as $period => $time) {
            $check = $this->historicalDataFor($symbol, $period);
            $fee = $this->chooseFee($sym_fee, $symbol);

            if ($check) {
                $data['v'] = $this->ensureKeyExists('v', $data);
                $v = $this->setupVolume($check, $period, $data, $time);
                $prev = $this->historicalPrevDataFor($symbol, $period, $check->open_time - 1);

                if ($fee) {
                    $price += $fee['now'] ?? 0;
                }

                $check->update([
                    'close' => $price,
                    'high' => $check->high > $price ? $check->high : $price,
                    'low' => $check->low < $price ? $check->low : $data['p'] + $fee,
                    'open' => $prev->close ?? $data['pc'],
                    'volume' => $v,
                ]);
            } else {
                $prev = $this->historicalPrevDataFor($symbol, $period, Carbon::now()->timestamp - 1);

                if ($fee) {
                    $price += $fee['now'] ?? 0;
                }

                HistoricalData::create([
                    'open_time' => $time['start'],
                    'close_time' => $time['end'],
                    'close' => $price,
                    'high' => $price,
                    'low' => $price,
                    'open' => $prev->close ?? $data['o'],
                    'time_period' => $period,
                    'symbol' => $symbol,
                    'volume' => 0
                ]);
            }
        }

        return true;
    }
}
