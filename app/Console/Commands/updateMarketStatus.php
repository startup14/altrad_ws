<?php

namespace App\Console\Commands;

use App\Contracts\CommoditiesProviderInterface as CommoditiesProvider;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class updateMarketStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'market:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var CommoditiesProvider
     */
    private $provider;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->provider = app(CommoditiesProvider::class);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $openTimeLeft = strtotime('12:30:00') - time();
        $closeTimeLeft = strtotime('21:00:00') - time();

        if (($openTimeLeft < 0) && ($closeTimeLeft > 0)) {
            $market = $this->provider->checkMarketStatus();
        } else {
            $market = [
                'open' => false,
                'time' => date('H:i:s', strtotime('13:30:00') - time()),
            ];
        }

        Cache::put('market', $market);

        return 0;
    }
}
