<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use Carbon\Carbon;

class updateCryptoData extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crypto:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        if ((Carbon::now()->timestamp - Carbon::today()->timestamp) < 60) {
            $cryptos = redis_hgetall_for_keyspace('crypto');

            foreach ($cryptos as $redis_key => $fields) {
                ['quote' => $quoteSymbol] = redis_key_to_base_and_quote($redis_key);
                if (!in_array($quoteSymbol, ['name', 'active'])) {
                    $fields['p'] = $this->ensureKeyExists('p', $fields);
                    $fields['o'] = $fields['p'];
                    $fields['pc'] = $fields['p'];
                    $fields['spread'] = 0;
                }
                $cryptos[$redis_key] = $fields;
            }

            redis_hmsetall($cryptos);
        }

        return 0;
    }
}
