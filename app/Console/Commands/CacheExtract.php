<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CacheExtract extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:extract {keyspace}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Extract data from cache';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \JsonException
     */
    public function handle(): int
    {
        $cache = redis_hgetall_for_keyspace($this->argument('keyspace'));
        echo json_encode($cache, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT);
        return count($cache);
    }
}
