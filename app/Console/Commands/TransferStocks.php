<?php

namespace App\Console\Commands;


use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\CommandAbleTransfer;

final class TransferStocks extends BaseCommand
{
    use CommandAbleTransfer;

    protected $signature = 'stocks:transfer';
    protected $description = 'Transfer stocks data from prev stage via API.';

    private function transferStocks(string $server, string $path): int
    {
        return $this->transferFrom(REDIS_STREAM_STOCKS, $server, $path);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $proceeded = $this->transferStocks(env('TRANSFER_FROM_API_HOST'), 'currency-rate');
        $this->logProcessed($proceeded);

        return $proceeded;
    }
}
