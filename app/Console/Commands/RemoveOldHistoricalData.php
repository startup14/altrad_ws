<?php

namespace App\Console\Commands;

use App\HistoricalData;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RemoveOldHistoricalData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'historical-data:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        if ((Carbon::now()->timestamp - Carbon::today()->timestamp) < 60) {
            HistoricalData::whereNotIn('time_period', ['1M', '1w'])->where('created_at', '<', Carbon::yesterday())->delete();
            HistoricalData::where('time_period', '1w')->where('created_at', '<', Carbon::now()->startOfWeek()->subWeek())->delete();
            HistoricalData::where('time_period', '1M')->where('created_at', '<', Carbon::now()->startOfMonth()->subMonth())->delete();
        }

        HistoricalData::where('time_period', '1m')->where('created_at', '<', Carbon::now()->subMinutes(2))->delete();

        return 0;
    }
}
