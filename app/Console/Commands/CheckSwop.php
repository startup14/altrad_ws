<?php

namespace App\Console\Commands;

use App\Positions;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class CheckSwop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'swop:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check position if created not today additional swop';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $today = Carbon::today();
        $positions = Positions::where('created_at', '<=', $today)->get();

        if ((Carbon::now()->timestamp - Carbon::today()->timestamp) < 60) {
            foreach ($positions as $position) {
                if ($position->type_of_market === 'Crypto') {
                        $position->swop += 0.005;
                        $position->save();
                } elseif ($position->type_of_market === 'Stock') {
                    $day = Carbon::now()->dayOfWeek;
                    if (in_array($day, [0, 6])) {
                        continue;
                    }
                    $position->swop += 0.0025;
                    $position->save();
                }
            }
        }

        return 0;
    }
}
