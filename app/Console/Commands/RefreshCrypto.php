<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\RefreshCommand;
use App\Console\Ship\Transformers\DefaultTickerTransformer;
use App\Console\Traits\CommandUsesRedis;
use App\Contracts\CryptoProviderInterface as CryptoProvider;

class RefreshCrypto extends RefreshCommand
{
    use CommandUsesRedis;

    protected $signature = 'crypto:refresh';
    protected $description = 'Command description';

    private CryptoProvider $provider;

    public function __construct()
    {
        parent::__construct();
        $this->provider = app(CryptoProvider::class);
    }

    public function handle(): int
    {
        $crypto_keys = $this->ensureHashKeysExist(REDIS_STREAM_CRYPTO, 'crypto:init');
        $tickers = $this->provider->getTickersAsync(array_keys($crypto_keys));

        $updated = [];

        foreach ($tickers ?? [] as $ticker) {
            $pair = implode(REDIS_KEY_SEPARATOR, [$ticker['base'], $ticker['quote']]);
            $redis_key = redis_create_crypto_key($pair);
            if (array_key_exists($redis_key, $crypto_keys)) {
                $parent = redis_hgetall($redis_key);
                $trait = (new DefaultTickerTransformer)->transform($ticker);
                $updated[$redis_key] = array_merge($parent, $trait);
            }
        }

        redis_hmsetall($updated);

        $processed = count($updated);
        $this->logProcessed($processed);

        return $processed;
    }
}
