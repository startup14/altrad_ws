<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\InitCommand;

class CommoditiesInit extends InitCommand
{
    protected $signature = 'commodities:init';

    protected $description = 'Load commodities template';

    private function convertCommodities(string $prev_fn, string $new_fn): int
    {
        $prev = include storage_path($prev_fn);
        $new = [];
        foreach ($prev as $key => $fields) {
            $fields['active'] = 1;
            $new[$key] = $fields;
        }
        return
            file_put_contents(
                storage_path($new_fn), "<?php \n return " . var_export($new, true) . ';'
            ) === 0;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $this->unlinkKeys(REDIS_STREAM_COMMO);

        $processed = $this->loadTemplateFromStorageToRedis('symbols/commodities-v2.php', 'redis_create_commodities_key');
        $this->logProcessed($processed);

        return $processed;

//        return $this->convertCommodities('symbols/commodities.php', 'symbols/commodities-v2.php');
    }
}
