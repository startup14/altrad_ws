<?php

namespace App\Console\Commands;

use App\Console\Traits\CommandUsesRedis;
use Illuminate\Console\Command;

final class CryptoTemplateConvert extends Command
{
    use CommandUsesRedis;

    public const SOURCE_FILE = 'symbols/crypto.php';
    public const TARGET_FILE = 'symbols/crypto-v2.php';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crypto:crypto2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Converts symbols/crypto.php template to version for use with Redis';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $keys = [];

        $template = include storage_path(self::SOURCE_FILE);

        foreach ($template as $base_key => $base) {
            foreach ($base['quotes'] as $quote => $fields) {
                $key = implode(REDIS_KEY_SEPARATOR, [$base_key, $quote]);
                $fields['active'] = 1;
                $fields['name'] = $base['name'];
                $keys[$key] = $fields;
            }
        }

        return
            file_put_contents(
                storage_path(self::TARGET_FILE), "<?php \n return " . var_export($keys, true) . ';'
            ) === 0;

    }
}
