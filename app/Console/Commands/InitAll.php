<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\ExecuteCommandsGroup;

class InitAll extends BaseCommand
{
    use ExecuteCommandsGroup;

    protected $signature = 'init:all';
    protected $description = 'Execute init for all quotes (crypto, stocks and commo)';

    public function handle(): int
    {
        $commands = ['crypto:init', 'stocks:init', 'commodities:init'];
        return $this->handleCommands($commands);
    }
}
