<?php

namespace App\Console\Commands;

use App\HistoricalData;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use WebSocket;
use Illuminate\Support\Facades\Log;

class WebsocketCCcConnect extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'websocket-ccc:connect';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Connects to cryptocompare websockets via Ratchet to get information about crypto';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new WebSocket\Client("wss://streamer.cryptocompare.com/v2?api_key=443816dff55d45bda33579472998fe615fa35cf97fd599f5177a16baa239b0d1");
        $rec = false;

        $cryptos = Cache::get('crypto_symbols');

        foreach ($cryptos as $crypto_name => $crypto_info) {
            foreach ($crypto_info['currency'] as $currency => $data) {
                $client->send(json_encode(['action' => 'SubAdd', 'subs' => ["2~Binance~$crypto_name~$currency"]]));
            }
        }

        while (true) {
            try {
                if ($rec) {
                    foreach ($cryptos as $crypto_name => $crypto_info) {
                        foreach ($crypto_info['currency'] as $currency => $data) {
                            $client->send(json_encode(['action' => 'SubAdd', 'subs' => ["2~Binance~$crypto_name~$currency"]]));
                        }
                    }
                    $rec = false;
                }

                $price = Cache::get('crypto');
                $sym_fee = Cache::get('currency-fees');
                $message = json_decode($client->receive(), true);
                if (is_array($message)) {
                    if (array_key_exists('TYPE', $message) && ($message['TYPE'] == '2')) {
                        if (empty($sym_fee)) {
                            $fee = 0;
                        } else {
                            if (array_key_exists($message['FROMSYMBOL'] . $message['TOSYMBOL'], $sym_fee)) {
                                $fee = $sym_fee[$message['FROMSYMBOL'] . $message['TOSYMBOL']]['now'];
                            } else {
                                $fee = 0;
                            }
                        }
                        if (array_key_exists('PRICE', $message)) $price[$message['FROMSYMBOL']][$message['TOSYMBOL']]['p'] = $message['PRICE'] + $fee;
                        $p = $price[$message['FROMSYMBOL']][$message['TOSYMBOL']]['p'];
                        if (array_key_exists('HIGHDAY', $message)) $price[$message['FROMSYMBOL']][$message['TOSYMBOL']]['h'] = (($p + $fee) > $message['HIGHDAY']) ? ($p + $fee) : $message['HIGHDAY'];
                        if (array_key_exists('LOWDAY', $message)) $price[$message['FROMSYMBOL']][$message['TOSYMBOL']]['l'] = (($p + $fee) < $message['LOWDAY']) ? ($p + $fee) : $message['LOWDAY'];
                        if (array_key_exists('VOLUMEDAY', $message)) $price[$message['FROMSYMBOL']][$message['TOSYMBOL']]['v'] = round($message['VOLUMEDAY']);
                        if (array_key_exists('OPENDAY', $message)) $price[$message['FROMSYMBOL']][$message['TOSYMBOL']]['pc'] = $message['OPENDAY'];
                        if (array_key_exists('OPENHOUR', $message)) {
                            $price[$message['FROMSYMBOL']][$message['TOSYMBOL']]['o'] = $message['OPENHOUR'];
                            if ($price[$message['FROMSYMBOL']][$message['TOSYMBOL']]['oh'] != $message['OPENHOUR']) {
                                $price[$message['FROMSYMBOL']][$message['TOSYMBOL']]['poh'] = $price[$message['FROMSYMBOL']][$message['TOSYMBOL']]['oh'];
                                $price[$message['FROMSYMBOL']][$message['TOSYMBOL']]['oh'] = $message['OPENHOUR'];
                            }
                        }
                        Cache::put('crypto', $price);
                    }
                }
            } catch (\WebSocket\ConnectionException $e) {
                $rec = true;
            }
        }
        $client->close();

        return 0;
    }
}
