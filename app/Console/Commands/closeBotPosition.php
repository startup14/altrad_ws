<?php

namespace App\Console\Commands;

use App\BoughtTradingBot;
use Illuminate\Console\Command;

class closeBotPosition extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trading-bot-position:close';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (rand(1, 15) == rand(1, 15)) {
            $bought_trading_bots = BoughtTradingBot::where('active', '1')->get();
            $bots_ids = [];
            foreach ($bought_trading_bots as $bought) {
                $bots_ids[$bought->bot_id][] = $bought->user_id;
            }

            foreach ($bots_ids as $bot_id => $bot) {
                foreach ($bot as $user_id) {
                    $user = User::where('id', $user_id)->first();
                    $pos_count = 1;

                    for ($i = 1; $i <= $pos_count; $i++) {
                        $bought_bot = BoughtTradingBot::where('active', '1')->where('bot_id', $bot_id)->where('user_id', $user_id)->first();
                        $this->closePosition($user, $bought_bot);
                    }
                }
            }
        }
        return 0;
    }
}
