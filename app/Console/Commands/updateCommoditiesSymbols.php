<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class updateCommoditiesSymbols extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commodities:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $commodities = [
            'XAU' => [
                'name' => 'Gold',
                'graph' => 'XAUUSD',
                'short_name' => 'Gold',
                'units' => 'OZ'
            ],
            'XAG' => [
                'name' => 'Silver',
                'graph' => 'XAGUSD',
                'short_name' => 'Silver',
                'units' => 'OZ'
            ],
            'XPT' => [
                'name' => 'Platinum',
                'graph' =>'XPTUSD',
                'short_name' => 'Platinum',
                'units' => 'OZ'
            ],
            'XPD' => [
                'name' => 'Palladium',
                'graph' => 'XPDUSD',
                'short_name' => 'Palladium',
                'units' => 'OZ'
            ]
        ];

        Cache::put('commodities_symbols', $commodities);

        return 0;
    }
}
