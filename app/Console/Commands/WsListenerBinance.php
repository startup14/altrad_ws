<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\WsProviderCommand;
use App\Console\Ship\Transformers\DefaultTickerTransformer;
use App\QuoteProviders\Binance\WebSocket;

class WsListenerBinance extends WsProviderCommand
{
    protected $signature = 'ws-crypto-binance:listen';
    protected $description = 'Start listen Crypto updates WS (Binance)';

    public function __construct()
    {
        parent::__construct();
        $this->keyspace = REDIS_STREAM_CRYPTO;
        $this->wsProvider = WebSocket::class;
        $this->wsProviderTitle = 'Binance';
    }

    protected function redisKey(array $data): string
    {
        ['base' => $base, 'quote' => $quote] = $data;
        return redis_create_crypto_key("$base:$quote");
    }

    protected function redisWatchDogKey(array $data): string
    {
        $pair = implode(REDIS_KEY_SEPARATOR, [$data['base'], $data['quote']]);
        return redis_create_dogkey($this->keyspace, $pair);
    }

    protected function transformIncoming(array $data, array $parent): array
    {
        return (new DefaultTickerTransformer)->transform($data, $parent);
    }

    /**
     * @throws \JsonException
     */
    public function handle(): int
    {
        $crypto_keys = $this->waitingForShow();
        $this->showMustGoOn($crypto_keys);
        return 0;
    }
}
