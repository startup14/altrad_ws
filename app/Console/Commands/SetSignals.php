<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\RefreshCommand;
use App\Console\Traits\CommandUsesRedis;
use App\QuoteProviders\CryptoCompare\CryptoCompareAPI;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class SetSignals extends RefreshCommand
{
    use CommandUsesRedis;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'signals:set';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $crypto_keys = redis_keys_by_keyspace(REDIS_STREAM_CRYPTO);
        $now = Carbon::now()->timestamp;
        $provider = app(CryptoCompareAPI::class);
        $signals = [];

        foreach ($crypto_keys as $redis_key) {

            $fields = redis_hgetall($redis_key);
            if (! array_key_exists('name', $fields)) {
                $this->logError("Ticket must have <name> field [$redis_key]");
                continue;
            }

            ['base' => $base] = redis_key_to_base_and_quote($redis_key);
            $signals[$base] = [
                'name' => $fields['name'] ?? $base,
                'signals' => []
            ];

            for ($i = 5; $i > 0; $i--) {

                $ts = Carbon::now()->subHours($i)->timestamp;
                $nextTs = Carbon::now()->subHours($i + 1)->timestamp;

                $reference = $provider
                    ->ts($ts)
                    ->getPrice($base, 'USD');
                $close = $provider
                    ->ts($nextTs)
                    ->getPrice($base, 'USD');

                while (!isset($reference)){
                    sleep(1);
                    $reference = $provider
                        ->ts($ts)
                        ->getPrice($base, 'USD');
                }
                while (!isset($close)){
                    sleep(1);
                    $close = $provider
                        ->ts($nextTs)
                        ->getPrice($base, 'USD');
                }

                $signals[$base]['signals'][] = [
                    'move' => $this->randomMove(),
                    'correct' => $this->randomCorrect(),
                    'reference' => $reference,
                    'close' => $close,
                    'hours' => Carbon::now()->subHours($i)->hour . ':00 - ' . Carbon::now()->subHours($i - 1)->hour . ':00'
                ];
            }

            $reference = $provider
                ->ts($now)
                ->getPrice($base, 'USD');
            while (!isset($reference)){
                sleep(1);
                $reference = $provider
                    ->ts($now)
                    ->getPrice($base, 'USD');
            }

            $signals[$base]['signals'][] = [
                'move' => $this->randomMove(),
                'correct' => $this->randomCorrect(),
                'reference' => $reference,
                'close' => 1,
                'hours' => Carbon::now()->hour . ':00 - ' . Carbon::now()->addHour()->hour . ':00'
            ];
        }

        Cache::put(REDIS_STREAM_SIGNALS, $signals);

        return count($signals);
    }

    public function randomMove(): string
    {
        $strings = array(
            'Up',
            'Down'
        );
        $key = array_rand($strings);

        return $strings[$key];
    }

    public function randomCorrect()
    {
        $strings = array(
            true,
            false
        );
        $key = array_rand($strings);

        return $strings[$key];
    }
}
