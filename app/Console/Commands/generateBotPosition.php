<?php

namespace App\Console\Commands;

use App\BoughtTradingBot;
use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\SafeCRMRequests;
use App\Positions;
use App\User;
use Illuminate\Support\Facades\Cache;

class generateBotPosition extends BaseCommand
{
    use SafeCRMRequests;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trading-bot-position:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $start = rand(1, 20); $stop = rand(1, 20);

        if ($start === $stop) {
            $bought_trading_bots = BoughtTradingBot::where('active', '1')->get();
            $bots_ids = [];
            foreach ($bought_trading_bots as $bought) {
                $bots_ids[$bought->bot_id][] = $bought->user_id;
            }

            foreach ($bots_ids as $bot_id => $bot) {
                foreach ($bot as $user_id) {
                    $user = User::where('id', $user_id)->first();
                    $pos_count = rand(1, 3);

                    for ($i = 1; $i <= $pos_count; $i++) {
                        $bought_bot = BoughtTradingBot::where('active', '1')->where('bot_id', $bot_id)->where('user_id', $user_id)->first();
                        $this->generatePosition($user, $bought_bot);
                    }
                }
            }
        }

        return 0;
    }

    public function generatePosition($user, $bought)
    {
        $stocks = Cache::get('stocks');
        $crypto = Cache::get('crypto');
        $market = Cache::get('market')['open'];
        $type_of_market = 'Crypto';
        $currency = 'USDT';

        if ($market) {
            $types = [
                'Crypto',
                'Stocks'
            ];

            $key = array_rand($types);
            $type_of_market = $types[$key];
        }

        if ($type_of_market == 'Crypto') {
            $pair = array_keys($crypto)[array_rand(array_keys($crypto))];
            $keys = array_keys($crypto[$pair]);
            unset($keys[0]);
            $currency = array_keys($crypto[$pair])[array_rand($keys)];

            if ($currency != 'USDT') {
                $amount = rand(100, 300);
                $crypto_amount = $crypto[$pair][$currency]['p'] * $amount;
            } else {
                $amount = rand(100, 300);
                $crypto_amount = $amount;
            }

            if (($bought->current_amount + $amount) > $bought->max_amount) return 0;

            $av_types = [
                'buy',
                'sell'
            ];
            $type = $av_types[array_rand($av_types)];

            $liq = 0.5;
            $entry_price = $crypto[$pair][$currency]['p'];

            if ($user->subscription == 'dragon') {
                $available_lv = [
                    1,
                    2,
                    3,
                    5,
                    10
                ];
                $lv = $available_lv[array_rand($available_lv)];

                if ($lv == 1) $liq = 0.1;
                if ($lv == 2) $liq = 0.25;
                if ($lv == 3) $liq = 0.33;
                if ($lv == 5) $liq = 0.49;
                if ($lv == 10) $liq = 0.51;
            } else {
                $available_lv = [
                    1,
                    2,
                    3,
                    5
                ];
                $lv = $available_lv[array_rand($available_lv)];

                if ($lv == 1) $liq = 0.1;
                if ($lv == 2) $liq = 0.25;
                if ($lv == 3) $liq = 0.33;
                if ($lv == 5) $liq = 0.49;
            }
            if ($type == 'buy') {
                $liq = round($crypto[$pair][$currency]['p'] * $liq * 100) / 100;
                $entry_price = $entry_price + ($entry_price * $user->crypto_fee);
            } else {
                $liq = round(($crypto[$pair][$currency]['p'] + ($crypto[$pair][$currency]['p'] * (1 - $liq))) * 100) / 100;
                $entry_price = $entry_price - ($entry_price * $user->crypto_fee);
            }

            $value = $amount / $entry_price;

            $user->update([
                'balance' => $user->balance - $amount
            ]);
        } elseif ($type_of_market == 'Stocks') {
            $pair = array_keys($stocks)[array_rand(array_keys($stocks))];
            $value = rand(100, 200);

            $av_types = [
                'buy',
                'sell'
            ];
            $type = $av_types[array_rand($av_types)];

            $lv = 5;
            $liq = 0.5;
            $entry_price = $stocks[$pair]['p'];

            if ($user->subscription == 'dragon') {
                $available_lv = [
                    5,
                    10
                ];
                $lv = $available_lv[array_rand($available_lv)];

                if ($lv == 5) $liq = 0.49;
                if ($lv == 10) $liq = 0.51;
            } else {
                $liq = 0.49;
            }

            if ($type == 'buy') {
                $liq = round($stocks[$pair]['p'] * $liq * 100) / 100;
                $entry_price = $entry_price + ($entry_price * $user->stock_fee);
            } else {
                $liq = round(($stocks[$pair]['p'] + ($stocks[$pair]['p'] * (1 - $liq))) * 100) / 100;
                $entry_price = $entry_price - ($entry_price * $user->stock_fee);
            }

            $amount = round($value * $entry_price / $lv, 2);
            $crypto_amount = $amount;
            $user->update([
                'balance' => $user->balance - $amount
            ]);
        }

        Positions::create([
            'user_id' => $user->id,
            'take_profit' => null,
            'stop_loss' => null,
            'balance_before' => $user->balance,
            'balance_after' => $user->balance - $amount,
            'amount' => $amount,
            'class' => 'positions',
            'currency' => $currency,
            'entry_price' => $entry_price,
            'init_price' => $entry_price,
            'lv' => $lv,
            'leverage' => $lv * $amount,
            'liq' => $liq,
            'limit' => $entry_price,
            'pair' => $pair,
            'status' => 'new',
            'type' => $type,
            'type_of_market' => $type_of_market,
            'unit' => $pair,
            'value' => $value,
            'crypto_amount' => $crypto_amount
        ]);

        $bought->update([
            'current_amount' => $bought->current_amount + $amount
        ]);

        /*Http::put(env('CRM_URL') . 'api/update-balance', [
            'email' => $user->email,
            'balance' => $user->balance
        ]);*/
    }
}
