<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\SmartOutput;
use Illuminate\Support\Facades\Redis;

class ShowCacheLog extends BaseCommand
{
    use SmartOutput;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:view {stream} {--l|maxlen=101}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Extract and show log from redis (ws | api)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \JsonException
     */
    public function handle(): int
    {
        $stream = $this->argument('stream');
        $count = $this->option('maxlen');

        $redis_key = $stream . RONINS_WATCH_SUFFIX;
        $lines[] = sprintf("Viewing $redis_key on %s", env('REDIS_HOST'));

        $lines += array_values(Redis::xrevrange($redis_key, '+', '-', $count));

        $this->smartOutput($lines);

        return 0;
    }
}
