<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\InitCommand;
use App\Console\Traits\CommandAbleTransfer;
use Illuminate\Support\Facades\Redis;

class CryptoInit extends InitCommand
{
    use CommandAbleTransfer;

    protected string $keyspace = REDIS_STREAM_CRYPTO;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crypto:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load crypto coins template';


    private function convertCrypto(string $prev_fn, string $new_fn): int
    {
        $keys = [];

        $template = include storage_path($prev_fn);

        foreach ($template as $base => $quotes) {
            foreach ($quotes['quotes'] as $quote => $fields) {
                $key = implode(REDIS_KEY_SEPARATOR, [$base, $quote]);
                $fields['active'] = 1;
                $keys[$key] = $fields;
            }
        }

        return
            file_put_contents(
                storage_path($new_fn), "<?php \n return " . var_export($keys, true) . ';'
            ) === 0;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $this->unlinkKeys(REDIS_STREAM_CRYPTO);
        $processed = $this->loadTemplateFromStorageToRedis('symbols/crypto-v2.php', 'redis_create_crypto_key');
        $this->logProcessed($processed);
        return $processed;

//        return $this->convertCrypto('symbols/crypto.php', 'symbols/crypto-v2.php');
    }
}
