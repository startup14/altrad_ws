<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\WsProviderCommand;
use App\Console\Ship\Transformers\TwelveDataTransformer;
use App\QuoteProviders\TwelveData\WebSocket as TwelveWebSocket;

class WsListenerTwelve extends WsProviderCommand
{

    protected $signature = 'ws-stocks-twelve:listen';
    protected $description = 'Listen Stocks updates WS (Twelve Data)';

    public function __construct()
    {
        parent::__construct();
        $this->keyspace = REDIS_STREAM_STOCKS;
        $this->wsProvider = TwelveWebSocket::class;
        $this->wsProviderTitle = 'Twelve';
    }

    /**
     * @throws \JsonException
     */
    public function handle(): int
    {
        $stocks_keys = $this->waitingForShow();

        new TwelveWebSocket($stocks_keys, function (array $answer) {
            if ($answer['event'] !== 'price') {
                $this->infoTime("$this->wsProviderTitle sent '{$answer['event']}'");
                return;
            }
            $this->updateCache($answer);
        });

        return 0;
    }

    protected function redisKey(array $data): string
    {
        return redis_create_stocks_key($data['symbol']);
    }

    protected function redisWatchDogKey(array $data): string
    {
        return redis_create_dogkey($this->keyspace, $data['symbol']);
    }

    protected function transformIncoming(array $data, array $parent): array
    {
        return (new TwelveDataTransformer())->transform($data, $parent);
    }
}
