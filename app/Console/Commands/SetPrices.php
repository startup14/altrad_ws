<?php

namespace App\Console\Commands;

use App\CurrencyList;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Output\ConsoleOutput;

use App\Contracts\PreferencesInterface as Preferences;

class SetPrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prices:set';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets quotes for crypto, stocks and commodities from providers and stores them in Cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public $stocks = [];

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cryptoTemplate = Cache::get('crypto_symbols');
        $stocksTemplate = Cache::get('stock_symbols');
        $commoditiesTemplate = Cache::get('commodities_symbols');

        $crypto = $this->loadCrypto($cryptoTemplate);
        $stocks = $this->loadStocks($stocksTemplate);
        $commodities = $this->loadCommodities($commoditiesTemplate);

        Cache::put('crypto', $crypto);
        Cache::put('stocks', $stocks);
        Cache::put('commodities', $commodities);

        $this->info("Command prices:set completed");

        return 0;
    }

    function loadCrypto(array $template): array
    {
        $crypto = [];
        foreach ($template as $quote => $symbol) {
            foreach ($symbol['currency'] as $base => $data) {

                $crypto[$quote]['name'] = $symbol['name'];

                try {
                    $response = Http::get("https://api.binance.com/api/v3/klines?symbol={$quote}{$base}&interval=1d&limit=1");
                    $ticker = $response->json()[0];
                } catch (\Exception $exception) {
                    $this->error($exception->getMessage());
                }
                $crypto[$quote][$base] = [
                    'pp' => $this->clean_num($ticker[4]),
                    'p' => $this->clean_num($ticker[4]),
                    'h' => $this->clean_num($ticker[2]),
                    'l' => $this->clean_num($ticker[3]),
                    'v' => $this->clean_num($ticker[5]),
                    'pc' => $this->clean_num($ticker[1]),
                    'o' => $this->clean_num($ticker[1]),
                    'active' => '1'
                ];
            }
        }

        $this->line(count($crypto) . " Crypto symbols loaded.");

        return $crypto;
    }

    function loadCommodities(array $template): array
    {
        // $apiKey = env('POLYGON_API_KEY');
        $apiKey = 'Lypjj36DCON3rZWUGQYpVgjS5pacz98d';
        $commodities = [];
        foreach ($template as $key => $symbol) {
            try {
                $response = Http::get("https://api.polygon.io/v2/snapshot/locale/global/markets/forex/tickers/C:{$symbol['graph']}?&apiKey={$apiKey}")->json();
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
            }

            if (isset($response['status']) && $response['status'] == "OK") {
                $ticker = $response['ticker']['day'];

                $commodities[$key] = [
                    'p' => $ticker['c'],
                    'pp' => $ticker['c'],
                    'h' => $ticker['h'],
                    'l' => $ticker['l'],
                    'pc' => $ticker['o'],
                    'v' => $ticker['v'],
                    'name' => $symbol['name'],
                    'name_short' => $symbol['short_name'],
                    'units' => $symbol['units']
                ];
            }
        }

        $this->line(count($commodities) . " Commodities symbols loaded.");

        return $commodities;
    }

    function loadStocks(array $template): array
    {
        // $apiKey = env('TWELVE_API_KEY');

        // TODO change when on PROD
        // $apiKey = '88e43b98effb4571b46fec0616b283af';

        // TODO change when on PREPROD
        // $apiKey = '851a92e8b3c04ec8bd93632f5c2c0bf8';

        // TODO change when on DEVELOP
        $apiKey = '5b537a66403c4c4a990e37f63804e1b0';

        $chunks = array_chunk($template, 20, true);
        $stocks = [];
        foreach ($chunks as $chunk) {
            $query = implode(',', array_keys($chunk));
            try {
                $response = Http::get("https://api.twelvedata.com/quote?symbol={$query}&apikey={$apiKey}");
                $tickers = $response->json();
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
            }

            $symbols = [];
            foreach ($tickers ?? [] as $ticker) {
                if (isset($ticker['close'])) {
                    $symbols[$ticker['symbol']] = [
                        'p' => round($ticker['close'], 2),
                        'pp' => round($ticker['close'], 2),
                        'h' => round($ticker['high'] ?? $ticker['close'], 2),
                        'l' => round($ticker['low'] ?? $ticker['close'], 2),
                        'pc' => round($ticker['previous_close'] ?? $ticker['open'], 2),
                        'v' => $ticker['volume'],
                        'name' => $ticker['name'],
                        'country' => $template[$ticker['symbol']]['country']
                    ];
                }
            }

            $stocks = array_merge($stocks, $symbols);
        }

        $this->line(count($stocks) . " Stock symbols loaded.");

        return $stocks;
    }

    function clean_num($num)
    {
        return strpos($num, '.')
            ? (DOUBLE) rtrim(rtrim($num, '0'), '.')
            : (DOUBLE) $num;
    }
}
