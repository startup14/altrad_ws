<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class updateCryptoSymbols extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crypto:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cryptos = [
            'BTC' => [
                'name' => 'Bitcoin',
                'currency' => [
                    'USDT' => [
                        'minmov' => '1',
                        'pricescale' => '100'
                    ],
                ]
            ],
            'BCH' => [
                'name' => 'Bitcoin Cash',
                'currency' => [
                    'USDT' => [
                        'minmov' => '1',
                        'pricescale' => '100'
                    ],
                    'BTC' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                ]
            ],
            'BNB' => [
                'name' => 'Binance Coin',
                'currency' => [
                    'USDT' => [
                        'minmov' => '1',
                        'pricescale' => '100'
                    ],
                ]
            ],
            'ETH' => [
                'name' => 'Ethereum',
                'currency' => [
                    'USDT' => [
                        'minmov' => '1',
                        'pricescale' => '100'
                    ],
                    'BTC' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                ]
            ],
            'LTC' => [
                'name' => 'Litecoin',
                'currency' => [
                    'USDT' => [
                        'minmov' => '1',
                        'pricescale' => '100'
                    ],
                    'BTC' => [
                        'minmov' => '0.001',
                        'pricescale' => '100000'
                    ],
                    'ETH' => [
                        'minmov' => '0.001',
                        'pricescale' => '100000'
                    ],
                ]
            ],
            'XRP' => [
                'name' => 'Ripple',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.001',
                        'pricescale' => '100000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                ]
            ],
            'KNC' => [
                'name' => 'Kyber Network',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.1',
                        'pricescale' => '1000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.00001',
                        'pricescale' => '10000000'
                    ],
                ]
            ],
            'REP' => [
                'name' => 'Augur',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.1',
                        'pricescale' => '1000'
                    ],
                    'BTC' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.001',
                        'pricescale' => '100000'
                    ],
                ]
            ],
            'XTZ' => [
                'name' => 'Tezos',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.01',
                        'pricescale' => '10000'
                    ],
                    'BTC' => [
                        'minmov' => '0.00001',
                        'pricescale' => '10000000'
                    ],
                ]
            ],
            'XLM' => [
                'name' => 'Stellar',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.001',
                        'pricescale' => '100000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                ]
            ],
            'ZRX' => [
                'name' => '0rx',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.01',
                        'pricescale' => '10000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.00001',
                        'pricescale' => '10000000'
                    ],
                ]
            ],
            'OMG' => [
                'name' => 'OMG Network',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.01',
                        'pricescale' => '10000'
                    ],
                    'BTC' => [
                        'minmov' => '0.00001',
                        'pricescale' => '10000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.001',
                        'pricescale' => '100000'
                    ],
                ]
            ],
            'LINK' => [
                'name' => 'Chainlink',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.01',
                        'pricescale' => '10000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                ]
            ],
            'MKR' => [
                'name' => 'Maker',
                'currency' => [
                    'USDT' => [
                        'minmov' => '1',
                        'pricescale' => '100'
                    ],
                    'BTC' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                ]
            ],
            'EOS' => [
                'name' => 'EOS',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.01',
                        'pricescale' => '10000'
                    ],
                    'BTC' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                ]
            ],
            'AION' => [
                'name' => 'AION',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.01',
                        'pricescale' => '10000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                ]
            ],
            'ADA' => [
                'name' => 'Cardano',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.001',
                        'pricescale' => '100000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.00001',
                        'pricescale' => '10000000'
                    ],
                ]
            ],
            'TRX' => [
                'name' => 'TRON',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.001',
                        'pricescale' => '100000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                ]
            ],
            'XMR' => [
                'name' => 'Monero',
                'currency' => [
                    'USDT' => [
                        'minmov' => '1',
                        'pricescale' => '100'
                    ],
                    'BTC' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.001',
                        'pricescale' => '100000'
                    ],
                ]
            ],
            'NEO' => [
                'name' => 'NEO',
                'currency' => [
                    'USDT' => [
                        'minmov' => '1',
                        'pricescale' => '100'
                    ],
                    'BTC' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                ]
            ],
            'ATOM' => [
                'name' => 'Cosmos',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.01',
                        'pricescale' => '1000'
                    ],
                    'BTC' => [
                        'minmov' => '0.00001',
                        'pricescale' => '10000000'
                    ],
                ]
            ],
            'ANT' => [
                'name' => 'Aragon',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.01',
                        'pricescale' => '10000'
                    ],
                    'BTC' => [
                        'minmov' => '0.00001',
                        'pricescale' => '10000000'
                    ],
                ]
            ],
            'ARDR' => [
                'name' => 'Ardor',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.01',
                        'pricescale' => '10000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                ]
            ],
            'BEAM' => [
                'name' => 'Beam',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.01',
                        'pricescale' => '10000'
                    ],
                    'BTC' => [
                        'minmov' => '0.00001',
                        'pricescale' => '10000000'
                    ],
                ]
            ],
            'BLZ' => [
                'name' => 'Bluzelle',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.001',
                        'pricescale' => '100000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                ]
            ],
            'BNT' => [
                'name' => 'Bancor',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.01',
                        'pricescale' => '10000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                ]
            ],
            'BTS' => [
                'name' => 'BitShares',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.001',
                        'pricescale' => '100000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                ]
            ],
            'BTT' => [
                'name' => 'BitTorrent Token',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.00001',
                        'pricescale' => '10000000'
                    ],
                ]
            ],
            'CVC' => [
                'name' => 'Civic',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.001',
                        'pricescale' => '100000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                ]
            ],
            'DATA' => [
                'name' => 'Streamr DATAcoin',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.001',
                        'pricescale' => '100000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                ]
            ],
            'DCR' => [
                'name' => 'Decred',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.1',
                        'pricescale' => '1000'
                    ],
                    'BTC' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                ]
            ],
            'DENT' => [
                'name' => 'Dent',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.00001',
                        'pricescale' => '10000000'
                    ],
                ]
            ],
            'DGB' => [
                'name' => 'DigiByte',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.001',
                        'pricescale' => '100000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                ]
            ],
            'DOCK' => [
                'name' => 'Dock',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                ]
            ],
            'DOGE' => [
                'name' => 'Dogecoin',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.00001',
                        'pricescale' => '10000000'
                    ],
                ]
            ],
            'ENJ' => [
                'name' => 'Enjin Coin',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.001',
                        'pricescale' => '100000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                ]
            ],
            'FUN' => [
                'name' => 'FunFair',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                ]
            ],
            'GTO' => [
                'name' => 'Gifto',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.001',
                        'pricescale' => '100000'
                    ],
                ]
            ],
            'GXS' => [
                'name' => 'GXChain',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.01',
                        'pricescale' => '10000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                ]
            ],
            'ICX' => [
                'name' => 'Icon',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.01',
                        'pricescale' => '10000'
                    ],
                    'BTC' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                ]
            ],
            'IOST' => [
                'name' => 'IOST',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                ]
            ],
            'IOTX' => [
                'name' => 'IoTeX',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.000001',
                        'pricescale' => '100000000'
                    ],
                ]
            ],
            'ETC' => [
                'name' => 'Ethereum Classic',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.01',
                        'pricescale' => '10000'
                    ],
                    'BTC' => [
                        'minmov' => '0.00001',
                        'pricescale' => '10000000'
                    ],
                    'ETH' => [
                        'minmov' => '0.0001',
                        'pricescale' => '1000000'
                    ],
                ]
            ],
            'BTG' => [
                'name' => 'Bitcoin Gold',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.01',
                        'pricescale' => '10000'
                    ],
                    'BTC' => [
                        'minmov' => '0.00001',
                        'pricescale' => '10000000'
                    ]
                ]
            ],
            'SHIB' => [
                'name' => 'SHIBA INU',
                'currency' => [
                    'USDT' => [
                        'minmov' => '0.00001',
                        'pricescale' => '10000000'
                    ],
                ]
            ]
        ];

        Cache::put('crypto_symbols', $cryptos);

        return 0;
    }
}
