<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\SafeCRMRequests;
use App\Positions;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class generateCopyTraderPosition extends BaseCommand
{
    use SafeCRMRequests;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'copy-trader-position:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle(): ?int
    {
        if (Carbon::now()->minute === 33) {
            $bots = User::where('role', 'bot')->get();
            if ($bots->count() === 0) {
                return -1;
            }

            $bots_ids = [];
            foreach ($bots as $bot) {
                $bots_ids[] = $bot->id;
            }

            for ($i = 0; $i < 5; $i++) {
                $key = array_rand($bots_ids);
                $bot_id_to_generate = $bots_ids[$key];
                $bot = User::where('id', $bot_id_to_generate)->first();

                $this->generatePosition($bot);
            }
        }

        return 0;
    }

    /**
     * @throws \Exception
     */
    public function generatePosition($user): void
    {
        $crypto_keys = redis_keys_by_keyspace('crypto');

        $currency = 'USDT';
        $base = redis_key_to_base_and_quote(array_rand($crypto_keys))['base'];
        $quote = 'USDT';

        $amount = random_int(100, 500);

        $av_types = ['buy', 'sell'];
        $type = $av_types[array_rand($av_types)];

        $redis_key = redis_create_key_for_keyspace('crypto', $base, $quote);
        $entry_price = redis_hgetall($redis_key)['p'];

        if ($user->subscription === 'dragon') {
            $available_lv = [
                1 => 0.1,
                2 => 0.25,
                3 => 0.33,
                5 => 0.49,
                10 => 0.51
            ];
            $lv = array_rand(array_keys($available_lv));
            $liq = $available_lv[$lv];

            if ($user->balance < 100000){
                $user->update([
                    'balance' => $user->balance + 100000
                ]);
            }
            $amount = random_int(10000, 100000);
        } else {
            if ($user->subscription === 'gold'){
                if ($user->balance < 1000){
                    $user->update([
                        'balance' => $user->balance + 500
                    ]);
                }
                $amount = random_int(100, 1000);
            } elseif($user->subscription === 'platinum') {
                if ($user->balance < 5000){
                    $user->update([
                        'balance' => $user->balance + 5000
                    ]);
                }
                $amount = random_int(500, 5000);
            } elseif ($user->subscription === 'vip'){
                if ($user->balance < 10000){
                    $user->update([
                        'balance' => $user->balance + 10000
                    ]);
                }
                $amount = random_int(1000, 10000);
            } elseif ($user->subscription === 'vip+'){
                if ($user->balance < 50000){
                    $user->update([
                        'balance' => $user->balance + 50000
                    ]);
                }
                $amount = random_int(5000, 50000);
            } elseif ($user->subscription === 'silver'){
                if ($user->balance < 500){
                    $user->update([
                        'balance' => $user->balance + 500
                    ]);
                }
                $amount = random_int(100, 500);
            }
            $available_lv = [
                1 => 0.1,
                2 => 0.25,
                3 => 0.33,
                5 => 0.49,
            ];
            $lv = array_rand(array_keys($available_lv));
            $liq = $available_lv[$lv];
        }

        $crypto_amount = $amount;

        if ($type === 'buy') {
            $liq = round($entry_price * $liq * 100) / 100;
            $entry_price += ($entry_price * $user->crypto_fee);
        } else {
            $liq = round(($entry_price + ($entry_price * (1 - $liq))) * 100) / 100;
            $entry_price -= ($entry_price * $user->crypto_fee);
        }

        $value = $amount / $entry_price;

        $user->update([
            'balance' => $user->balance - $amount
        ]);

        $position = Positions::create([
            'user_id' => $user->id,
            'take_profit' => null,
            'stop_loss' => null,
            'balance_before' => $user->balance + $amount,
            'balance_after' => $user->balance,
            'amount' => $amount,
            'class' => 'positions',
            'currency' => $currency,
            'entry_price' => $entry_price,
            'init_price' => $entry_price,
            'lv' => $lv,
            'leverage' => $lv * $amount,
            'liq' => $liq,
            'limit' => $entry_price,
            'pair' => $base,
            'status' => 'new',
            'type' => $type,
            'type_of_market' => 'Crypto',
            'unit' => $base,
            'value' => $value,
            'crypto_amount' => $crypto_amount
        ]);

        $this->crmHttpPutJson('api/update-balance', [
            'email' => $user->email,
            'balance' => $user->balance
        ]);

        $this->copyPositionToFollowers($user, $position);
    }

    public function copyPositionToFollowers($user, $position): void
    {
        $followers = User::where('follow', $user->id)->get();

        foreach ($followers as $follower) {
            if ($follower->balance < $position->amount) {
                continue;
            }

            $copy_position = $position->replicate();
            $copy_position->user_id = $follower->id;
            $copy_position->balance_before = $follower->balance;
            $copy_position->balance_after = $follower->balance - $position->amount;
            $copy_position->save();

            $follower->balance -= $position->amount;
            $follower->save();

            $this->crmHttpPutJson('api/update-balance', [
                'email' => $follower->email,
                'balance' => $follower->balance
            ]);
        }
    }
}
