<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\SafeCRMRequests;
use App\Inform;
use App\OrdersHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class CheckAbnormalProfit extends BaseCommand
{
    use SafeCRMRequests;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'abnormal-profit:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $user_ids = OrdersHistory::select('user_id')
            ->where('created_at', '>', Carbon::now()->subHours(24))
            ->groupBy('user_id')
            ->get();
        $users = [];

        foreach ($user_ids as $id) {
            $users[] = $id->user_id;
        }
        $users = User::select('id', 'balance', 'email', 'first_name', 'last_name')
            ->whereIn('id', $users)
            ->where('role', 'trader')
            ->get();

        foreach ($users as $user) {
            $history = OrdersHistory::where('user_id', $user->id)
                ->where('created_at', '>', Carbon::today())
                ->get();

            $profit = $this->countProfit($history);
            $midDep = $this->crmHttpGetJson('api/clients/midcheck-profit', [
                'email' => $user->email
            ], 'midcheck');

            $allowedProfit = $midDep * 2;

            if ($profit > $allowedProfit){
                $inform = Inform::where('user_id', $user->id)
                    ->where('type', 'abnormal-profit')
                    ->latest()
                    ->first();

                if ($inform) {
                    if ($inform->created_at < Carbon::now()->subDay()) {
                        $this->inform($user, $profit);
                    }
                } else {
                    $this->inform($user, $profit);
                }
            }
        }

        return 0;
    }

    public function inform($user, $profit): void
    {
        $response = $this->crmHttpGetJson('api/clients/assigned/telegram', [
            'email' => $user->email
        ]);

        if ($response && $response['success']) {
            $telegram = $response['username'];
            Http::get(env('BOT_URL'), [
                'u_name' => $telegram,
                'fname' => $user->first_name,
                'lname' => $user->last_name,
                'client_id' => $user->id,
                'type' => 'profit',
                'profit' => $profit
            ]);

            $this->crmHttpPostJson('api/notification/abnormal-profit', [
                'email' => $user->email
            ]);

            Inform::create([
                'user_id' => $user->id,
                'type' => 'abnormal-profit'
            ]);
        }
    }

    public function countProfit($history): float
    {
        $sum = 0;

        foreach ($history as $item) {
            $sum += round($item->front_balance - $item->amount, 8);
        }

        return round($sum, 2);
    }
}
