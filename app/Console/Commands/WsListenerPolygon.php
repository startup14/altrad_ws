<?php

namespace App\Console\Commands;


use App\Console\Ship\Parents\WsProviderCommand;
use App\Console\Ship\Transformers\DefaultTickerTransformer;
use App\Contracts\CommoditiesProviderInterface;
use App\QuoteProviders\Polygon\WebSocket as PolygonWebSocket;

final class WsListenerPolygon extends WsProviderCommand
{

    protected $signature = 'ws-commo-polygon:listen';
    protected $description = 'Listen Commo updates WS (Polygon)';

    private CommoditiesProviderInterface $apiProvider;

    public function __construct()
    {
        parent::__construct();
        $this->apiProvider = app(CommoditiesProviderInterface::class);
        $this->keyspace = REDIS_STREAM_COMMO;
        $this->wsProvider = PolygonWebSocket::class;
        $this->wsProviderTitle = 'Polygon';
    }

    /**
     * @throws \JsonException
     */
    public function handle(): int
    {
        $commo_keys = $this->waitingForShow();

        new PolygonWebSocket($commo_keys, function (array $answer) {
            if ($answer['event'] !== 'CA') {
                $this->infoTime("$this->wsProviderTitle sent '{$answer['event']}'");
                return;
            }
            $answer['vol'] = $this->getVolumeViaAPI($answer['symbol']);
            $this->updateCache($answer);
        });

        return 0;
    }

    private function getVolumeViaAPI(string $symbol): ?int
    {
        $ticker = $this->apiProvider->getTickers([$symbol]);
        if ($key =  array_key_first($ticker)) {
            return $ticker[$key]['vol'];
        }
        return null;
    }

    protected function redisKey(array $data): string
    {
        return redis_create_commodities_key($data['symbol']);
    }

    protected function redisWatchDogKey(array $data): string
    {
        return redis_create_dogkey($this->keyspace, $data['symbol']);
    }

    protected function transformIncoming(array $data, array $parent): array
    {
        return (new DefaultTickerTransformer)->transform($data, $parent);
    }
}
