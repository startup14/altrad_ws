<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\User;
use App\Positions;
use App\OrdersHistory;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

use App\Contracts\PreferencesInterface as Preferences;
use Illuminate\Support\Facades\Log;

class PositionCheck extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'position:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check take profit or stop loss of positions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $preferences = app(Preferences::class);
        $disablestoplimitChecking = $preferences->disable_stoplimit_checking;
        if ((INT)$disablestoplimitChecking) {
            return 0;
        }

        $positions = Positions::whereNotNull('take_profit')->orWhereNotNull('stop_loss')->get();

        foreach ($positions as $position) {
            $base = $position->pair;
            $quote = $position->currency;

            if ($position->type_of_market === 'Crypto') {

                $redis_data = redis_hgetall(redis_create_crypto_key("$base:$quote"));
                $convert_data = redis_hgetall(redis_create_crypto_key("$quote:USDT"));

                if ($position->type === 'buy') {
                    if (!is_null($position->take_profit)) {
                        if ($redis_data['p'] >= $position->take_profit) {
                            $info = $this->countBalanceForPositionTakeProfit($position, $redis_data, $convert_data);
                            $data = [
                                'id' => $position->id,
                                'status' => 'take profit',
                                'balance' => $info['balance'],
                                'close' => $info['close'],
                                'activity_token' => 'tpsl'
                            ];
                            if ($position->currency !== 'USDT') {
                                $data['conversion'] = $convert_data['p'];
                            }
                            $this->deletePosition(new Request($data));
                            usleep(100000);
                        }
                    }
                    if (!is_null($position->stop_loss)) {
                        if ($redis_data['p'] <= $position->stop_loss) {
                            $info = $this->countBalanceForPositionStopLoss($position, $redis_data, $convert_data);
                            $data = [
                                'id' => $position->id,
                                'status' => 'stop loss',
                                'balance' => $info['balance'],
                                'close' => $info['close'],
                                'activity_token' => 'tpsl'
                            ];
                            if ($position->currency !== 'USDT') {
                                $data['conversion'] = $convert_data['p'];
                            }
                            $this->deletePosition(new Request($data));
                            usleep(100000);
                        }
                    }
                } elseif ($position->type === 'sell') {
                    if (!is_null($position->take_profit)) {
                        if ($redis_data['p'] <= $position->take_profit) {
                            $info = $this->countBalanceForPositionTakeProfit($position, $redis_data, $convert_data);
                            $data = [
                                'id' => $position->id,
                                'status' => 'take profit',
                                'balance' => $info['balance'],
                                'close' => $info['close'],
                                'activity_token' => 'tpsl'
                            ];
                            if ($position->currency !== 'USDT') {
                                $data['conversion'] = $convert_data['p'];
                            }
                            $this->deletePosition(new Request($data));
                            usleep(100000);
                        }
                    }
                    if (!is_null($position->stop_loss)) {
                        if ($redis_data['p'] >= $position->stop_loss) {
                            $info = $this->countBalanceForPositionStopLoss($position, $redis_data, $convert_data);
                            $data = [
                                'id' => $position->id,
                                'status' => 'stop loss',
                                'balance' => $info['balance'],
                                'close' => $info['close'],
                                'activity_token' => 'tpsl'
                            ];
                            if ($position->currency !== 'USDT') {
                                $data['conversion'] = $convert_data['p'];
                            }
                            $this->deletePosition(new Request($data));
                            usleep(100000);
                        }
                    }
                }
            } elseif ($position->type_of_market === 'Stock') {
                $redis_data = redis_hgetall(redis_create_stocks_key($base));
                if ($position->type === 'buy') {
                    if (!is_null($position->take_profit)) {
                        if ($redis_data['p'] >= $position->take_profit) {
                            $info = $this->countBalanceForPositionTakeProfit($position, $redis_data, []);
                            $data = [
                                'id' => $position->id,
                                'status' => 'take profit',
                                'balance' => $info['balance'],
                                'close' => $info['close'],
                                'conversion' => '1',
                                'activity_token' => 'tpsl'
                            ];
                            $this->deletePosition(new Request($data));
                            usleep(100000);
                        }
                    }
                    if (!is_null($position->stop_loss)) {
                        if ($redis_data['p'] <= $position->stop_loss) {
                            $info = $this->countBalanceForPositionStopLoss($position, $redis_data, []);
                            $data = [
                                'id' => $position->id,
                                'status' => 'stop loss',
                                'balance' => $info['balance'],
                                'close' => $info['close'],
                                'conversion' => '1',
                                'activity_token' => 'tpsl'
                            ];
                            $this->deletePosition(new Request($data));
                            usleep(100000);
                        }
                    }
                } elseif ($position->type === 'sell') {
                    if (!is_null($position->take_profit)) {
                        if ($redis_data['p'] <= $position->take_profit) {
                            $info = $this->countBalanceForPositionTakeProfit($position, $redis_data, []);
                            $data = [
                                'id' => $position->id,
                                'status' => 'take profit',
                                'balance' => $info['balance'],
                                'close' => $info['close'],
                                'conversion' => '1',
                                'activity_token' => 'tpsl'
                            ];
                            $this->deletePosition(new Request($data));
                            usleep(100000);
                        }
                    }
                    if (!is_null($position->stop_loss)) {
                        if ($redis_data['p'] >= $position->stop_loss) {
                            $info = $this->countBalanceForPositionStopLoss($position, $redis_data, []);
                            $data = [
                                'id' => $position->id,
                                'status' => 'stop loss',
                                'balance' => $info['balance'],
                                'close' => $info['close'],
                                'conversion' => '1',
                                'activity_token' => 'tpsl'
                            ];
                            $this->deletePosition(new Request($data));
                            usleep(100000);
                        }
                    }
                }
            } elseif ($position->type_of_market === 'Commodities') {
                $redis_data = redis_hgetall(redis_create_commodities_key($base));
                if ($position->type === 'buy') {
                    if (!is_null($position->take_profit)) {
                        if ($redis_data['p'] >= $position->take_profit) {
                            $info = $this->countBalanceForPositionTakeProfit($position, $redis_data, []);
                            $data = [
                                'id' => $position->id,
                                'status' => 'take profit',
                                'balance' => $info['balance'],
                                'close' => $info['close'],
                                'conversion' => '1',
                                'activity_token' => 'tpsl'
                            ];
                            $this->deletePosition(new Request($data));
                            usleep(100000);
                        }
                    }
                    if (!is_null($position->stop_loss)) {
                        if ($redis_data['p'] <= $position->stop_loss) {
                            $info = $this->countBalanceForPositionStopLoss($position, $redis_data, []);
                            $data = [
                                'id' => $position->id,
                                'status' => 'stop loss',
                                'balance' => $info['balance'],
                                'close' => $info['close'],
                                'conversion' => '1',
                                'activity_token' => 'tpsl'
                            ];
                            $this->deletePosition(new Request($data));
                            usleep(100000);
                        }
                    }
                } elseif ($position->type === 'sell') {
                    if (!is_null($position->take_profit)) {
                        if ($redis_data['p'] <= $position->take_profit) {
                            $info = $this->countBalanceForPositionTakeProfit($position, $redis_data, []);
                            $data = [
                                'id' => $position->id,
                                'status' => 'take profit',
                                'balance' => $info['balance'],
                                'close' => $info['close'],
                                'conversion' => '1',
                                'activity_token' => 'tpsl'
                            ];
                            $this->deletePosition(new Request($data));
                            usleep(100000);
                        }
                    }
                    if (!is_null($position->stop_loss)) {
                        if ($redis_data['p'] >= $position->stop_loss) {
                            $info = $this->countBalanceForPositionStopLoss($position, $redis_data, []);
                            $data = [
                                'id' => $position->id,
                                'status' => 'stop loss',
                                'balance' => $info['balance'],
                                'close' => $info['close'],
                                'conversion' => '1',
                                'activity_token' => 'tpsl'
                            ];
                            $this->deletePosition(new Request($data));
                            usleep(100000);
                        }
                    }
                }
            }
        }

        return 0;
    }

    public function countBalanceForPositionTakeProfit($position, $currency_data, $convert_data): array
    {
        $user = User::where('id', $position->user_id)->select('id', 'positions_fee')->first();

        $rate = $currency_data['p'];

        $random = $rate * $user->positions_fee;
        if ($position->type == 'buy') {
            $close = $rate - $random;

            if ($position->type_of_market != 'Crypto') {
                $balance = $position->amount + ($close - $position->limit) * ($position->value) - ($position->amount * $position->swop);
            } elseif ($position->type_of_market == 'Crypto') {
                if ($position->currency != 'USDT') {
                    try {
                        $balance = $position->amount + ($close - $position->limit) * $convert_data['p'] * ($position->value);
                    } catch (Exception $e) {
                        Log::error($e->getMessage());
                    }
                } else {
                    $balance = $position->amount + ($close - $position->limit) * ($position->value);
                }
            }
            $balance = round($balance - ($position->amount * $position->swop), 2);
        } elseif ($position->type == 'sell') {
            $close = $rate + $random;

            if ($position->type_of_market != 'Crypto') {
                $balance = $position->amount + ($position->limit - $close) * ($position->value);
            } elseif ($position->type_of_market == 'Crypto') {
                if ($position->currency != 'USDT') {
                    try {
                        $balance = $position->amount + ($position->limit - $close) * $convert_data['p'] * ($position->value) - ($position->amount * $position->swop);
                    } catch (Exception $e) {
                        Log::error($e->getMessage());
                    }
                } else {
                    $balance = $position->amount + ($position->limit - $close) * ($position->value);
                }
            }
            $balance = round($balance - ($position->amount * $position->swop), 2);
        }

        return [
            'close' => $close,
            'balance' => $balance
        ];
    }

    public function countBalanceForPositionStopLoss($position, $currency_data, $convert_data)
    {
        $user = User::where('id', $position->user_id)->select('id', 'positions_fee')->first();

        $rate = $currency_data['p'];

        $random = $rate * $user->positions_fee;

        if ($position->type == 'buy') {
            $close = $rate - $random;

            if ($position->type_of_market != 'Crypto') {
                $balance = $position->amount + ($close - $position->limit) * ($position->value);
            } elseif ($position->type_of_market == 'Crypto') {
                if ($position->currency != 'USDT') {
                    try {
                        $balance = $position->amount + ($close - $position->limit) * $convert_data['p'] * ($position->value);
                    } catch (Exception $e) {
                        Log::error($e->getMessage());
                    }
                } else {
                    $balance = $position->amount + ($close - $position->limit) * ($position->value);
                }
            }
            $balance = round($balance - ($position->amount * $position->swop), 2);
        } elseif ($position->type == 'sell') {
            $close = $rate + $random;

            if ($position->type_of_market != 'Crypto') {
                $balance = $position->amount + ($position->limit - $close) * ($position->value);
            } elseif ($position->type_of_market == 'Crypto') {
                if ($position->currency != 'USDT') {
                    try {
                        $balance = $position->amount + ($position->limit - $close) * $convert_data['p'] * ($position->value);
                    } catch (Exception $e) {
                        Log::error($e->getMessage());
                    }
                } else {
                    $balance = $position->amount + ($position->limit - $close) * ($position->value);
                }
            }
            $balance = round($balance - ($position->amount * $position->swop), 2);
        }

        return [
            'close' => $close,
            'balance' => $balance
        ];
    }

    public function deletePosition(Request $request)
    {
        $position = Positions::where('id', $request->id)->first();
        if (!$position) return;

        $user = User::where('id', $position->user_id)->first();

        if (!$user->is_active) return response()->json([
            'success' => false
        ]);

        if (!in_array($user->account_status, ['pending', 'can_deposit'])) {
            if ($user->account_status == 'can_trade_crypto') {
                if ($position->type_of_market != 'Crypto') return response()->json([
                    'message' => 'Failed',
                    'status' => 'false',
                    'balance' => $user->balance
                ]);
            }
            if ($user->account_status == 'can_trade_stock') {
                if ($position->type_of_market != 'Stock') return response()->json([
                    'message' => 'Failed',
                    'status' => 'false',
                    'balance' => $user->balance
                ]);
            }

            if (in_array($request->status, ['take profit', 'stop loss'])) {
                if ($position->type_of_market == 'Crypto') {
                    if ($position->type == 'buy') {
                        $close = $request->status == 'take profit' ? $position->take_profit : $position->stop_loss;
                        if ($position->currency == 'USDT') {
                            $balance = $position->amount + ($close - $position->limit) * $position->value - ($position->amount * $position->swop);
                        } else {
                            $convert_data = redis_hgetall(redis_create_crypto_key("$position->currency:USDT"));
                            $conversion = $convert_data['p'];
                            $balance = $position->amount + ($close - $position->limit) * $conversion * $position->value - ($position->amount * $position->swop);
                        }
                    } else {
                        $close = $request->status == 'take profit' ? $position->take_profit : $position->stop_loss;
                        if ($position->currency == 'USDT') {
                            $balance = $position->amount + ($position->limit - $close) * $position->value - ($position->amount * $position->swop);
                        } else {
                            $convert_data = redis_hgetall(redis_create_crypto_key("$position->currency:USDT"));
                            $conversion = $convert_data['p'];
                            $balance = $position->amount + ($position->limit - $close) * $conversion * $position->value - ($position->amount * $position->swop);
                        }
                    }
                } else {
                    if ($position->type == 'buy') {
                        $close = $request->status == 'take profit' ? $position->take_profit : $position->stop_loss;
                        $balance = $position->amount + ($close - $position->limit) * $position->value - ($position->amount * $position->swop);
                    } else {
                        $close = $request->status == 'take profit' ? $position->take_profit : $position->stop_loss;
                        $balance = $position->amount + ($position->limit - $close) * $position->value - ($position->amount * $position->swop);
                    }
                }
            }

            $position->update(['status' => $request->status]);

            $balance_before = $user->balance;
            $balance_after = $user->balance + $balance;

            try {
                $order = OrdersHistory::create([
                    'status' => $position->status,
                    'class' => $position->class,
                    'entry_price' => $position->entry_price,
                    'type' => $position->type,
                    'amount' => $position->amount,
                    'limit' => $position->limit,
                    'value' => $position->value,
                    'pair' => $position->pair,
                    'currency' => $position->currency,
                    'liq' => $position->liq,
                    'lv' => $position->lv,
                    'user_id' => $position->user_id,
                    'leverage' => $position->leverage,
                    'close' => $close,
                    'time_of_open' => Carbon::parse($position->created_at, 'UTC'),
                    'type_of_market' => $position->type_of_market,
                    'unit' => $position->unit,
                    'balance_before_open' => $position->balance_before,
                    'balance_after_open' => $position->balance_after,
                    'balance_before_close' => $balance_before,
                    'balance_after_close' => $balance_after,
                    'front_balance' => $balance,
                    'swop' => $position->swop,
                    'crypto_amount' => $position->crypto_amount,
                    'conversion' => $conversion ?? 1,
                    'description' => $request->broker
                ]);

                $user->update([
                    'balance' => round($user->balance + $balance, 2)
                ]);

                Positions::where('id', $request->id)->delete();
            } catch (\Exception $e) {
                $position->restore();
                return $e->getMessage();
            }

            if ($user->role == 'bot') {
                $followers = User::where('follow', $user->id)->get();

                foreach ($followers as $follower) {
                    $fol_pos = Positions::where('user_id', $follower->id)->where('limit', $position->limit)->where('lv', $position->lv)->where('leverage', $position->leverage)->first();
                    if (!$fol_pos) continue;

                    Positions::where('user_id', $follower->id)->where('limit', $position->limit)->where('lv', $position->lv)->where('leverage', $position->leverage)->delete();

                    $copy_order = $order->replicate();
                    $copy_order->user_id = $follower->id;
                    $copy_order->balance_before_open = $fol_pos->balance_before;
                    $copy_order->balance_after_open = $fol_pos->balance_after;
                    $copy_order->balance_before_close = $follower->balance;
                    $copy_order->balance_after_close = $follower->balance + $balance;
                    $copy_order->save();

                    $follower->balance = $follower->balance + $balance;
                    $follower->save();

                    Http::put(env('CRM_URL') . 'api/update-balance', [
                        'email' => $follower->email,
                        'balance' => $follower->balance
                    ]);
                }
            } else {
                Http::put(env('CRM_URL') . 'api/update-balance', [
                    'email' => $user->email,
                    'balance' => $user->balance
                ]);
            }
        } else {
            return response()->json([
                'message' => 'Failed',
                'status' => 'false',
                'balance' => $user->balance
            ]);
        }

        return response()->json([
            'message' => 'Success',
            'status' => 'true',
            'data' => $order,
            'balance' => $user->balance
        ]);
    }
}
