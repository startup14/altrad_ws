<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\InitCommand;
use App\Console\Traits\CommandAbleTransfer;

class StocksInit extends InitCommand
{
    use CommandAbleTransfer;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stocks:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load stocks template';


    private function convertStocks(string $prev_fn, string $new_fn): int
    {
        $prev = include storage_path($prev_fn);
        $new = [];
        foreach ($prev as $key => $fields) {
            $fields['active'] = 1;
            $new[$key] = $fields;
        }
        return
            file_put_contents(
                storage_path($new_fn), "<?php \n return " . var_export($new, true) . ';'
            ) === 0;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $this->unlinkKeys(REDIS_STREAM_STOCKS);
        $proceeded = $this->loadTemplateFromStorageToRedis('symbols/stocks-v2.php', 'redis_create_stocks_key');
        $this->logProcessed($proceeded);
        return $proceeded;

//        return $this->convertStocks('symbols/stocks.php', 'symbols/stocks-v2.php');

    }
}
