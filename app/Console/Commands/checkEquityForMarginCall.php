<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\SafeCRMRequests;
use App\Inform;
use App\OrdersHistory;
use App\Positions;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class checkEquityForMarginCall extends BaseCommand
{
    use SafeCRMRequests;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'margin-call:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $user_ids = Positions::select('user_id')->groupBy('user_id')->get();
        $users = [];

        foreach ($user_ids as $id) {
            $users[] = $id->user_id;
        }
        $users = User::select(
            'id',
            'positions_fee',
            'balance',
            'email'
        )
            ->whereIn('id', $users)
            ->get();

        foreach ($users as $user) {
            $positions = Positions::where('user_id', $user->id)->get();
            $this->countEquity($user, $positions);
        }

        return 0;
    }

    private function traceUserPositions(User $user)
    {
        $sum = $user->balance;
        $marginUsed = 0;
        $positions = Positions::whereUserId($user->id)->get();

        foreach ($positions as $position) {
            $sum += $position->amount;
            $marginUsed += $position->amount;
        }
    }

    private function countEquity($user, $positions): void
    {
        $crypto = redis_hgetall_for_keyspace(REDIS_STREAM_CRYPTO);
        $stocks = redis_hgetall_for_keyspace(REDIS_STREAM_STOCKS);
        $commodities = redis_hgetall_for_keyspace(REDIS_STREAM_COMMO);

        $sum = $user->balance;
        $marginUsed = 0;
        foreach ($positions as $position) {
            $sum += $position->amount;
            $marginUsed += $position->amount;
            if ($position->type_of_market === 'Crypto') {
                $base = $position->pair;
                $quote = $position->currency;
                $crypto_key = redis_create_crypto_key("$base:$quote");

                if (! recursive_key_exists([$crypto_key, 'p'], $crypto)) {
                    $this->warningRedisKeysEmpty(sprintf('%s %s:%s', REDIS_STREAM_STOCKS, $crypto_key, 'p'));
                    continue;
                }

                $crypto_price = $crypto[$crypto_key]['p'];
                $crypto_opposite_key = redis_create_crypto_key("$quote:USDT");
                $random = $crypto_price * $user->positions_fee;

                $v = 0;
                if ($position->type === 'buy') {
                    $v = round(($crypto_price - $random - $position->limit) * ($position->value), 2);
                    if ($position->currency !== 'USDT'){
                        $crypto_opposite_price = $crypto[$crypto_opposite_key]['p'];
                        $v = round($v * $crypto_opposite_price, 2);
                    }
                } elseif ($position->type === 'sell') {
                    $v = round(($position->limit - $crypto_price - $random) * ($position->value), 2);
                    if ($position->currency !== 'USDT'){
                        $crypto_opposite_price = $crypto[$crypto_opposite_key]['p'];
                        $v = round($v * $crypto_opposite_price, 2);
                    }
                }
                $sum += $v;

            } elseif ($position->type_of_market === 'Stock') {
                $stock_key = redis_create_stocks_key($position->pair);

                if (! recursive_key_exists([$stock_key, 'p'], $stocks)) {
                    $this->warningRedisKeysEmpty(sprintf('%s %s:%s', REDIS_STREAM_STOCKS, $stock_key, 'p'));
                    continue;
                }

                $stock_price = $stocks[$stock_key]['p'] ?? 0;
                $random = $stock_price * $user->positions_fee;

                if ($position->type === 'buy') {
                    $sum += round(($stock_price - $random - $position->limit) * $position->value, 2);
                } elseif ($position->type === 'sell') {
                    $sum += round(($position->limit - $stocks[$stock_key]['p'] - $random) * $position->value, 2);
                }

            } elseif ($position->type_of_market === 'Commodities') {
                $commo_key = redis_create_commodities_key($position->pair);
                // todo: wtf if p not set?
                $commo_price = $commodities[$commo_key]['p'] ?? 0;
                $random = $commo_price * $user->positions_fee;

                if ($position->type === 'buy') {
                    $sum += round(($commo_price - $random - $position->limit) * $position->value, 2);
                } elseif ($position->type === 'sell') {
                    $sum += round(($position->limit - $commo_price - $random) * $position->value, 2);
                }
            }
        }

        $balanceMargin = $marginUsed + $user->balance;
        $percentLeft = $sum / $balanceMargin * 100;

        if ($percentLeft < 20) {
            $inform = Inform::where('user_id', $user->id)->where('type', 'margin_call')->latest()->first();
            if ($inform) {
                if ($inform->created_at < Carbon::now()->subMinutes(30)) {
                    $this->notifyAboutMarginCall($user, $percentLeft);
                }
            } else {
                $this->notifyAboutMarginCall($user, $percentLeft);
            }
        }
        if ($sum < 0){
            $this->closeByMarginCall($user);
        }
    }

    private function closeByMarginCall($user): void
    {
        $positions = Positions::where('user_id', $user->id)->get();

        $crypto = redis_hgetall_for_keyspace('crypto');
        $stocks = redis_hgetall_for_keyspace('stocks');
        $commodities = redis_hgetall_for_keyspace('commodities');

        foreach ($positions as $position) {
            $position->update(['status' => 'margin call']);
            $close = 0;
            $conversion = 1;

            if ($position->type_of_market === 'Crypto') {
                $base = $position->pair;
                $quote = $position->currency;

                $crypto_key = redis_create_crypto_key("$base:$quote");
                $crypto_opposite_key = redis_create_crypto_key("$quote:USDT");

                $crypto_price = $crypto[$crypto_key]['p'];

                $conversion = null;

                if ($position->type === 'buy') {
                    $close = $crypto_price - ($crypto_price * $user->positions_fee);
                } elseif ($position->type === 'sell') {
                    $close = $crypto_price + ($crypto_price * $user->positions_fee);
                }
                if ($position->currency !== 'USDT'){
                    $conversion = $crypto[$crypto_opposite_key]['p'];
                }

            } elseif ($position->type_of_market === 'Stock') {
                $stock_key = redis_create_stocks_key($position->pair);
                if (!recursive_key_exists([$stock_key, 'p'], $stocks)) {
                    $this->warningRedisKeysEmpty(sprintf('%s %s:%s', REDIS_STREAM_STOCKS, $stock_key, 'p'));
                    continue;
                }
                $stock_price = $stocks[$stock_key]['p'];

                if ($position->type === 'buy') {
                    $close = $stock_price - ($stock_price * $user->positions_fee);
                } elseif ($position->type === 'sell') {
                    $close = $stock_price + ($stock_price * $user->positions_fee);
                }

            } elseif ($position->type_of_market === 'Commodities') {
                $commo_key = redis_create_commodities_key($position->pair);
                $commo_price = $commodities[$commo_key]['p'];
                $commo_fee = $commo_price * $user->positions_fee;

                if ($position->type === 'buy') {
                    $close = $commo_price - $commo_fee;
                } elseif ($position->type === 'sell') {
                    $close = $commo_price + $commo_fee;
                }
            }

            OrdersHistory::create([
                'status' => $position->status,
                'class' => $position->class,
                'entry_price' => $position->entry_price,
                'type' => $position->type,
                'amount' => $position->amount,
                'crypto_amount' => $position->crypto_amount,
                'limit' => $position->limit,
                'value' => $position->value,
                'pair' => $position->pair,
                'currency' => $position->currency,
                'liq' => $position->liq,
                'lv' => $position->lv,
                'user_id' => $position->user_id,
                'leverage' => $position->leverage,
                'close' => $close,
                'time_of_open' => Carbon::parse($position->created_at, 'UTC'),
                'type_of_market' => $position->type_of_market,
                'unit' => $position->unit,
                'balance_before_open' => $position->balance_before,
                'balance_after_open' => $position->balance_after,
                'balance_before_close' => $user->balance,
                'balance_after_close' => $user->balance,
                'conversion' => $conversion
            ]);

            $position->delete();

            $user->update([
                'balance' => 0
            ]);
        }

        $this->crmHttpPutJson('api/update-balance', [
            'email' => $user->email,
            'balance' => $user->balance
        ]);
    }

    private function notifyAboutMarginCall($user, $left): void
    {
        $this->crmHttpPostJson('api/notification/margin-call', [
            'email' => $user->email,
            'left' => $left
        ]);

        Inform::create([
            'user_id' => $user->id,
            'type' => 'margin_call'
        ]);
    }
}
