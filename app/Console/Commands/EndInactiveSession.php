<?php

namespace App\Console\Commands;

use App\ConnectionHistory;
use Carbon\Carbon;
use Illuminate\Console\Command;

class EndInactiveSession extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inactive-sessions:end';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $unclosed_sessions = ConnectionHistory::whereNull('closed_at')->get();

        foreach ($unclosed_sessions as $session) {
            if (Carbon::now()->diffInMinutes($session->updated_at) >= 10) {
                $session->update([
                    'closed_at' => Carbon::now()->subminutes(10)
                ]);
            }
        }

        return 0;
    }
}
