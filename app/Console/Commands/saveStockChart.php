<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\CommandUsesHistorical;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class saveStockChart extends BaseCommand
{
    use CommandUsesHistorical;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock-chart:store';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle(): int
    {
        $market = Cache::get('market');

        if ($market['open'] ?? false) {
            $stocks = redis_hgetall_for_keyspace('stocks');
            foreach ($stocks as $redis_key => $quote) {
                ['base' => $symbol] = redis_key_to_base_and_quote($redis_key);
                $this->storeDataForChart($symbol, $quote);
            }
        }

        return 0;
    }

    /**
     * @throws \Exception
     */
    public function storeDataForChart($symbol, $data): bool
    {
        $price = (float) $this->ensureKeyExists('p', $data);
        $periods = $this->getPeriods();
        $sym_fee = Cache::get('currency-fees');

        foreach ($periods as $period => $time) {
            $check = $this->historicalDataFor($symbol, $period);
            $fee = $this->chooseFee($sym_fee, $symbol);

            if ($check) {
                $data['v'] = $this->ensureKeyExists('v', $data);
                $v = $this->setupVolume($check, $period, $data, $time);
                $prev = $this->historicalPrevDataFor($symbol, $period, $check->open_time - 1);

                if ($fee) {
                    $price += $fee['now'] ?? 0;
                }


                try {
                    $check->update([
                        'close' => $price,
                        'high' => $check->high > $price ? $check->high : $price,
                        'low' => $check->low < $price ? $check->low : $price,
                        'open' => $prev->close ?? $data['pc'],
                        'volume' => $v,
                    ]);
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                }
            } else {
                $data['v'] = 0; // wtf?
                $this->historicalCreate($symbol, $period, $data, $time);
            }

        }

        return true;
    }
}
