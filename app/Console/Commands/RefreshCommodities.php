<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\RefreshCommand;
use App\Console\Ship\Transformers\DefaultTickerTransformer;
use App\Contracts\CommoditiesProviderInterface as CommoditiesProvider;

class RefreshCommodities extends RefreshCommand
{
    protected $signature = 'commodities:refresh';
    protected $description = 'Command description';

    private CommoditiesProvider $provider;

    public function __construct()
    {
        parent::__construct();
        $this->provider = app(CommoditiesProvider::class);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $commo = $this->ensureHashKeysExist(REDIS_STREAM_COMMO, 'commodities:init');
        $commo_keys = redis_keys_for_keyspace_base(REDIS_STREAM_COMMO);

        $tickers = $this->provider->getTickers($commo_keys);
        $updated = [];

        foreach ($tickers ?? [] as $ticker) {
            $redis_key = redis_create_commodities_key($ticker['symbol']);
            $parent = $commo[$redis_key];
            $trait = (new DefaultTickerTransformer())->transform($ticker);
            $updated[$redis_key] = array_merge($parent, $trait);
        }

        redis_hmsetall($updated);

        $processed = count($updated);
        $this->logProcessed($processed);

        return $processed;
    }
}
