<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class updateCurrencyFee extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currency-fee:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $fees = Cache::get('currency-fees');

        foreach ($fees as $key => $fee) {
            if ($fee['now'] !== $fee['fee']) {
                $fees[$key]['now'] += $fee['step'];
            }
        }

        Cache::put('currency-fees', $fees);

        return 0;
    }
}
