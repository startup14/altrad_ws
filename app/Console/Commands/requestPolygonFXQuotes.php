<?php

namespace App\Console\Commands;

use App\Contracts\CommoditiesProviderInterface as CommoditiesProvider;
use App\ZeroLog;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class requestPolygonFXQuotes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pol-fx:request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var CommoditiesProvider
     */
    private $provider;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->provider = app(CommoditiesProvider::class);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $commodities = Cache::get('commodities');

        if (empty($commodities)) {
            return 0;
        }

        $symbols = array_map(function($symbol) {
            return "C:{$symbol['graph']}";
        }, $commodities);

        $tickers = $this->provider->getTickers($symbols);

        if (isset($tickers)) {
            foreach ($tickers as $ticker) {

                $quote = &$commodities[$ticker['symbol']];

                if ($ticker['close'] <= 0 || is_null($ticker['close'])) {
                    Logger::create([
                        'description' => print_r($ticker, true),
                    ]);
                }

                $quote['pp'] = $quote['p'] ?? $ticker['close'];
                $quote['p'] = $ticker['close'] ?? $quote['p'];
                $quote['h'] = $ticker['high'] ?? $quote['h'];
                $quote['l'] = $ticker['low'] ?? $quote['low'];
                $quote['pc'] = $ticker['open'] ?? $quote['pc'];
                $quote['v'] = $ticker['vol'] ?? $quote['v'];
            }

            Cache::put('commodities', $commodities);
        }
        return 0;
    }
}
