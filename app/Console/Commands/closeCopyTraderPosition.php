<?php

namespace App\Console\Commands;

use App\Console\Traits\SafeCRMRequests;
use App\OrdersHistory;
use App\Positions;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class closeCopyTraderPosition extends Command
{
    use SafeCRMRequests;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'copy-trader-position:close';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        if ((Carbon::now()->hour > 11) && (Carbon::now()->minute > 1)) {
            $bots = User::where('role', 'bot')->get();
            if ($bots->count() === 0) {
                return -1;
            }
            $bots_ids = [];
            foreach ($bots as $bot) {
                $bots_ids[] = $bot->id;
            }

            $bots_positions = Positions::whereIn('user_id', $bots_ids)->count();
            $day = Carbon::now()->dayOfWeek;
            if ($day === 4) {
                $to_close = (int)($bots_positions / 1.5);
            } elseif ($day === 5) {
                $to_close = $bots_positions;
            } else {
                $to_close = (int)($bots_positions / 2);
            }

            $positions_to_close = Positions::inRandomOrder()->whereIn('user_id', $bots_ids)->limit($to_close)->get();
            foreach ($positions_to_close as $close) {
                $this->closePosition(User::where('id', $close->user_id)->first(), $close);
            }
        }

        return 0;
    }

    public function closePosition($user, $position): void
    {
        $position->update([
            'status' => 'trader close'
        ]);

        $count_balance = [];

        if ($position->type_of_market === 'Stock') {
            $count_balance = $this->countBalance($user, $position, true);
        } elseif ($position->type_of_market === 'Crypto') {
            $count_balance = $this->countBalance($user, $position, false);
        }

        $updated_balance = $count_balance['balance'];

        $balance_before = $user->balance;
        $balance_after = $user->balance + $updated_balance;

        $user->update([
            'balance' => $user->balance + $updated_balance
        ]);

        $order = OrdersHistory::create([
            'status' => $position->status,
            'class' => $position->class,
            'entry_price' => $position->entry_price,
            'type' => $position->type,
            'amount' => $position->amount,
            'limit' => $position->limit,
            'value' => $position->value,
            'pair' => $position->pair,
            'currency' => $position->currency,
            'liq' => $position->liq,
            'lv' => $position->lv,
            'user_id' => $position->user_id,
            'leverage' => $position->leverage,
            'close' => $count_balance['close'],
            'time_of_open' => Carbon::createFromFormat('Y-m-d H:i:s', $position->created_at, 'UTC'),
            'type_of_market' => $position->type_of_market,
            'unit' => $position->unit,
            'balance_before_open' => $position->balance_before,
            'balance_after_open' => $position->balance_after,
            'balance_before_close' => $balance_before,
            'balance_after_close' => $balance_after,
            'front_balance' => $count_balance['balance'],
            'swop' => $position->swop
        ]);

        $followers = User::where('follow', $user->id)->get();

        foreach ($followers as $follower) {
            $fol_pos = Positions::where('user_id', $follower->id)->where('limit', $position->limit)->where('lv', $position->lv)->where('leverage', $position->leverage)->first();
            if (!$fol_pos) continue;

            $copy_order = $order->replicate();
            $copy_order->user_id = $follower->id;
            $copy_order->balance_before_open = $fol_pos->balance_before;
            $copy_order->balance_after_open = $fol_pos->balance_after;
            $copy_order->balance_before_close = $follower->balance;
            $copy_order->balance_after_close = $follower->balance + $updated_balance;
            $copy_order->save();

            $follower->balance += $updated_balance;
            $follower->save();

            $fol_pos->delete();

            $this->crmHttpPutJson('api/update-balance', [
                'email' => $follower->email,
                'balance' => $follower->balance
            ]);
        }

        $position->delete();

        $this->crmHttpPutJson('api/update-balance', [
            'email' => $user->email,
            'balance' => $user->balance
        ]);
    }

    public function countBalance($user, $position, $stock): array
    {
        $redis_key = $stock
            ? redis_create_key_for_keyspace('stocks', $position->pair)
            : redis_create_key_for_keyspace('crypto', $position->pair, $position['currency']);

        $rate = redis_hgetall($redis_key)['p'];
        $random = $rate * $user->positions_fee;
        $balance = $close = 0;

        if ($position->type === 'buy') {
            $close = $rate - $random;

            if ($position->type_of_market === 'Stock') {
                $balance = $position->amount + ($close - $position->limit) * ($position->value) - ($position->amount * $position->swop);
            } elseif ($position->type_of_market === 'Crypto') {
                $balance = ($position->currency === 'USDT')
                    ? $position->amount + ($close - $position->limit) * ($position->value)
                    : $position->amount + ($close - $position->limit) * $rate * ($position->value);

            }
            $balance = round($balance - ($position->amount * $position->swop), 2);
        } elseif ($position->type === 'sell') {
            $close = $rate + $random;

            if ($position->type_of_market === 'Stock') {
                $balance = $position->amount + ($position->limit - $close) * ($position->value);
            } elseif ($position->type_of_market === 'Crypto') {
                $balance = ($position->currency === 'USDT')
                    ? $position->amount + ($position->limit - $close) * ($position->value)
                    : $position->amount + ($position->limit - $close) * $rate * ($position->value);
            }
            $balance = round($balance - ($position->amount * $position->swop), 2);
        }

        return compact('close', 'balance');
    }
}
