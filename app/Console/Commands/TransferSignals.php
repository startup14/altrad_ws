<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\CommandAbleTransfer;
use Illuminate\Support\Facades\Cache;

class TransferSignals extends BaseCommand
{
    use CommandAbleTransfer;

    protected $signature = 'signals:transfer';
    protected $description = 'Transfer signals cache data from prev stage';

    public function handle(): int
    {
        $processed = 0;

        $source = $this->grabJsonResource(env('TRANSFER_FROM_API_HOST'), 'signals');
        $currencies = $source['currencies'];

        if ($currencies) {
            Cache::put(REDIS_STREAM_SIGNALS, $currencies);
            $processed = count($currencies);
        }

        $this->logProcessed($processed);
        return $processed;
    }
}
