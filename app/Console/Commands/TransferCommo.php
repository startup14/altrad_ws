<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\CommandAbleTransfer;

class TransferCommo extends BaseCommand
{
    use CommandAbleTransfer;

    protected $signature = 'commodities:transfer';
    protected $description = 'Transfer commodities data from prev ver via API';

    private function transferCommo(string $server, string $path): int
    {
        return $this->transferFrom(REDIS_STREAM_COMMO, $server, $path);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $processed = $this->transferCommo(env('TRANSFER_FROM_API_HOST'), 'currency-rate');
        $this->logProcessed($processed);

        return $processed;
    }
}
