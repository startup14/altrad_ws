<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\ExecuteCommandsGroup;

class CacheExtractAll extends BaseCommand
{
    use ExecuteCommandsGroup;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:extract-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        return 0;
    }
}
