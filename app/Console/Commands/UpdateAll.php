<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\SmartOutput;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;

class UpdateAll extends BaseCommand
{
    use SmartOutput;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'all:update {mode=silent}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        // Redeploy line.

        $this->lineRedisServerInfo();

        $init_modes = [
            'silent' =>     ['inspire'],
            'clear' =>      ['redis:clear', 'market:status'],
            'transfer' =>   ['redis:clear', 'market:status', 'transfer:all', 'book:set', 'market:status'],
            'refresh' =>    ['redis:clear', 'market:status', 'init:all', 'refresh:all', 'signals:set', 'book:set', 'market:status'],
        ];

        $mode = $this->argument('mode');

        $init_commands = $init_modes[$mode];

        $outputs = [];

        foreach ($init_commands as $command) {
            $retcode = Artisan::call($command);
            $line = "$command done. $retcode lines proceeded";
            $this->infoTime($line);
            $outputs[] = $line;
        }

        Cache::put('force-update', true);
        $this->infoTime('force-update set true.');

        Cache::put('currency-fees', array());
        $this->infoTime('currency-fees set [].');

        if (PHP_SAPI !== 'cli') {
            $this->smartOutput($outputs);
        }

        return 0;
    }
}
