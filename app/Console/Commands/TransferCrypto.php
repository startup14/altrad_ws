<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\CommandAbleTransfer;

final class TransferCrypto extends BaseCommand
{
    use CommandAbleTransfer;

    protected $signature = 'crypto:transfer';
    protected $description = 'Transfer crypto data from preprod via API';

    private function assignTarget(
        string $fun_key, string $base, array $details, array &$target):void
    {
        $title = $details['name'] ?? $base;

        $details = array_filter($details, static function ($value) {
            return is_array($value);
        });

        foreach ($details as $quote => $data) {
            $redis_key = $fun_key("$base:$quote");
            $data['name'] = $title;
            $target[$redis_key] = $data;
        }
    }

    private function transferCrypto(string $server, string $path): int
    {
        return $this->transferFrom(REDIS_STREAM_CRYPTO, $server, $path);
    }

    public function handle(): int
    {
        $processed = $this->transferCrypto(env('TRANSFER_FROM_API_HOST'), 'currency-rate');
        $this->logProcessed($processed);

        return $processed;
    }
}
