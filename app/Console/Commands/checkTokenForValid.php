<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class checkTokenForValid extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'token:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $online_users = User::select('id', 'remember_token')
            ->whereNotNull('remember_token')
            ->get();

        foreach ($online_users as $user) {
            try {
                auth()->setToken($user->remember_token)->userOrFail();
            } catch (\Exception $e) {
                $user->remember_token = null;
                $user->save();
            }
        }

        return 0;
    }
}
