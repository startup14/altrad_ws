<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\SmartOutput;
use Illuminate\Support\Facades\Redis;

class RedisTrimLog extends BaseCommand
{
    use SmartOutput;

    public const LOG_SUFFIX = ':log';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:trim {stream} {--l|maxlen=100}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trim Redis stream by key name';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $stream = $this->argument('stream') . self::LOG_SUFFIX;
        $maxlen = $this->option('maxlen');
        $output = [];

        $xlen = Redis::xlen($stream);
        $output[] = "$stream BEFORE xlen: $xlen";

        $output[] = "Trimming to $maxlen";
        Redis::xtrim($stream, $maxlen);

        $xlen = Redis::xlen($stream);
        $output[] = "$stream AFTER xlen: $xlen";

        $this->smartOutput($output);

        return 0;
    }
}
