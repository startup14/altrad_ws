<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\SafeCRMRequests;
use Illuminate\Support\Facades\Http;

class checkDepositsOnCapitalist extends BaseCommand
{
    use SafeCRMRequests;

    private string $secret = 'coinmarketsolutioncoinmarketsolution';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deposits:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks deposits status on capitalist';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $url = 'https://capitalist.net/merchant/payGate/checkstate';
        $secret = $this->secret;

        $to_check = $this->crmHttpGetJson('api/clients/financial-process', [], 'deps');

        foreach ($to_check as $check) {
            $order = [
                'merchantid' => 99927,
                'order_number' => $check['order_number'],
                'amount' => $check['amount'],
                'currency' => $check['currency']
            ];

            $order['sign'] = $this->signData($order, $secret);

            $response = Http::get($url, [
                'merchant_id' => 99927,
                'order_number' => $order['order_number'],
                'currency' => $order['currency'],
                'sign' => $order['sign'],
                'amount' => $order['amount']
            ])->json();

            if (!is_null($response) && $response['success']) {
                $order = $response['data']['order'];
                if ($order['paid']) {
                    $this->crmHttpPostJson('api/deposit/update', [
                        'order_number' => $order['order_number']
                    ]);
                }
                return 1;
            }
        }

        return 0;
    }

    public function signData($data, $secret)
    {
        unset($data['sign']);
        ksort($data, SORT_STRING);
        $str = implode(':', $data);
        return hash_hmac('md5', $str, $secret);
    }
}
