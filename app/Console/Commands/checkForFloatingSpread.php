<?php

namespace App\Console\Commands;

use App\CurrencySpread;
use App\DynamicSpreadSetting;
use App\CurrencyList;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class checkForFloatingSpread extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'floating-spread:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $crypto = Cache::get('crypto');
        $setting = DynamicSpreadSetting::first();

        foreach ($crypto as $baseSymbol => $base) {
            foreach (array_keys($base['quotes']) as $quoteSymbol) {
                if (!in_array($quoteSymbol, ['name', 'active'])) {
                    $currency = CurrencyList::where('name', $baseSymbol)->first();
                    $changes = Http::get("https://api.binance.com/api/v3/klines?symbol={$baseSymbol}{$quoteSymbol}&interval=1m&limit=2")->json();
                    $first_num = $changes[0][4];
                    $second_num = $changes[1][4];

                    if ($first_num > $second_num) {
                        $dif = ($first_num / $second_num - 1) * 100;
                    } else {
                        $dif = ($second_num / $first_num - 1) * 100;
                    }

                    if ($setting && ($currency->active == '2')) {
                        if ($dif < $setting->bot) {
                            $crypto[$baseSymbol]['quotes'][$quoteSymbol]['spread'] = $setting->coef * ($setting->bot / ($setting->top / 100)) / 10000;
                        } elseif ($dif > $setting->top) {
                            $crypto[$baseSymbol]['quotes'][$quoteSymbol]['spread'] = $setting->coef * ($setting->top / ($setting->top / 100)) / 10000;
                            $currency->update([
                                'status' => 0
                            ]);
                        } else {
                            $crypto[$baseSymbol]['quotes'][$quoteSymbol]['spread'] = $setting->coef * ($dif / ($setting->top / 100)) / 10000;
                        }
                    }
                }
            }
        }

        Cache::put('crypto', $crypto);
        return 0;
    }
}
