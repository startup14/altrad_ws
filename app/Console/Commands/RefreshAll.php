<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\ExecuteCommandsGroup;

class RefreshAll extends BaseCommand
{
    use ExecuteCommandsGroup;

    protected $signature = 'refresh:all';
    protected $description = 'Refreshes all quotes (crypto, stocks and commo';

    public function handle(): int
    {
        $commands = ['crypto:refresh', 'stocks:refresh', 'commodities:refresh'];
        $this->infoTime("Starting $this->signature...");
        return $this->handleCommands($commands);
    }
}
