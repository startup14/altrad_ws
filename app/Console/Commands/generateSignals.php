<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class generateSignals extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'signals:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $signals = Cache::get(REDIS_STREAM_SIGNALS);
        $crypto = redis_hgetall_for_keyspace(REDIS_STREAM_CRYPTO);

        if (Carbon::now()->minute === 59) {
            foreach ($signals as $key => $signal) {
                $crypto_key = redis_create_crypto_key("$key:USDT");
                $currentPrice = $crypto[$crypto_key]['p'];
                $currentSignal = $signal['signals'][5];
                if ($currentSignal['move'] === 'Up') {
                    if ($currentSignal['reference'] > $currentPrice) {
                        $currentSignal['correct'] = false;
                    } else {
                        $currentSignal['correct'] = true;
                    }
                } elseif ($currentSignal['move'] === 'Down') {
                    if ($currentSignal['reference'] < $currentPrice) {
                        $currentSignal['correct'] = false;
                    } else {
                        $currentSignal['correct'] = true;
                    }
                }

                $currentSignal['close'] = $currentPrice;
                $signal['signals'][5] = $currentSignal;
                array_shift($signal['signals']);
                $signal['signals'][] = [
                    'move' => $this->randomMove(),
                    'correct' => null,
                    'reference' => (float) $currentPrice,
                    'hours' => Carbon::now()->addHours()->hour . ':00 - ' . Carbon::now()->addHours(2)->hour . ':00',
                ];

                $signals[$key] = $signal;
            }
        }

        Cache::put('signals', $signals);

        return 0;
    }

    public function randomMove(): string
    {
        $strings = ['Up', 'Down'];
        $key = array_rand($strings);
        return $strings[$key];
    }
}
