<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\ExecuteCommandsGroup;

class TransferAll extends BaseCommand
{
    use ExecuteCommandsGroup;

    protected $signature = 'transfer:all';
    protected $description = 'Transfer all quotes from file cache via API';

    public function handle(): int
    {
        $commands = [
            'crypto:transfer', 'stocks:transfer', 'commodities:transfer', 'signals:transfer',
        ];
        $this->infoTime(sprintf('Start transfer from %s...', env('TRANSFER_FROM_API_HOST')));
        return $this->handleCommands($commands);
    }
}
