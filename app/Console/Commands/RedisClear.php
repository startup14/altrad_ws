<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use Illuminate\Support\Facades\Redis;

class RedisClear extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Execute <flushall sync> (clear all keys) on Redis";

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $count = Redis::flushall();
        $this->logProcessed($count);
        return $count;
    }
}
