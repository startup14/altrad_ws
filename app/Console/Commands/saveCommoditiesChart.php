<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\CommandUsesHistorical;
use Illuminate\Support\Facades\Cache;

class saveCommoditiesChart extends BaseCommand
{
    use CommandUsesHistorical;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commodities-chart:store';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle(): int
    {
        $commodities = redis_hgetall_for_keyspace('commodities');
        foreach ($commodities as $redis_key => $quote) {
            ['base' => $symbol] = redis_key_to_base_and_quote($redis_key);
            $this->storeDataForChart($symbol, $quote);
        }

        return 0;
    }

    /**
     * @throws \Exception
     */
    private function storeDataForChart($symbol, $data): bool
    {
        $price = $this->ensureKeyExists('p', $data, 0);
        $price_close = $this->ensureKeyExists('pc', $data, 0);
        $vol = $this->ensureKeyExists('v', $data, 0);

        $periods = $this->getPeriods();
        $sym_fee = Cache::get('currency-fees');

        foreach ($periods as $period => $time) {
            $check = $this->historicalDataFor($symbol, $period);
            $fee = $this->chooseFee($sym_fee, $symbol);
            if ($fee) {
                $fee = $fee['now'] ?? 0;
            }

            if ($check) {
                $prev = $this->historicalPrevDataFor($symbol, $period, $check->open_time - 1);

                $data['v'] = $vol;
                $check->volume = $this->setupVolume($check, $period, $data, $time);
                $check->close = $price;
                $check->open = $prev->close ?? $price_close;

                $check->high = $check->high > ($price + $fee)
                    ? $check->high
                    : $price + $fee;

                $check->low = $check->low < ($price + $fee)
                    ? $check->low
                    : $price + $fee;

                $check->save();
            } else {
                $this->historicalCreate($symbol, $period, $data, $time);
            }
        }

        return true;
    }
}
