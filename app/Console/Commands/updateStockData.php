<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Ship\Redis\RedisStreamNames;
use App\Console\Traits\SimplePriceUpdate;
use Carbon\Carbon;

class updateStockData extends BaseCommand
{
    use SimplePriceUpdate;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        if ((Carbon::now()->timestamp - Carbon::today()->timestamp) < 60) {
            $this->simplePriceUpdate(RedisStreamNames::STOCKS);
        }

        return 0;
    }
}
