<?php

namespace App\Console\Commands;

use App\Orders;
use App\Positions;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class OrderCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check available orders to buy or sell';

    private function transferOrder($order, $price): bool
    {
        return isset($price)
            && (
                (($order->type === 'buy') && ($order->limit >= $price))
                || (($order->type === 'sell') && ($order->limit <= $price))
            );
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $orders = Orders::all();

        foreach ($orders as $order) {
            $keyspace = strtolower($order->type_of_market);
            $redis_key = redis_create_key_for_keyspace($keyspace, $order->pair, $order->currency);
            if (in_array($keyspace, ['crypto', 'stock', 'commodities'])) {
                $price =  redis_hgetall($redis_key)['p'] ?? null;
                if ($this->transferOrder($order, $price)) {
                    $this->transferOrderToPosition(new Request([
                        'id' => $order->id,
                        'balance' => "-" . $order->amount,
                        'class' => 'positions',
                        'status' => 'new',
                    ]));
                }
            }
        }

        return 0;
    }

    private function transferOrderToPosition(Request $request): JsonResponse
    {
        $order = Orders::where('id', $request->id)->first();
        Orders::where('id', $request->id)->delete();

        $user = User::where('id', $order->user_id)->first();
        $balance_before = $user->balance;
        $balance_after = $user->balance + $request->balance;

        if ($user->balance < $order->amount) {
            return response()->json([
                'message' => 'Error',
                'status' => 'false',
            ]);
        }

        $user->update([
            'balance' => $user->balance + $request->balance
        ]);

        $position = Positions::create([
            'status' => $request->status,
            'class' => $request->class,
            'entry_price' => $request->entry_price,
            'init_price' => $request->entry_price,
            'type' => $order->type,
            'amount' => $order->amount,
            'limit' => $order->limit,
            'value' => $order->value,
            'pair' => $order->pair,
            'currency' => $order->currency,
            'liq' => $order->liq,
            'lv' => $order->lv,
            'user_id' => $order->user_id,
            'leverage' => $order->leverage,
            'type_of_market' => $order->type_of_market,
            'unit' => $order->unit,
            'balance_before' => $balance_before,
            'balance_after' => $balance_after,
            'crypto_amount' => $order->crypto_amount
        ]);
        $position = Positions::where('id', $position->id)->first();

        Http::put(env('CRM_URL') . 'api/update-balance', [
            'email' => $user->email,
            'balance' => $user->balance
        ]);

        return response()->json([
            'message' => 'Success',
            'status' => 'true',
            'position' => $position,
            'balance' => $user->balance
        ]);
    }
}
