<?php

namespace App\Console\Commands;

use App\Console\Ship\Bots\Bot;
use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\CommandUsesRedis;
use Illuminate\Support\Facades\Cache;


final class SetOrderBook extends BaseCommand
{
    use CommandUsesRedis;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'book:set';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update market depth cache value when necessary';

    private function botName(string $namespace): string
    {
        return ucfirst($namespace) . 'Bot';
    }

    private function simulatePrices(string $stream, string $base, string $quote, $data): array
    {
        $bot_name = Bot::BOTS_NS . '\\' . $this->botName($stream);
        return (new $bot_name)->generateActivity($base, $quote, $data);
    }

    private function emptyTradesAndOrders(): array
    {
        $orders = $trades = [];
        return compact('orders', 'trades');
    }

    public function handle(): int
    {
        // Redeploy

        $proceeded = 0;

        if (redis_keys_by_keyspace(REDIS_STREAM_CRYPTO)) {
            $crypto = redis_hgetall_for_keyspace(REDIS_STREAM_CRYPTO);
            foreach ($crypto as $redis_key => $fields) {
                ['base' => $baseSymbol, 'quote' => $quoteSymbol] = redis_key_to_base_and_quote($redis_key);
                $data = (INT)($fields['active'] ?? null)
                    ? $this->retrieveData($baseSymbol, $quoteSymbol, $crypto)
                    : $this->emptyTradesAndOrders();
                $this->infoBookSet($data, 'crypto');
                Cache::put("orders:$baseSymbol:$quoteSymbol", $data['orders']);
                Cache::put("trades:$baseSymbol:$quoteSymbol", $data['trades']);
                $proceeded++;
            }
        }

        if (redis_keys_by_keyspace(REDIS_STREAM_STOCKS)) {
            $stocks = redis_hgetall_for_keyspace(REDIS_STREAM_STOCKS);
            foreach ($stocks as $redis_key => $fields) {
                $data = $this->generateData($fields) ?? $this->emptyTradesAndOrders();
                ['base' => $symbol] = redis_key_to_base_and_quote($redis_key);
                $this->infoBookSet($data, 'stocks');
                Cache::put("orders:$symbol", $data['orders']);
                Cache::put("trades:$symbol", $data['trades']);
                $proceeded++;
            }
        }

        if (redis_keys_by_keyspace(REDIS_STREAM_COMMO)) {
            $commodities = redis_hgetall_for_keyspace(REDIS_STREAM_COMMO);
            foreach ($commodities as $redis_key => $fields) {
                $data = $this->generateCommodities($fields) ?? $this->emptyTradesAndOrders();
                ['base' => $symbol] = redis_key_to_base_and_quote($redis_key);
                $this->infoBookSet($data, 'commo');
                Cache::put("orders:$symbol", $data['orders']);
                Cache::put("trades:$symbol", $data['trades']);
                $proceeded++;
            }
        }

        return $proceeded;
    }

    private function infoBookSet(array $data, string $keyspace): void
    {
        $this->infoTime(sprintf('%s: Update %s: orders[%d], trades[%d]', strtoupper($this->signature),
            $keyspace, count($data['orders']), count($data['trades'])));
    }

    public function generateCommodities($fields): array
    {
        $orders = [
            'buy' => [],
            'sell' => []
        ];
        $buy = [];
        $sell = [];
        $trades = [];
        $amount = 14;

        $tradesBuyAmount = rand(1, $amount);
        $min = 100 * $fields['p'];
        $max = 300 * $fields['p'];

        for ($i = 0; $i < $amount; $i++) {
            $p = $fields['p'] + (rand(1, 10) / 10) * $fields['p'] * 0.001;
            $v = rand($min, $max);
            if ($p > 1) {
                $p = round($p, 2);
                $v = round($v / $p);
            } else {
                $p = round($p, 8);
                $v = round($v / $p);
            }
            $buy[] = [
                'p' => $p,
                'v' => $v
            ];

            $p = $fields['p'] - (rand(1, 10) / 10) * $fields['p'] * 0.001;
            $v = rand($min, $max);
            if ($p > 1) {
                $p = round($p, 2);
                $v = round($v / $p);
            } else {
                $p = round($p, 8);
                $v = round($v / $p);
            }
            $sell[] = [
                'p' => $p,
                'v' => $v
            ];
        }

        $orders['buy'] = array_slice($buy, 1, $amount / 2);
        $orders['sell'] = array_slice($sell, 1, $amount / 2);

        sort($orders['buy']);
        sort($orders['sell']);

        $trades_ask = array_slice($buy, 1, $tradesBuyAmount);
        $trades_bid = array_slice($sell, 1, $amount - $tradesBuyAmount);

        foreach ($trades_ask as $trade_ask) {
            $trades[] = [
                'p' => $trade_ask['p'],
                'v' => round($trade_ask['v'] / 2),
                'type' => 'buy'
            ];
        }
        foreach ($trades_bid as $trade_bid) {
            $trades[] = [
                'p' => $trade_bid['p'],
                'v' => round($trade_bid['v'] / 2),
                'type' => 'sell'
            ];
        }

        shuffle($trades);

        return [
            'orders' => $orders,
            'trades' => $trades
        ];

    }

    public function generateData(array $fields): array
    {
        $orders = [
            'buy' => [],
            'sell' => []
        ];
        $buy = [];
        $sell = [];
        $trades = [];
        $amount = 14;

        $price = Cache::get('stocks');
        $tradesBuyAmount = rand(1, $amount);
        $min = 100 * $fields['p'];
        $max = 300 * $fields['p'];

        for ($i = 0; $i < $amount; $i++) {
            $p = $fields['p'] + (rand(1, 10) / 10) * $fields['p'] * 0.001;
            $v = rand($min, $max);
            if ($p > 1) {
                $p = round($p, 2);
                $v = round($v / $p);
            } else {
                $p = round($p, 8);
                $v = round($v / $p);
            }
            $buy[] = [
                'p' => $p,
                'v' => $v
            ];

            $p = $fields['p'] - (rand(1, 10) / 10) * $fields['p'] * 0.001;
            $v = rand($min, $max);
            if ($p > 1) {
                $p = round($p, 2);
                $v = round($v / $p);
            } else {
                $p = round($p, 8);
                $v = round($v / $p);
            }
            $sell[] = [
                'p' => $p,
                'v' => $v
            ];
        }

        $orders['buy'] = array_slice($buy, 1, $amount / 2);
        $orders['sell'] = array_slice($sell, 1, $amount / 2);

        sort($orders['buy']);
        sort($orders['sell']);

        $trades_ask = array_slice($buy, 1, $tradesBuyAmount);
        $trades_bid = array_slice($sell, 1, $amount - $tradesBuyAmount);

        foreach ($trades_ask as $trade_ask) {
            $trades[] = [
                'p' => $trade_ask['p'],
                'v' => round($trade_ask['v'] / 2),
                'type' => 'buy'
            ];
        }
        foreach ($trades_bid as $trade_bid) {
            $trades[] = [
                'p' => $trade_bid['p'],
                'v' => round($trade_bid['v'] / 2),
                'type' => 'sell'
            ];
        }

        shuffle($trades);

        return [
            'orders' => $orders,
            'trades' => $trades
        ];

    }

    private function randomVolume(int $volume, int $min, int $max): float
    {
        return $min + (mt_rand() / mt_getrandmax()) * ($max - $min);
    }

    public function retrieveData($base, $quote, &$price): array
    {
        $orders = [
            'buy' => [],
            'sell' => []
        ];
        $buy = [];
        $sell = [];
        $trades = [];
        $amount = 14;

        $tradesBuyAmount = rand(1, $amount);
        $min = 100;
        $max = 15000;
        if ($quote !== 'USDT') {
            $temp_redis_key = redis_create_crypto_key("$quote:USDT");
            $min /= $price[$temp_redis_key]['p'];
            $max /= $price[$temp_redis_key]['p'];
        }

        $redis_key = redis_create_crypto_key("$base:$quote");
        for ($i = 0; $i < $amount; $i++) {
            $p = $price[$redis_key]['p'] + (rand(1, 10) / 10) * $price[$redis_key]['p'] * 0.001;
            $v = $min + mt_rand() / mt_getrandmax() * ($max - $min);
            if ($p > 1) {
                $p = round($p, 2);
                $v = round($v / $p, 6);
            } else {
                $p = round($p, 8);
                $v = round($v / $p, 1);
            }
            $buy[] = [
                'p' => $p,
                'v' => $v
            ];

            $p = $price[$redis_key]['p'] - (rand(1, 10) / 10) * $price[$redis_key]['p'] * 0.001;
            $v = $min + mt_rand() / mt_getrandmax() * ($max - $min);
            if ($p > 1) {
                $p = round($p, 2);
                $v = round($v / $p, 6);
            } else {
                $p = round($p, 8);
                $v = round($v / $p, 1);
            }
            $sell[] = [
                'p' => $p,
                'v' => $v
            ];
        }

        $orders['buy'] = array_slice($buy, 1, $amount / 2);
        $orders['sell'] = array_slice($sell, 1, $amount / 2);

        sort($orders['buy']);
        sort($orders['sell']);

        $trades_ask = array_slice($buy, 1, $tradesBuyAmount);
        $trades_bid = array_slice($sell, 1, $amount - $tradesBuyAmount);

        foreach ($trades_ask as $trade_ask) {
            $trades[] = [
                'p' => $trade_ask['p'],
                'v' => $trade_ask['v'] / 2,
                'type' => 'buy'
            ];
        }
        foreach ($trades_bid as $trade_bid) {
            $trades[] = [
                'p' => $trade_bid['p'],
                'v' => $trade_bid['v'] / 2,
                'type' => 'sell'
            ];
        }

        shuffle($trades);

        return [
            'orders' => $orders,
            'trades' => $trades
        ];

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
//    public function handle(): int
//    {
//        // todo: bots to service provider
//
//        $streams = [
//            RedisStreamNames::CRYPTO,
//            RedisStreamNames::STOCKS,
//            RedisStreamNames::COMMODITIES,
//        ];
//
//        foreach ($streams as $stream) {
//            $cache = redis_hgetall_for_keyspace($stream);
//            if (empty($cache)) {
//                continue;
//            }
//
//            foreach ($cache as $redis_key => $fields) {
//                if (is_arr_key_live('active', $fields)) {
//                    ['base' => $base, 'quote' => $quote] = redis_key_to_base_and_quote($redis_key);
//                    $orders = $this->simulatePrices($stream, $base, $quote, $fields);
//                    foreach ($orders as $market => $transactions) {
//                        $redis_market_key = $this->generateHashKey($market, $base, $quote);
//                        Cache::put($redis_market_key, $transactions);
//                    }
//                }
//            }
//        }
//
//        return 0;
//    }
}
