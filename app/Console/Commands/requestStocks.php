<?php

namespace App\Console\Commands;

use App\HistoricalData;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class requestStocks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'request-stocks:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $available = Cache::get('market');
        if ($available['open']) {
            $stocks = Cache::get('stock_symbols');
            $ticker_string = '';
            $num = 1;

            $keys = array_keys($stocks);
            $last = array_pop($keys);

            foreach ($stocks as $stock_key => $stock_info) {
                if ($num == 50) {
                    $this->updateStocks($ticker_string);
                    $num = 1;
                    $ticker_string = '';
                } elseif ($stock_key == $last) {
                    $ticker_string .= $stock_key . ',';
                    $this->updateStocks($ticker_string);
                }

                $ticker_string .= $stock_key . ',';
                $num++;
            }
        }

        return 0;
    }

    function updateStocks($string)
    {
        $sym_fee = Cache::get('currency-fees');
        $response = Http::get("https://cloud.iexapis.com/stable/stock/market/batch?symbols=$string&types=quote&token=pk_a689c778c2c34dfbbc65bfcc01a00250")->json();

        $price = Cache::get('stocks');
        if (!is_null($response)) {
            foreach ($response as $key => $ticker) {
                if (empty($sym_fee)) {
                    $fee = 0;
                } else {
                    if (array_key_exists($key . 'USDT', $sym_fee)) {
                        $fee = $sym_fee[$key . 'USDT']['now'];
                    } else {
                        $fee = 0;
                    }
                }
                if (array_key_exists('quote', $ticker)) {
                    $ticker = $ticker['quote'];

                    if ($ticker['latestPrice'] > 1){
                        $price[$key]['p'] = round($ticker['latestPrice'] + $fee, 2);
                        $price[$key]['h'] = (($ticker['latestPrice'] + $fee) > $ticker['high']) ? round($ticker['latestPrice'] + $fee, 2) : round($ticker['high'], 2);
                        $price[$key]['l'] = (($ticker['latestPrice'] + $fee) < $ticker['low']) ? round($ticker['latestPrice'] + $fee, 2) : round($ticker['low'], 2);
                        $price[$key]['pc'] = round($ticker['previousClose'], 2);
                        $price[$key]['v'] = $ticker['volume'];
                        $price[$key]['o'] = is_null($ticker['open']) ? round($ticker['latestPrice'], 2) : round($ticker['open'], 2);
                    } else {
                        $price[$key]['p'] = $ticker['latestPrice'] + $fee;
                        $price[$key]['h'] = (($ticker['latestPrice'] + $fee) > $ticker['high']) ? ($ticker['latestPrice'] + $fee) : $ticker['high'];
                        $price[$key]['l'] = (($ticker['latestPrice'] + $fee) < $ticker['low']) ? ($ticker['latestPrice'] + $fee) : $ticker['low'];
                        $price[$key]['pc'] = $ticker['previousClose'];
                        $price[$key]['v'] = $ticker['volume'];
                        $price[$key]['o'] = is_null($ticker['open']) ? $ticker['latestPrice'] : $ticker['open'];
                    }

                    Cache::put('stocks', $price);
                }
            }
        }
    }
}
