<?php

namespace App\Console\Commands;

use App\Console\Ship\Parents\BaseCommand;
use App\Console\Traits\SafeCRMRequests;
use App\OrdersHistory;
use App\PammAccount;
use App\Positions;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class closePammPosition extends BaseCommand
{
    use SafeCRMRequests;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pamm-position:close';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $start = rand(1, 20); $stop = rand(1, 20);

        if ($start === $stop) {
            $pamms = PammAccount::where('balance', '>', 0)->get();
            $pamms_ids = [];
            foreach ($pamms as $pamm) {
                $pamms_ids[] = $pamm->owner_id;
            }

            $bots_positions = Positions::whereIn('user_id', $pamms_ids)->count();
            if ($bots_positions === 0) return 0;

            $positions_to_close = Positions::inRandomOrder()->whereIn('user_id', $pamms_ids)->first();
            foreach ($positions_to_close as $close) {
                $this->closePosition(User::where('id', $close->user_id)->first(), $close);
            }
        }

        return 0;
    }

    public function closePosition($user, $position): void
    {
        $position->update([
            'status' => 'trader close'
        ]);

        $count_balance = [];

        if ($position->type_of_market === 'Stock') {
            $count_balance = $this->countBalance($user, $position, true);
        } elseif ($position->type_of_market === 'Crypto') {
            $count_balance = $this->countBalance($user, $position, false);
        }

        $updated_balance = $count_balance['balance'] - ($position->amount * $position->swop);

        $balance_before = $user->balance;
        $balance_after = $user->balance + $updated_balance;

        $user->update([
            'balance' => $user->balance + $updated_balance
        ]);

        OrdersHistory::create([
            'status' => $position->status,
            'class' => $position->class,
            'entry_price' => $position->entry_price,
            'type' => $position->type,
            'amount' => $position->amount,
            'limit' => $position->limit,
            'value' => $position->value,
            'pair' => $position->pair,
            'currency' => $position->currency,
            'liq' => $position->liq,
            'lv' => $position->lv,
            'user_id' => $position->user_id,
            'leverage' => $position->leverage,
            'close' => $count_balance['close'],
            'time_of_open' => Carbon::parse($position->created_at, 'UTC'),
            'type_of_market' => $position->type_of_market,
            'unit' => $position->unit,
            'balance_before_open' => $position->balance_before,
            'balance_after_open' => $position->balance_after,
            'balance_before_close' => $balance_before,
            'balance_after_close' => $balance_after,
            'front_balance' => $count_balance['balance'],
            'swop' => $position->swop
        ]);

        $position->delete();

        $this->crmHttpPutJson('api/update-balance', [
            'email' => $user->email,
            'balance' => $user->balance
        ]);
    }

    public function countBalance($user, $position, $stock): array
    {
        $redis_key = redis_create_stocks_key((string)$position->pair);
        if (!$stock) {
            $redis_key = redis_create_crypto_key("$position->pair:$position->currency");
        }
        $fields = redis_hgetall($redis_key);
        $rate = $fields['p'];

        $random = $rate * $user->positions_fee;
        $close = $balance = RONINS_UNDEFINED_PRICE;
        if ($position->type === 'buy') {
            $close = $rate - $random;
            $balance = round($position->amount * $position->lv + ($close - $position->limit) * ($position->value * $position->lv));
            $balance = round($balance - $position->leverage * (($position->lv - 1) / $position->lv));
        } elseif ($position->type === 'sell') {
            $close = $rate + $random;
            $balance = round($position->amount * $position->lv + ($position->limit - $close) * ($position->value * $position->lv));
            $balance = round($balance - $position->leverage * (($position->lv - 1) / $position->lv));
        }

        return compact('close', 'balance');
    }
}
