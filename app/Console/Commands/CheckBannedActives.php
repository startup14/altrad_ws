<?php

namespace App\Console\Commands;

use App\CurrencyList;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class CheckBannedActives extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'actives:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $crypto = redis_hgetall_for_keyspace('crypto');
        $actives = CurrencyList::all();

        foreach ($crypto as $redis_key => $fields) {
            ['base' => $baseSymbol, 'quote' => $quoteSymbol] = redis_key_to_base_and_quote($redis_key);
            if (!in_array($quoteSymbol, ['name', 'active'])) {
                $rec = $actives
                    ->where('type', 'crypto')
                    ->where('name', "$baseSymbol$quoteSymbol")
                    ->first();
                $fields['active'] = $rec->status ?? 0;
                $crypto[$redis_key] = $fields;
            }
        }

        redis_hmsetall($crypto);

        return 0;
    }
}
