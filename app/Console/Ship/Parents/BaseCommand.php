<?php

namespace App\Console\Ship\Parents;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

abstract class BaseCommand extends Command
{
    protected int $line_width = 80;
    protected string $keyspace;

    /**
     * @throws \JsonException
     */
    protected function infoArrPretty(string $intro, array $arr): void
    {
        $var = json_encode($arr, JSON_THROW_ON_ERROR | true);
        if (strlen($var) > $this->line_width) {
            $var = substr($var, 0, $this->line_width-1);
        }
        $this->infoTime(sprintf("$intro %s", $var));
    }

    protected function warningRedisKeysEmpty(string $keyspace): void
    {
        $this->infoTime("WARNING: $keyspace keys are empty. Try $keyspace:init or $keyspace:transfer commands.");
    }

    protected function lineRedisServerInfo(): void
    {
        $info = sprintf('Flirting with prefix "%s" on [%s:%s]', env('REDIS_PREFIX'), env('REDIS_HOST'), env('REDIS_PORT'));
        $this->infoTime($info);
    }

    protected function lineProcess(string $cmd, int $count): void
    {
        $this->infoTime(sprintf('%s: %d', $cmd, $count));
    }

    protected function logProcessed(int $processed): void
    {
        $this->infoTime(sprintf('%s: %d processed.', $this->signature, $processed));
    }

    protected function redisXadd(string $key, array $fields): array
    {
        return redis_xadd($key, $fields);
    }

    protected function redisSadd(string $key, string $value): array
    {
        return redis_sadd($key, $value);
    }

    protected function logError(string $error): void
    {
        Log::error($error);
    }

    protected function redisLog(string $stream, array $fields): void
    {
        $response = $this->redisXadd($stream, $fields);
        if (array_key_exists('error', $response)) {
            $this->info($response['error']);
            return;
        }
        $xlen = $response['xlen'];
        if ($xlen > RONINS_WATCH_MAXLEN) {
            $message = sprintf('WARNING: %s maxlen(%d) > default(%d)', $stream, RONINS_WATCH_MAXLEN, $xlen);
            $this->redisXadd($stream, compact('message'));
        }
    }

    protected function infoTime(string $info): void
    {
        $message = implode(': ', [Carbon::now(), $info]);
        $this->info($message);
        Log::info($message);
        $this->redisLog('api:log', compact('message'));
    }

    protected function ensureKeyExists(string $key, array $details, $default = 'n/a')
    {
        if (!array_key_exists($key, $details)) {
            Log::warning("Key <$key> not found in: " . print_r($details, true));
            return $default;
        }
        return $details[$key];
    }

}
