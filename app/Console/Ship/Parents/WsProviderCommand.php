<?php

namespace App\Console\Ship\Parents;


abstract class WsProviderCommand extends BaseCommand
{
    protected int $sleepSecs = 5;
    protected string $wsProvider;
    protected string $wsProviderTitle;
    protected string $wsProviderSecret;

    protected function cacheKeys(?string $keyspace): array
    {
        $keyspace ??= $this->keyspace;
        return redis_keys_by_keyspace($keyspace);
    }

    /**
     * @throws \JsonException
     */
    protected function updateCache(array $answer): int
    {
        // todo: redeploy
        $redis_key = $this->redisKey($answer);
        $parent = redis_hgetall($redis_key);
        $child = $this->transformIncoming($answer, $parent);

        if ($diff = array_diff($child, $parent)) {
            redis_hmset($redis_key, $child);
            $this->infoArrPretty("$this->wsProviderTitle -> Redis [$redis_key]:", $diff);
            $redis_watch_key = $this->redisWatchDogKey($answer);
            $this->redisXadd($redis_watch_key, $child);
            $redis_update_key = $this->redisUpdateKey();
            $this->redisSadd($redis_update_key, $redis_key);
            return 1;
        }

        return 0;
    }

    protected function showMustGoOn(array $keys): void
    {
        new $this->wsProvider($keys, function (array $answer) {
            $this->updateCache($answer);
        });
    }

    /**
     * @throws \JsonException
     */
    protected function waitingForShow(): array
    {
        $keys = $this->cacheKeys($this->keyspace);

        while (empty($keys)) {
            $this->warningRedisKeysEmpty($this->keyspace);
            $this->infoTime(sprintf('Waiting for transfer or init %d secs...', $this->sleepSecs));
            sleep($this->sleepSecs);
            $keys = $this->cacheKeys($this->keyspace);
        }

        return $keys;
    }

    protected function redisUpdateKey(): string
    {
        return redis_create_update_key($this->keyspace);
    }

    abstract protected function redisKey(array $data): string;
    abstract protected function redisWatchDogKey(array $data): string;
    abstract protected function transformIncoming(array $data, array $parent): array;
}
