<?php

namespace App\Console\Ship\Parents;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use RedisException;
use RuntimeException;

abstract class InitCommand extends BaseCommand
{
    protected function unlinkKeys(string $keyspace): int
    {
        $keys = redis_keys_by_keyspace($keyspace);
        return Redis::unlink($keys);
    }

    protected function loadTemplateFromStorageToRedis(string $template_fn, callable $redis_key_fn): int
    {
        $template = include storage_path($template_fn);

        $count = 0;

        try {
            Redis::pipeline(function($pipe) use ($redis_key_fn, $template, &$count) {
                foreach ($template as $quote => $values) {
                    $redis_key = $redis_key_fn($quote);
                    $pipe->hmset($redis_key, $values);
                    $count++;
                }
            });
        } catch (RedisException | RuntimeException $e) {
            Log::error($e->getMessage());
        }

        return $count;
    }
}
