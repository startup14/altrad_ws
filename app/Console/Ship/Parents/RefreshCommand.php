<?php

namespace App\Console\Ship\Parents;

use App\Console\Traits\CommandUsesRedis;
use Illuminate\Support\Facades\Log;
use LogicException;

class RefreshCommand extends BaseCommand
{
    use CommandUsesRedis;

    protected function ensureHashKeysExist(string $stream, $init_command): array
    {

        $cache_keys = redis_keys_by_keyspace($stream);
        if ($cache_keys) {
            return redis_hgetall_for_keyspace($stream);
        }

        $this->call($init_command);
        $redis_keys = redis_hgetall_for_keyspace($stream);
        $this->infoTime(sprintf("Init keys for $stream: %d", count($redis_keys)));

        if (empty($redis_keys)) {
            $error = "Cant get $stream keys. Check $stream:init command";
            Log::error($error);
            throw new LogicException($error);
        }

        return $redis_keys;
    }
}
