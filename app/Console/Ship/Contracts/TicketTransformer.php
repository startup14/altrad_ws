<?php

namespace App\Console\Ship\Contracts;

interface TicketTransformer
{
    public function transform(array $updates, array $parent = []): array;
}
