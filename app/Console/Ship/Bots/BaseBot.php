<?php

namespace App\Console\Ship\Bots;

class BaseBot
{
    public const DEALS = ['buy', 'sell'];

    protected function roundPrice(float $price): float
    {
        return $price > 1 ? round($price, 4) : round($price, 8);
    }

    protected function roundVolume(float $price, float $vol): float
    {
        return $price > 1 ? round($vol / $price, 6) : round($vol / $price ? $price : 1, 1);
    }

    /**
     * @param int $trades_buy_count
     * @param $traders_count
     * @param array $orders
     * @return array
     */
    public function generateOrders(int $trades_buy_count, $traders_count, array $orders): array
    {
        $trades = [];
        $slice_size = $trades_buy_count;

        foreach (self::DEALS as $deal) {
            $orders[$deal] = array_slice($orders[$deal], 1, $traders_count / 2);
            sort($orders[$deal]);

            $transactions = array_slice($orders[$deal], 1, $slice_size);
            foreach ($transactions as $transaction) {
                $transaction['v'] /= 2;
                $trades[] = $transaction;
            }

            $slice_size = $traders_count - $trades_buy_count;
        }

        shuffle($trades);

        return compact('orders', 'trades');
    }
}
