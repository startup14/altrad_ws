<?php

namespace App\Console\Ship\Bots;

final class StocksBot extends BaseBot implements Bot
{

    /**
     * @throws \Exception
     */
    public function k1(): float
    {
        return random_int(1, 10) / 10000;
    }

    public function k2(): float
    {
        return 1;
    }

    /**
     * @throws \Exception
     */
    public function generateActivity(string $base, string $quote, array $data, array $params = []): array
    {
        /*
         * Init vars
         */
        [$buy, $sell] = [[], []];
        $orders = compact('buy', 'sell');

        $traders_count = $params['count'] ?? self::DEFAULT_TRADERS_COUNT;
        $trades_buy_count = random_int(1, $traders_count);

        $min = $data['p'] * 100;
        $max = $data['p'] * 300;

        $price = $data['p'];

        /*
         * Calculations
         * Buy-Sell simulation prices
         */

        // Fill in prices for buy/sell
        for ($i = 0; $i < $traders_count; ++$i) {
            foreach (self::DEALS as $deal) {
                $p = $price + $this->k1() * $price;
                $v = $this->k2() * random_int($min, $max);

                $p = $this->roundPrice($p);
                $v = $this->roundVolume($p, $v);

                $type = $deal;
                $orders[$deal][] = compact('p', 'v', 'type');

                // sell price must be > buy price
                $price = $p;
            }
        }

        return $this->generateOrders($trades_buy_count, $traders_count, $orders);
    }
}
