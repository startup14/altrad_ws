<?php

namespace App\Console\Ship\Bots;

final class EmptyBook
{
    public static function build(): array
    {
        [$orders, $trades] = [[], []];
        return compact('orders', 'trades');
    }
}
