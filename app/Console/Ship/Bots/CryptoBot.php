<?php

namespace App\Console\Ship\Bots;

final class CryptoBot extends BaseBot implements Bot
{

    /**
     * @throws \Exception
     */
    public function generateActivity(string $base, string $quote, array $data, array $params = []): array
    {
        /*
         * Init vars
         */
        [$buy, $sell] = [[], []];
        $orders = compact('buy', 'sell');

        $traders_count = $params['count'] ?? self::DEFAULT_TRADERS_COUNT;
        $trades_buy_count = random_int(1, $traders_count);

        $min = $params['min'] ?? self::DEFAULT_VOL_MIN;
        $max = $params['max'] ?? self::DEFAULT_VOL_MAX;
        if ($base === 'USDT') {
            $min /= $data['p'];
            $max /= $data['p'];
        }
        $price = $data['p'];

        /*
         * Calculations
         * Buy-Sell simulation prices
         */

        // Fill in prices for buy/sell
        for ($i = 0; $i < $traders_count; ++$i) {
            foreach (self::DEALS as $deal) {
                $p = $price + $this->k1() * $price;
                $v = $min +  $this->k2() * ($max - $min);

                $p = $this->roundPrice($p);
                $v = $this->roundVolume($p, $v);

                $type = $deal;
                $orders[$deal][] = compact('p', 'v', 'type');

                // sell price must be > buy price
                $price = $p;
            }
        }

        return $this->generateOrders($trades_buy_count, $traders_count, $orders);
    }

    /**
     * @throws \Exception
     */
    public function k1(): float
    {
        return random_int(1, 10) / 10000;
    }

    public function k2(): float
    {
        return mt_rand() / mt_getrandmax();
    }
}
