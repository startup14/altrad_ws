<?php

namespace App\Console\Ship\Bots;

interface Bot
{
    public const BOTS_NS = __NAMESPACE__;

    public const DEFAULT_VOL_MIN = 100; // todo: Know why?
    public const DEFAULT_VOL_MAX = 15000; // todo: Know why?

    public const DEFAULT_TRADERS_COUNT = 21;


    public function generateActivity(string $base, string $quote, array $data, array $params = []): array;

    public function k1(): float;
    public function k2(): float;
}
