<?php

namespace App\Console\Ship\Decorators;

class BinanceKlineDecorator implements TickerDecorator
{

    public function dressUp(array $ticker): array
    {
        $append = [
            'open_time' => $ticker[0],
            'close_time' => $ticker[6],
            'open' => (DOUBLE)$ticker[1],
            'high' => (DOUBLE)$ticker[2],
            'low' => (DOUBLE)$ticker[3],
            'close' => (DOUBLE)$ticker[4],
            'vol' => (DOUBLE)$ticker[5],
        ];

        return array_merge($ticker, $append);
    }
}
