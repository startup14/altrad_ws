<?php

namespace App\Console\Ship\Decorators;

interface TickerDecorator
{
    public function dressUp(array $ticker): array;
}
