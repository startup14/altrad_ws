<?php

namespace App\Console\Ship\Decorators;

class BinanceTickerDecorator implements TickerDecorator
{

    public function dressUp(array $ticker): array
    {
        $append = [
            'open_time' => $ticker['openTime'],
            'close_time' => $ticker['closeTime'],
            'open' => (DOUBLE)$ticker['openPrice'],
            'high' => (DOUBLE)$ticker['highPrice'],
            'low' => (DOUBLE)$ticker['lowPrice'],
            'close' => (DOUBLE)$ticker['lastPrice'],
            'vol' => (DOUBLE)$ticker['volume'],
        ];
        return array_merge($ticker, $append);
    }
}
