<?php

namespace App\Console\Ship\Transformers;

use App\Console\Ship\Contracts\TicketTransformer;

final class DefaultTickerTransformer implements TicketTransformer
{

    public function transform(array $updates, array $parent = []): array
    {
        $newborn = $parent;

        $newborn['symbol'] = $updates['symbol'] ?? $updates['base'] ?? 'N/A';

        $newborn['active'] = 1;
        if ($updates['close'] <= 0 || is_null($updates['close'])) {
            $newborn['active'] = 0;
        }

        $newborn['pp'] = (float) ($parent['p'] ?? $updates['close']);
        $newborn['p'] = (float) $updates['close'];
        $newborn['o'] = (float) $updates['open'];
        $newborn['h'] = (float) $updates['high'];
        $newborn['l'] = (float) $updates['low'];
        $newborn['pc'] = (float) $updates['open'];
        $newborn['v'] = (int) $updates['vol'];
        $newborn['ph'] = crc32($updates['close'] . config('hashing.rate_hash'));

        return $newborn;
    }
}
