<?php

namespace App\Console\Ship\Transformers;

use App\Console\Ship\Contracts\TicketTransformer;

class TwelveDataTransformer implements TicketTransformer
{

    public function transform(array $updates, array $parent = []): array
    {
        $newborn = $parent;

        $new_price = (float) ($updates['price'] ?? $updates['close']);
        $price_high = (float) ($parent['h'] ?? 0);
        $price_low = (float) ($parent['l'] ?? 0);

        $newborn['pp'] = (float) ($parent['p'] ?? $new_price);
        $newborn['p'] = $new_price;
        $newborn['h'] = $new_price > $price_high ? $new_price : $price_high;
        $newborn['l'] = $new_price < $price_low ? $new_price : $price_low;
        $newborn['v'] = (int) ($updates['day_volume'] ?? $parent['v']);

        return $newborn;
    }
}
