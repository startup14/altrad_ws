<?php

namespace App\Console\Ship\Redis;

final class RedisStreamNames
{
    public const KEY_SEPARATOR = ':';

    public const CRYPTO = 'crypto';
    public const COMMODITIES = 'commodities';
    public const STOCKS = 'stocks';

    public const ORDERS = 'orders';
    public const TRADES = 'trades';
}
