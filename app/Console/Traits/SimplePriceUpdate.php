<?php

namespace App\Console\Traits;

use Illuminate\Support\Facades\Log;

trait SimplePriceUpdate
{
    protected function ensureKeyExists(string $key, $details, $default = 'n/a')
    {
        if (!array_key_exists($key, $details)) {
            Log::warning("Key <$key> not found in: " . print_r($details, true));
            return $default;
        }
        return $details[$key];
    }

    protected function simplePriceUpdate(string $keyspace): bool
    {
        $docs = redis_hgetall_for_keyspace($keyspace);
        foreach ($docs as $redis_key => $fields) {
            $fields['p'] = $this->ensureKeyExists('p', $fields);
            $fields['h'] = $fields['p'];
            $fields['l'] = $fields['p'];
            $fields['o'] = $fields['p'];
            $fields['pc'] = $fields['p'];

            $docs[$redis_key] = $fields;
        }

        return redis_hmsetall($docs);
    }
}
