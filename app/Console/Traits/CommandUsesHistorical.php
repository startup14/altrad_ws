<?php

namespace App\Console\Traits;

use App\HistoricalData;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;

trait CommandUsesHistorical
{
    protected function setupVolume($check, string $period, array $data, array $time)
    {
        if (! $data['v'] || ! is_numeric($data['v'])) {
            $this->error(sprintf('setupVolume damaged data <v>: %s', var_export($data, true)));
            return false;
        }

        if ($period === '1d') {
            return $data['v'];
        }

        if (($period === '1w') || ($period === '1M')) {
            return $data['v'] * $time['mul'];
        }
        $v = $data['v'] * $time['mul'] / $time['div'] ;
        return $check->volume + rand(1, max(1, $v));
    }

    protected function chooseFee(array $sym_fee, string $symbol)
    {
        if (empty($sym_fee)) {
            return 0;
        }
        return $sym_fee[$symbol . 'USDT'] ?? 0;
    }

    protected function historicalDataFor(string $symbol, string $period)
    {
        return HistoricalData::where('symbol', $symbol)
            ->where('time_period', $period)
            ->where('open_time', '<=', Carbon::now()->timestamp . '000')
            ->where('close_time', '>=', Carbon::now()->timestamp . '999')
            ->latest('updated_at')
            ->first();
    }

    protected function historicalPrevDataFor(string $symbol, string $period, $close_time)
    {
        return HistoricalData::where('symbol', $symbol)
            ->where('close_time', $close_time)
            ->where('time_period', $period)
            ->first();
    }

    protected function historicalCreate(string $symbol, string $period, array $data, array $time): int
    {
        if (count(missed_keys($data, ['p', 'v'])) > 0) {
            Log::warning(__METHOD__ . ": not all required fields for <$symbol>:" . var_export($data, true));
            return false;
        }

        $price = $data['p'];
        $volume = $data['v'];

        $response = 200;

        try {
            HistoricalData::create([
                'open_time' => $time['start'],
                'close_time' => $time['end'],
                'close' => $price,
                'volume' => $volume,
                'high' => $price,
                'low' => $price,
                'open' => $price,
                'time_period' => $period,
                'symbol' => $symbol,
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            $response = $e->getCode();
        }

        return $response;
    }

    protected function getPeriods(): array
    {
        return [
            '1m' => [
                'start' => Carbon::now()->startOfMinute()->timestamp . '000',
                'end' => Carbon::now()->endOfMinute()->timestamp . '999',
                'div' => '36000',
                'mul' => '1'
            ],
            '5m' => [
                'start' => Carbon::now()->startOfMinute()->timestamp . '000',
                'end' => Carbon::now()->addMinutes(4)->endOfMinute()->timestamp . '999',
                'div' => '7200',
                'mul' => '1'
            ],
            '15m' => [
                'start' => Carbon::now()->startOfMinute()->timestamp . '000',
                'end' => Carbon::now()->addMinutes(14)->endOfMinute()->timestamp . '999',
                'div' => '2400',
                'mul' => '1'
            ],
            '30m' => [
                'start' => Carbon::now()->startOfMinute()->timestamp . '000',
                'end' => Carbon::now()->addMinutes(29)->endOfMinute()->timestamp . '999',
                'div' => '1200',
                'mul' => '1'
            ],
            '1h' => [
                'start' => Carbon::now()->startOfHour()->timestamp . '000',
                'end' => Carbon::now()->endOfHour()->timestamp . '999',
                'div' => '24',
                'mul' => '1'
            ],
            '1d' => [
                'start' => Carbon::now()->startOfDay()->timestamp . '000',
                'end' => Carbon::now()->endOfDay()->timestamp . '999',
                'div' => '1',
                'mul' => '1'
            ],
            '1w' => [
                'start' => Carbon::now()->startOfWeek()->timestamp . '000',
                'end' => Carbon::now()->endOfWeek()->timestamp . '999',
                'div' => '1',
                'mul' => '7'
            ],
            '1M' => [
                'start' => Carbon::now()->startOfMonth()->timestamp . '000',
                'end' => Carbon::now()->endOfMonth()->timestamp . '999',
                'div' => '1',
                'mul' => '3'
            ]
        ];
    }
}
