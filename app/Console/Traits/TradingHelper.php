<?php

namespace App\Console\Traits;

trait TradingHelper
{
    protected function typeOfMarkets(): array
    {
        return [
            'crypto',
            'stock',
            'commodities',
        ];
    }
}
