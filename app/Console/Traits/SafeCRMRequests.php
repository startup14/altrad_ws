<?php

namespace App\Console\Traits;

use Exception;
use Illuminate\Support\Facades\Http;

trait SafeCRMRequests
{
    private function crmHttpGetJson(string $path, array $data = [], string $key = null)
    {
        $json = [];

        try {
            $response = Http::get(env('CRM_URL', $data) . $path);
            $json = $response->json();
            if ($key) {
                $json = $json[$key];
            }
        } catch (Exception $e) {
            $this->error($e->getMessage());
        }

        return $json;
    }

    private function crmHttpPostJson(string $path, array $json, string $method = 'post'): array
    {
        $response['code'] = 200;

        try {
            Http::$method(env('CRM_URL') . $path, $json);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $this->error($error);
            $response['code'] = $e->getCode();
            $response += compact('error');
        }

        return $response;
    }

    private function crmHttpPutJson(string $path, array $json): array
    {
        return $this->crmHttpPostJson($path, $json, 'put');
    }
}
