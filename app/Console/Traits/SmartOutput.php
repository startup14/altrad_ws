<?php

namespace App\Console\Traits;

trait SmartOutput
{
    protected function smartOutput(array $lines): void
    {
        $cli = PHP_SAPI === 'cli';
        foreach ($lines as $line) {
            $message = $line['message'] ?? $line['line'] ?? $line;
            if ($cli) {
                $this->info($message);
            } else {
                echo $message . '<br />';
            }
        }
    }
}
