<?php

namespace App\Console\Traits;

use App\Console\Ship\Redis\RedisStreamNames;
use Illuminate\Support\Facades\Redis;
use LogicException;

trait CommandUsesRedis
{
    protected function getHashKeysPattern(string $pattern): array
    {
        return Redis::keys("*$pattern*");
    }

    protected function generateHashKey(string $stream, string $base, string $quote): string
    {
        return redis_create_key_for_keyspace($stream, $base, $quote);
    }

    protected function extractBaseAndQuote(string $redis_key): array
    {
        return redis_key_to_base_and_quote($redis_key);
    }
}
