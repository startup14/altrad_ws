<?php

namespace App\Console\Traits;

use Exception;

trait CommandAbleTransfer
{
    private function assignTarget(
        string $fun_key, string $base, array $details, array &$target):void
    {
        $redis_key = $fun_key($base);
        $target[$redis_key] = $details;
    }

    private function exploreResource(string $server, string $path): string
    {
        $source = get_guzzle_response($server, $path);

        if (is_arr_key_live(RONINS_ERROR_KEY, $source)) {
            $error = $source[RONINS_ERROR_KEY];
            $this->line('Error: ' . $error);
            return $error;
        }

        return $source['origin']->getBody();
    }

    private function jsonifyString(string $source): array
    {
        $json = [];

        try {
            $json = json_decode($source, true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            $this->line($e->getMessage());
        }

        return $json;
    }

    private function grabJsonResource(string $server, string $path): array
    {
        return $this->jsonifyString(
            $this->exploreResource($server, $path)
        );
    }

    private function transferFrom(string  $keyspace, string $server, string $path): int
    {
        $source = $this->grabJsonResource($server, $path);

        if (! array_key_exists($keyspace, $source)) {
            $this->line("Error: key <$keyspace> not found");
            return false;
        }

        $target = [];
        foreach ($source[$keyspace] as $base => $details) {
            $fun_key = "redis_create_{$keyspace}_key";
            $this->assignTarget($fun_key, $base, $details, $target);
        }

        redis_hmsetall($target);

        return count($target);
    }
}
