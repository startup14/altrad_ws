<?php

namespace App\Console\Traits;

use Illuminate\Support\Facades\Artisan;

trait ExecuteCommandsGroup
{
    protected function handleCommands(array $commands): int
    {
        $total = 0;
        foreach ($commands as $command) {
            $processed = Artisan::call($command);
            $this->lineProcess($command, $processed);
            $total += $processed;
        }
        return $total;
    }
}
