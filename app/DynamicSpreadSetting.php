<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DynamicSpreadSetting extends Model
{
    protected $fillable = [
        'campaign_id',
        'coef',
        'bot',
        'top'
    ];
}
