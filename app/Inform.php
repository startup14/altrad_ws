<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inform extends Model
{
    protected $fillable = [
        'user_id',
        'type'
    ];
}
