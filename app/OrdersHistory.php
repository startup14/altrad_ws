<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdersHistory extends Model
{
    use SoftDeletes;

    protected $table = 'orders_history';

    protected $dates = [
        'time_of_open'
    ];

    protected $fillable = [
        'status',
        'entry_price',
        'type',
        'amount',
        'limit',
        'value',
        'pair',
        'currency',
        'liq',
        'lv',
        'user_id',
        'class',
        'leverage',
        'take_profit',
        'stop_loss',
        'close',
        'time_of_open',
        'type_of_market',
        'unit',
        'balance_before_open',
        'balance_after_open',
        'balance_before_close',
        'balance_after_close',
        'front_balance',
        'swop',
        'crypto_amount',
        'conversion',
        'deleted_at',
        'description'
    ];
}
