<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradingHours extends Model
{
    protected $table = 'trading_hours';
}
