<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Http;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Symfony\Component\HttpKernel\Exception\NotFoundHttpException::class
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Throwable $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        if((env('APP_ENV') === 'production') && !$this->contains($exception->getFile(), ['Application.php', 'AbstractRouteCollection.php'])) {
            $chats = [
                '1301684693',
                '1369422538'
            ];
            foreach ($chats as $chat) {
                Http::post("https://api.telegram.org/bot1382575312:AAHFzNjaVKKUXHzMlzjrfG83yOtN_SlzDRE/sendMessage?chat_id={$chat}&text=API, " . $exception->getMessage() . ", file: " . $exception->getFile() . ", line: " . $exception->getLine());
            }
        }

        // Sentry
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Throwable $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        return parent::render($request, $exception);
    }

    function contains($string, $array)
    {
        foreach ($array as $value) {
            if (stripos($string, $value) !== false) return true;
        }
        return false;
    }
}
