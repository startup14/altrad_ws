<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZeroLog extends Model
{
    protected $fillable = [
        'ticker'
    ];
}
