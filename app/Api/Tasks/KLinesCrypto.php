<?php

namespace App\Api\Tasks;

use App\Api\Contracts\KlinesProvider;
use App\Api\Controllers\ApiBaseController;
use App\Api\Transformers\ApiTransformer;
use App\Contracts\CryptoProviderInterface;
use Illuminate\Support\Facades\Log;

final class KLinesCrypto implements KlinesProvider
{
    private const KLINE_LIMIT = 1000;

    public function klines(string $symbol, $interval, $ts, ApiTransformer $transformer = null): array
    {
        $ts = $ts[0];

        $apiDataProvider = app(CryptoProviderInterface::class); // BinanceApi

        $klines = $apiDataProvider
            ->interval($interval)
            ->limit(self::KLINE_LIMIT)
            ->endTime($ts)
            ->getKline($symbol);

        if (array_key_exists('code', $klines) && $klines['code'] !== 0) {
            $msg = "Binance error {$klines['code']}: {$klines['msg']} [$symbol]";
            Log::error($msg);
            return ApiBaseController::warningResponse($msg);
        }

        if ($transformer) {
            $klines = array_map(static function ($kline) use ($transformer) {
                return $transformer->convert($kline);
            }, $klines);
        }

        return  $klines;
    }
}
