<?php

namespace App\Api\Tasks;

use App\Api\Transformers\ApiTransformer;
use App\HistoricalData;

final class GetHistoryForKlines
{
    public function grab(string $symbol, string $interval, ApiTransformer $transformer = null): array
    {
        $history = HistoricalData::where('symbol', $symbol)
            ->where('time_period', $interval)->get();

        if ($transformer) {
            $transformer = new $transformer;
            $history = $transformer->convert($history->toArray());
        }

        return $history;
    }
}
