<?php

namespace App\Api\Tasks;

use App\Api\Contracts\KlinesProvider;
use App\Api\Controllers\ApiBaseController;
use App\Api\Transformers\ApiTransformer;
use App\QuoteProviders\AlphaVantage\AlphaVantageAPI;
use Illuminate\Support\Facades\Log;

final class KLinesStocks implements KlinesProvider
{

    public function klines(string $symbol, $interval, $ts, ApiTransformer $transformer): array
    {
        $ts = $ts[0];

        $result = [];

        try {
            $apiDataProvider = app(AlphaVantageAPI::class); //

            $klines = $apiDataProvider
                ->getKline($symbol, $interval);

            foreach ($klines as $kline) {
                if ($kline['open_time'] >= $ts) {
                    continue;
                }

                if (in_array($interval, ['1M', '1w', '1d'], true)) {
                    $result[] = [
                        (int)($kline['open_time']),
                        (string)($kline['adj_close'] + ($kline['open'] - $kline['close'])),
                        (string)($kline['adj_close'] + ($kline['high'] - $kline['close'])),
                        (string)($kline['adj_close'] + ($kline['low'] - $kline['close'])),
                        (string)($kline['adj_close']),
                        (string)($kline['vol']),
                        (int)($kline['close_time']),
                        '0', 0, '0', '0', '0',
                    ];
                    continue;
                }
                $result[] = $transformer->convert($kline);
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return ApiBaseController::warningResponse($e->getMessage(), 500);
        }

        return  $result;
    }
}
