<?php

namespace App\Api\Tasks;

use App\Api\Contracts\KlinesProvider;
use App\Api\Controllers\ApiBaseController;
use App\Api\Transformers\ApiTransformer;
use App\Contracts\CommoditiesProviderInterface as CommoditiesProvider;
use Illuminate\Support\Facades\Log;

final class KLinesCommodities implements KlinesProvider
{

    public function klines(string $symbol, $interval, $ts, ApiTransformer $transformer): array
    {
        $result = [];
        [$ts0, $ts1] = [$ts[0], $ts[1]];

        try {
            $apiDataProvider = app(CommoditiesProvider::class); //

            $klines = $apiDataProvider
                ->sort('desc')
                ->getKline($symbol, $interval, '2020-01-01', $ts0);

            foreach ($klines as $kline) {
                if ($kline['open_time'] >= $ts1) {
                    continue;
                }
                $result[] = $transformer->convert($kline);
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return ApiBaseController::warningResponse($e->getMessage(), 500);
        }

        return  $result;
    }
}
