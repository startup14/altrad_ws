<?php

if (! function_exists('return_e')) {
    function return_e(string $info): array {
        $e = $info;
        return compact('e');
    }
}

if (! function_exists('jamison')) {
    function jamison(string $base_url, string $method, array $params = []): array {

        $response = get_guzzle_response($base_url, $method, $params);

        if (is_arr_key_live('e', $response)) {
            return return_e($response['e']);
        }

        $response = $response['origin'];

        expect($response->getStatusCode())->toEqual(200);
        expect((string)$response->getBody())->toBeJson();

        try {
            return json_decode((string)$response->getBody(), true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception | LogicException | RuntimeException $e) {
            return return_e($e->getMessage());
        }
    }
}
