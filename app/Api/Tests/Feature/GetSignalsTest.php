<?php

uses(Tests\TestCase::class);

include_once '_helpers.php';

test('signals', function (string $path) {
    $actual = jamison(env('TEST_API_HOST'), $path);
    $expected = jamison(env('PREPROD_API_HOST'), $path);

    $actual['time'] = $expected['time'];

    expect($actual)->toEqual($expected);
})->with([
    ['signals'],
]);
