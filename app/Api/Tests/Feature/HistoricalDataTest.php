<?php

uses(Tests\TestCase::class);

include_once '_helpers.php';

test('klines', function (string $symbol, string $base, string $interval) {
    $path = 'klines';

    $actual = jamison(env('TEST_API_HOST'), $path, compact('symbol', 'base', 'interval'));
    $expected = jamison(env('PREPROD_API_HOST'), $path, compact('symbol', 'base', 'interval'));

    $diff = array_diff_key_recursive($expected, $actual);
    expect($diff)->toBeEmpty();

})->with([
    ['BTCUSDT', 'BTC', '1d'],   // crypto
    ['ETHBTC', 'ETH', '1d'],   // crypto
    ['AAPL', '1d', 'AAPL'],     // stocks
    ['XAU', '1d', 'XAU'],       // commodities
]);
