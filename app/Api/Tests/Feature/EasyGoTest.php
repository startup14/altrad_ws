<?php

uses(Tests\TestCase::class);

include_once '_helpers.php';

test('easy-go', function (string $path, bool $strict = false) {
    $actual = jamison(env('TEST_API_HOST'), $path);
    $expected = jamison(env('PREPROD_API_HOST'), $path);

    if ($strict) {
        expect($actual)->toEqual($expected);
    } else {
        $diff = array_diff_key_recursive($expected, $actual);
        expect($diff)->toBeEmpty();
    }

})->with([
    ['exchangeInfo', true],
    ['currency-rate'],
    ['available-actives'],
    ['online-info'],
]);
