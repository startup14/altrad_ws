<?php

uses(Tests\TestCase::class);

include_once '_helpers.php';

test('structure identical', function (array $a1, array $a2) {
    $diff = array_diff_key_recursive($a1, $a2);
    expect($diff)->toBeEmpty();
})->with([
    [
        ['k1' => 'k1', 'k2' => 'k2', 'k3' => ['k31' => 'k31']],
        ['k1' => 'k1', 'k2' => 'k2', 'k3' => ['k31' => 'k31']],
    ],
    [
        ['k1' => 'k1', 'k2' => ['k21' => ['k211' => 'k211']], 'k3' => ['k31' => 'k31']],
        ['k1' => 'k1', 'k2' => ['k21' => ['k211' => 'k211']], 'k3' => ['k31' => 'k31']],
    ],
]);

test('structure NOT identical', function (array $a1, array $a2) {
    $diff = array_diff_key_recursive($a1, $a2);
    expect($diff)->not()->toBeEmpty();
})->with([
    [
        ['k1' => 'k1', 'k4' => 'k2', 'k3' => ['k31' => 'k31']],
        ['k1' => 'k1', 'k2' => 'k2', 'k3' => ['k31' => 'k31']],
    ],
    [
        ['k1' => 'k1', 'k2' => 'k21', 'k3' => ['k31' => 'k31']],
        ['k1' => 'k1', 'k2' => ['k21' => ['k211' => 'k211']], 'k3' => ['k31' => 'k31']],
    ],
]);
