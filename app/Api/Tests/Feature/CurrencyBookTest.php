<?php

uses(Tests\TestCase::class);

include_once '_helpers.php';

test('currency-book', function (string $type, string $fsym, string $tsym) {
    // currency-book?type=crypto&fsym=BTC&tsym=USDT
    // currency-book?type=stock&fsym=AAPL&tsym=USDT
    // currency-book?type=commodities&fsym=XAU&tsym=USDT
    $path = 'currency-book';

    $actual = jamison(env('TEST_API_HOST'), $path, compact('type', 'fsym', 'tsym'));
    $expected = jamison(env('PREPROD_API_HOST'), $path, compact('type', 'fsym', 'tsym'));

    $diff = array_diff_key_recursive($expected, $actual);
    expect($diff)->toBeEmpty();

})->with([
    ['crypto', 'BTC', 'USDT'],   // crypto
    ['crypto', 'ETH', 'USDT'],   // crypto
    ['stocks', 'AAPL', 'USDT'],     // stocks
    ['commodities', 'XAU', 'USDT'],       // commodities
]);
