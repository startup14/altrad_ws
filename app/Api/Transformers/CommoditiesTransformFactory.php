<?php

namespace App\Api\Transformers;

class CommoditiesTransformFactory implements ApiTransformerFactory
{

    public function createTransformer(): ApiTransformer
    {
        return new CommoditiesTransformer();
    }
}
