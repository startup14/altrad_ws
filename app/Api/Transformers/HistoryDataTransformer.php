<?php

namespace App\Api\Transformers;

class HistoryDataTransformer implements ApiTransformer
{

    private function klineType()
    {

    }
    public function convert(array $data): array
    {
        $converted = [];

        foreach ($data as $record) {
            $converted[] = [
                (int)    $record['open_time'],
                (string) $record['open'],
                (string) $record['high'],
                (string) $record['low'],
                (string) $record['close'],
                (string) $record['volume'],
                (int)    $record['close_time'],
                '0', 0, '0', '0', '0',
            ];
        }

        return $converted;
    }
}
