<?php

namespace App\Api\Transformers;

final class CryptoTransformer implements ApiTransformer
{
    private function initRecord(array $fields): array
    {
        $name = $fields['name'];
        return compact('name');
    }

    public function convert(array $data): array
    {
        $converted = [];

        foreach ($data as $redis_key => $fields) {
            ['base' => $base, 'quote' => $quote] = redis_key_to_base_and_quote($redis_key);
            if (! array_key_exists($quote, $converted)) {
                $converted[$quote] = $this->initRecord($fields);
            }
            $converted[$quote][$base] = $fields;
        }

        return $converted;
    }
}
