<?php

namespace App\Api\Transformers;

interface ApiTransformer
{
    public function convert(array $data): array;
}
