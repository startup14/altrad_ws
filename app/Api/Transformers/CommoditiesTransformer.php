<?php

namespace App\Api\Transformers;

class CommoditiesTransformer implements ApiTransformer
{

    public function convert(array $data): array
    {
        $converted = [];

        foreach ($data as $redis_key => $fields) {
            $api_key = redis_key_to_base_and_quote($redis_key)['quote'];
            $converted[$api_key] = $fields;
        }

        return $converted;
    }
}
