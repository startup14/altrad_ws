<?php

namespace App\Api\Transformers;

interface ApiTransformerFactory
{
    public const NS = __NAMESPACE__;

    public function createTransformer(): ApiTransformer;
}
