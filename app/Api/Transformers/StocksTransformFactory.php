<?php

namespace App\Api\Transformers;

class StocksTransformFactory implements ApiTransformerFactory
{

    public function createTransformer(): ApiTransformer
    {
        return new StocksTransformer();
    }
}
