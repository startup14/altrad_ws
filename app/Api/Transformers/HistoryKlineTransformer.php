<?php

namespace App\Api\Transformers;

class HistoryKlineTransformer implements ApiTransformer
{

    public function convert(array $data): array
    {
        return [
                (int)    $data['open_time'],
                (string) $data['open'],
                (string) $data['high'],
                (string) $data['low'],
                (string) $data['close'],
                (string) $data['vol'],
                (int)    $data['close_time'],
                '0', 0, '0', '0', '0',
            ];
    }
}
