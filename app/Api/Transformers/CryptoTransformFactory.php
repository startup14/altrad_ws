<?php

namespace App\Api\Transformers;

class CryptoTransformFactory implements ApiTransformerFactory
{

    public function createTransformer(): ApiTransformer
    {
        return new CryptoTransformer();
    }
}
