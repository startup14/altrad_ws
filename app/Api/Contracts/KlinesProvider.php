<?php

namespace App\Api\Contracts;

use App\Api\Transformers\ApiTransformer;

interface KlinesProvider
{
    public function klines(string $symbol, $interval, $ts, ApiTransformer $transformer): array;
}
