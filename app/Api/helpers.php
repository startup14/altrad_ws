<?php

use App\Api\Transformers\ApiTransformerFactory;
use App\Console\Ship\Redis\RedisStreamNames;
use Illuminate\Support\Str;

if (! function_exists('convert_redis_keyspace_to_api')) {
    function convert_redis_keyspace_to_api(string $keyspace): array  {
        $cache = redis_hgetall_for_keyspace($keyspace);
        $factory = Str::finish(ApiTransformerFactory::NS, "\\") . ucfirst($keyspace) . 'TransformFactory';
        return (new $factory)->createTransformer()->convert($cache);
    }
}

if (! function_exists('all_keyspaces')) {
    function all_keyspaces(): array {
        return [
            RedisStreamNames::CRYPTO,
            RedisStreamNames::STOCKS,
            RedisStreamNames::COMMODITIES,
        ];
    }
}


if (! function_exists('ucfirst_keyspaces')) {
    function ucfirst_keyspaces(): array {
        return array_map(static fn ($space) => ucfirst($space), all_keyspaces());
    }
}

