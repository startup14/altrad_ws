<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConnectionHistory extends Model
{
    protected $table = 'connection_history';

    protected $fillable = [
        'platform',
        'user_id',
        'os',
        'ip',
        'created_at',
        'updated_at',
        'closed_at'
    ];

    protected $dates = [
        'closed_at',
        'created_at',
        'updated_at'
    ];
}
