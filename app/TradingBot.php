<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradingBot extends Model
{
    protected $fillable = [
        'name',
        'description',
        'type',
        'fee',
        'price_buy',
        'price_rent',
        'rating',
        'avatar',
        'author',
        'version',
        'images',
        'campaign_id'
    ];

    public function reviews()
    {
        return $this->hasMany('App\TradingBotReview', 'bot_id');
    }

    public function images()
    {
        return $this->hasMany('App\TradingBotImage', 'bot_id');
    }
}
