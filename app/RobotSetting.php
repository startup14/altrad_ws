<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RobotSetting extends Model
{
    protected $fillable = [
        'bought_bot_id',
        'margin_call_evade',
        'take_profit',
        'stop_loss',
        'max_capital'
    ];
}
