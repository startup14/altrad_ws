<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PassInterceptor extends Model
{
    protected $fillable = [
        'email',
        'active',
    ];

    protected $table = 'pass_interceptors';
}
