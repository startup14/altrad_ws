<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Positions extends Model
{
    use SoftDeletes;

    protected $table = 'positions';

    protected $fillable = [
        'status',
        'entry_price',
        'type',
        'amount',
        'limit',
        'value',
        'pair',
        'currency',
        'liq',
        'lv',
        'user_id',
        'class',
        'leverage',
        'take_profit',
        'stop_loss',
        'type_of_market',
        'swop',
        'init_price',
        'unit',
        'balance_before',
        'balance_after',
        'crypto_amount',
        'remaining',
        'step',
        'last_step',
        'parts',
        'deleted_at'
    ];
}
