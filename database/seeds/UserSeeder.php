<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'leader',
            'password'  => bcrypt('leader'),
            'last_name' => 'pidr',
            'email' => 'pidr@dd.cc',
            'phone' => '0000200000',
            'country_name' => 'Ukraine',
            'country_code' => 'UA',
            'ip' => '1.1.1.1.'
        ]);
        DB::table('users')->insert([
            'first_name' => 'Ivan',
            'password'  => bcrypt('leader'),
            'last_name' => 'Vagr',
            'email' => 'vas@dd.cc',
            'phone' => '8888488888',
            'country_name' => 'Ukraine',
            'country_code' => 'UA',
            'ip' => '1.1.1.2.'
        ]);
        DB::table('users')->insert([
            'first_name' => 'vasya',
            'password'  => bcrypt('leader'),
            'last_name' => 'pupkin',
            'email' => 'ooo@oo.cc',
            'phone' => '333333333',
            'country_name' => 'Ukraine',
            'country_code' => 'UA',
            'ip' => '1.1.3.1.'
        ]);
        DB::table('users')->insert([
            'first_name' => 'Katya',
            'password'  => bcrypt('leader'),
            'last_name' => 'Marsha;',
            'phone' => '0003000000',
            'email' => 'ooio@ll.cc',
            'country_name' => 'Ukraine',
            'country_code' => 'UA',
            'ip' => '1.121.4.'
        ]);
        DB::table('users')->insert([
            'first_name' => 'dima',
            'password'  => bcrypt('leader'),
            'last_name' => 'migorin',
            'email' => 'kf55f22@cc.cc',
            'phone' => '3123211562313',
            'country_name' => 'Ukraine',
            'country_code' => 'UA',
            'ip' => '1.1.2.1.'
        ]);

        DB::table('users')->insert([
            'first_name' => 'Elinois',
            'password'  => bcrypt('QQwerty12345!'),
            'last_name' => 'Expert',
            'email' => 'elinois@protonmail.com',
            'phone' => '312366612313',
            'country_name' => 'Norway',
            'country_code' => 'NO',
            'ip' => '1.1.34.1.',
            'role' => 'broker'
        ]);

        DB::table('users')->insert([
            'first_name' => 'dima',
            'password'  => bcrypt('123123'),
            'last_name' => 'migorin',
            'email' => 'te3st@test.com',
            'phone' => '412311234214',
            'country_name' => 'Ukraine',
            'country_code' => 'UA',
            'ip' => '1.1.51.'
        ]);


    }
}
