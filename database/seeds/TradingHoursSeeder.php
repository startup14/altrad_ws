<?php

use Illuminate\Database\Seeder;

class TradingHoursSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trading_hours')->insert([
            'region' => 'US',
            'opening_time' => '13:30:00',
            'closing_time' => '20:00:00'
        ]);

        DB::table('trading_hours')->insert([
            'region' => 'UK',
            'opening_time' => '07:00:00',
            'closing_time' => '15:00:00'
        ]);

        DB::table('trading_hours')->insert([
            'region' => 'EU',
            'opening_time' => '06:00:00',
            'closing_time' => '16:00:00'
        ]);
    }
}
