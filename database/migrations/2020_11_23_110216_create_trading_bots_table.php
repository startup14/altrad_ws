<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTradingBotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trading_bots', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description');
            $table->enum('type', ['crypto', 'stocks', 'all'])->default('all');
            $table->enum('rating', ['0', '1', '2', '3', '4', '5'])->default('0');
            $table->double('fee');
            $table->double('price_buy')->default(0);
            $table->double('price_rent')->default(0);
            $table->binary('avatar')->nullable();
            $table->string('author');
            $table->string('version')->default('1.0');
            $table->unsignedBigInteger('campaign_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trading_bots');
    }
}
