<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRobotSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('robot_settings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('bought_bot_id');
            $table->enum('margin_call_evade', ['1', '0'])->default(0);
            $table->enum('take_profit', ['1', '0'])->default(0);
            $table->enum('stop_loss', ['1', '0'])->default(0);
            $table->double('max_capital')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('robot_settings');
    }
}
