<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTradingBotReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trading_bot_reviews', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('bot_id');
            $table->enum('rating', ['0', '1', '2', '3', '4', '5'])->default('0');
            $table->text('text')->nullable();
            $table->string('author');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trading_bot_reviews');
    }
}
